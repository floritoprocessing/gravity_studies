import java.util.Vector;

Vector particles;
float xCen1, yCen1, xCen2, yCen2;

void setup() {
  size(640,480,P3D);
  particles = new Vector();
  background(255);
  xCen1 = width/2.0 + random(-1,1)*width;
  yCen1 = height/2.0 + random(-1,1)*height;
  xCen2 = width/2.0 + random(-1,1)*width;
  yCen2 = height/2.0 + random(-1,1)*height;
}

void keyPressed() {
  if (key=='c') setup();
}

void draw() {
  float xOff1 = width/2.0 * sin(0.5*millis()/1000.0);
  float yOff1 = height/2.0 * sin(0.8*millis()/1000.0);
  float xOff2 = width/2.0 * sin(0.9*millis()/1000.0);
  float yOff2 = height/2.0 * sin(1.4*millis()/1000.0);

  if (mousePressed) for (int i=0;i<10;i++) particles.add(new Particle(mouseX,mouseY,mouseX-pmouseX,mouseY-pmouseY));

  for (int i=0;i<particles.size();i++) ((Particle)particles.elementAt(i)).update(particles,xCen1+xOff1,yCen1+yOff1,-1.0);
  for (int i=0;i<particles.size();i++) ((Particle)particles.elementAt(i)).update(particles,xCen2+xOff2,yCen2+yOff2,-0.3);

  for (int i=0;i<particles.size();i++) ((Particle)particles.elementAt(i)).show();

  if (keyPressed) if (key=='n') println(particles.size());
  
  if (keyPressed) if (key==' ') {
    int m = (int)random(3);
    for (int x=0;x<width;x++) for (int y=0;y<height;y++) {
      int c = get(x,y);
      int r = c>>16&0xFF;
      int g = c>>8&0xFF;
      int b = c&0xFF;
      if (m!=0) { 
        r++; 
        if (r>255) r=255; 
      }
      if (m!=1) { 
        g++; 
        if (g>255) g=255; 
      }
      if (m!=2) { 
        b++; 
        if (b>255) b=255; 
      }
      set(x,y,r<<16|g<<8|b);
    }
  }
}
