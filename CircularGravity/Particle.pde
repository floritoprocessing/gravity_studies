class Particle {
  
  private float x,y;
  private float xm=0, ym=0;
  private float young = 1.0;
  private float DRAG = 0.9;
  private float AGING_SPEED = 0.003;
  private int[] COLORS = { 0x000000, 0x500000, 0x005000, 0x000050, 0x000050, 0x000050 };
  private int COLOR_VARIATION = 32;
  private int col = COLORS[0];
  
  Particle(float _x, float _y, float _xm, float _ym) {
    int i = int(COLORS.length * noise(10*_x/(float)width,10*_y/(float)height,millis()/5000.0));
    col = COLORS[i];
    int r = (col>>16&0xFF) + int(random(COLOR_VARIATION));
    int g = (col>>8&0xFF) + int(random(COLOR_VARIATION));
    int b = (col&0xFF) + int(random(COLOR_VARIATION));
    col = r<<16|g<<8|b;
    x = _x;
    y = _y;
    xm = _xm;
    ym = _ym;
  }
  
  public void update(Vector ps, float xCen, float yCen, float force) {
    x += xm;
    y += ym;
    
    float dx = force*(xCen-x);
    float dy = force*(yCen-y);
    float d = sqrt(dx*dx+dy*dy);
    xm -= 0.001*dx*young;    
    ym -= 0.001*dy*young;
    xm +=  random(-0.3,0.3);
    ym +=  random(-0.3,0.3);
    xm *= DRAG;
    ym *= DRAG;
    
    float speed=sqrt(xm*xm+ym*ym);
    
    young -= AGING_SPEED*speed;
    if (young<0.0) ps.remove(this);
  }
  
  public void show() {
    noStroke();
    fill(col>>16&0xFF,col>>8&0xFF,col&0xFF,64*young);
    ellipseMode(CENTER);
    ellipse(x,y,3*young,3*young);
  }
}
