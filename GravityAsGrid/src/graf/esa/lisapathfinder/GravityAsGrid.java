package graf.esa.lisapathfinder;

import graf.astro.Astro;
import graf.math.vector.Vec;
import graf.particle.Particle;
import processing.core.PApplet;

public class GravityAsGrid extends PApplet {

	public static void main(String[] args) {
		PApplet.main(new String[] {"graf.esa.lisapathfinder.GravityAsGrid"});
		PApplet.main(new String[] {"graf.esa.lisapathfinder.GravityAsGrid"});
	}

	private final Particle[] sun = new Particle[] {
			new Particle(new Vec(0,0,0),new Vec(-0.2,0,0.1),800),
			new Particle(new Vec(50,0,-220), 10),
			new Particle(new Vec(40,0,70), 10)
			//new Particle(new Vec(-220,0,50),new Vec(0.22,0,-0.4), 400)
	};

	private final float gridEnd = 500;
	private final int gridDivisions = 200;
	private final int gridDrawingDivider = 1;
	private final double integrationTime = 1f;//10.0;

	Vec[][] gridPoint = new Vec[gridDivisions][gridDivisions];
	float[][] gridOpacity = new float[gridDivisions][gridDivisions];

	public void settings() {
		size(800,600,P3D);
	}
	public void setup() {
		
		//smooth();

		for (int i=1;i<sun.length;i++) {
			Vec orbVec = Vec.sub(sun[0].getPosition(),sun[i].getPosition());
			double distanceToSun = orbVec.length();
			double orbitalVelocity = Astro.orbitalVelocityCircular(sun[0].getMass(), distanceToSun);
			orbVec.rotY(0.5*Math.PI);
			orbVec.setLength(orbitalVelocity);
			sun[i].setMovement(orbVec);
		}

		for (int i=0;i<gridDivisions;i++) {
			double px = (double)i/(gridDivisions-1);
			double dx = 0.5-px;
			for (int j=0;j<gridDivisions;j++) {
				double pz = (double)j/(gridDivisions-1);
				double dz = 0.5-pz; 
				double dCen = 2 * Math.min(0.5, Math.sqrt(dx*dx+dz*dz));
				dCen = 1 - Math.pow(dCen,4);
				gridOpacity[i][j] = (float)(128 * dCen);
				double x = gridEnd * (2.0*px-1);
				double y = 0;
				double z = gridEnd * (2.0*pz-1);
				gridPoint[i][j] = new Vec(x,y,z);
			}
		}
		//calcGravityOnGrid();
	}

	public void draw() {
		background(0);
		
		ambientLight(50,50,50);
		lightSpecular(204, 204, 204); 
		directionalLight(100,100,100,1,1,-1);
		
		Vec eye = new Vec(0,-gridEnd*0.2 -200 - 200*Math.sin(Math.PI*frameCount*integrationTime/400.0-0.5*Math.PI),gridEnd*1.1);
		eye.rotY(frameCount*0.005*integrationTime);
		//Vec target = sun[0].getPos();
		Vec target = new Vec();
		camera((float)eye.x, (float)eye.y, (float)eye.z, (float)target.x, (float)target.y, (float)target.z, 0,1,0);
		calcGravityOnGrid();
		drawGrid(false);
		drawSuns();


		for (int c=0;c<sun.length;c++) {
			if (c==0)
				sun[c].integrate(integrationTime,new Vec());
			else {
				Vec toSun = Vec.sub(sun[0].getPosition(),sun[c].getPosition());
				double r = toSun.length();
				double r2 = r*r;
				double accLength = sun[0].getMass() / r2;
				toSun.setLength(accLength);
				sun[c].integrate(integrationTime, toSun);
			}
		}
		
		//println(frameCount);
		
//		saveFrame("output/GravityAsGrid_preview_####.jpg");
	}

	private void drawSuns() {
		specular(255, 255, 255);
		shininess(5.0f);
		fill(255,255,0);
		//stroke(255,255,0);
		//sphereDetail(8);
		noStroke();
		for (int c=0;c<sun.length;c++) {
			pushMatrix();
			float x = (float)sun[c].getPosition().x;
			float y = (float)sun[c].getPosition().y;
			float z = (float)sun[c].getPosition().z;
			translate(x,y,z);
			

			//V = 4.0/3.0*Math.PI*r*r*r;
			double volume = sun[c].getMass();
			double radius = 8 * Math.pow(volume*3.0/(4.0*Math.PI), 1/3.0);

			sphere((float)radius);
			popMatrix();
		}
	}

	private void calcGravityOnGrid() {
		for (int i=0;i<gridDivisions;i++) for (int j=0;j<gridDivisions;j++) {

			Vec totalAcc = new Vec();
			for (int c=0;c<sun.length;c++) {
				Vec point = new Vec(gridPoint[i][j]);
				point.setY(0);
				Vec dToSun = Vec.sub(sun[c].getPosition(), point);
				double r2 = dToSun.lengthSquared();
				double Fdm = sun[c].getMass()/r2;
				Vec acc = Vec.mul(Vec.normalize(dToSun),Fdm);
				totalAcc.add(acc);
			}
			double totalForce = totalAcc.length();
			gridPoint[i][j].setY(100*totalForce + 25);

		}
	}

	private void drawGrid(boolean wireframe) {
		if (wireframe) {
			noFill();		
			for (int i=0;i<gridDivisions;i+=gridDrawingDivider) {
				beginShape();
				for (int j=0;j<gridDivisions-1;j++) {
					Vec v0 = gridPoint[i][j];
					Vec v1 = gridPoint[i][j+1];
					stroke(64,64,255,gridOpacity[i][j]);
					vertex((float)v0.x,(float)v0.y,(float)v0.z);
					stroke(64,64,255,gridOpacity[i][j+1]);
					vertex((float)v1.x,(float)v1.y,(float)v1.z);

				}
				endShape();
			}
			for (int j=0;j<gridDivisions;j+=gridDrawingDivider) {
				beginShape();
				for (int i=0;i<gridDivisions-1;i++) {
					Vec v0 = gridPoint[i][j];
					Vec v1 = gridPoint[i+1][j];
					stroke(64,64,255,gridOpacity[i][j]);
					vertex((float)v0.x,(float)v0.y,(float)v0.z);
					stroke(64,64,255,gridOpacity[i+1][j]);
					vertex((float)v1.x,(float)v1.y,(float)v1.z);
				}
				endShape();
			}
		}
		else {
			//stroke(0);
			noStroke();
			beginShape(QUADS);
			specular(0,0,255);
			shininess(1.0f);
			for (int i=0;i<gridDivisions-gridDrawingDivider-1;i+=gridDrawingDivider) for (int j=0;j<gridDivisions-gridDrawingDivider-1;j+=gridDrawingDivider) {
				Vec v = gridPoint[i][j];
				fill(0,0,gridOpacity[i][j],150);
				vertex((float)v.x,(float)v.y,(float)v.z);
				v = gridPoint[i][j+gridDrawingDivider];
				fill(0,0,gridOpacity[i][j+gridDrawingDivider],150);
				vertex((float)v.x,(float)v.y,(float)v.z);
				v = gridPoint[i+gridDrawingDivider][j+gridDrawingDivider];
				fill(0,0,gridOpacity[i+gridDrawingDivider][j+gridDrawingDivider],150);
				vertex((float)v.x,(float)v.y,(float)v.z);
				v = gridPoint[i+gridDrawingDivider][j];
				fill(0,0,gridOpacity[i+gridDrawingDivider][j],150);
				vertex((float)v.x,(float)v.y,(float)v.z);
			}
			endShape();
		}
	}
}

