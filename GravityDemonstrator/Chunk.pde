class Chunk {
  Vec position;
  Vec movement;
  Vec acceleration;
  Vec acc=new Vec();
  color col=color(random(128,255),random(128,255),random(128,255));
  color colAlp=color(red(col),green(col),blue(col),40);
  boolean drawTraces=true;
  Chunk tmpChunk;
  float radius;
  double mass;
  
  double MASS_FACTOR=0.2;
  
  Vec[] pastPositions;
  int maxPastPositions=20;


  Chunk(float mW, float mH) {
    position=new Vec(random(-0.5*mW,0.5*mW),random(-0.1*mH,0.1*mH),random(-0.5*mW,0.5*mW));
    movement=new Vec(random(-1,1),random(-0.1,0.1),random(-1,1));
    movement.mul(MASS_FACTOR*10.0);
    acceleration=new Vec(0,0,0);
    radius=random(0.5,2.0);
    mass=4/3.0 * PI * pow(radius,3) / 10.0;
    pastPositions=new Vec[maxPastPositions];
    for (int i=0;i<maxPastPositions;i++) {
      pastPositions[i]=new Vec(position);
    }
  }

  void noTrace() { drawTraces=false; }
  
  void trace() { drawTraces=true; }

  void update(Vector cs) {    
    for (int i=maxPastPositions-1;i>0;i--) pastPositions[i].setVec(pastPositions[i-1]);
    pastPositions[0].setVec(position);
    acceleration.setVec(0,0,0);
    for (int i=0;i<cs.size();i++) {
      tmpChunk=(Chunk)(cs.elementAt(i));
      Vec oneAcc=makeAcceleration(position,tmpChunk.position,MASS_FACTOR*tmpChunk.mass,sq(radius+tmpChunk.radius));//sq(radius+tmpChunk.radius));
      if (position!=tmpChunk.position) acceleration.add(oneAcc);
    }
    movement.add(acceleration);
    position.add(movement);  
  }

  void toScreen() {
    
    for (int i=0;i<maxPastPositions;i++) {
      if (i==0) {
        fill(col);
        noStroke();
        pushMatrix();
        vecTranslate(pastPositions[i]);    
        ellipse(0,0,radius,radius);
        popMatrix();
      } else {
        if (drawTraces) {
          stroke(colAlp);
          noFill();
          vecLine(pastPositions[i],pastPositions[i-1]);
        }
      }
    }
  }

  Vec makeAcceleration(Vec obj1, Vec obj2, double mass2, double minDist2) {
    double dx=obj2.x-obj1.x;
    double dy=obj2.y-obj1.y;
    double dz=obj2.z-obj1.z;
    double d2=dx*dx+dy*dy+dz*dz;
    double F=0;
    if (d2>=minDist2) F=mass2/d2;
    Vec a=new Vec(dx*F,dy*F,dz*F);
    return a;
  }
}

