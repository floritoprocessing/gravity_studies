// Gravity Demonstrator
import java.util.Vector;

Vector chunks;
Chunk chunk;

void setup() {
  size(640,480,P3D);
  ellipseMode(RADIUS);
  noStroke();
  fill(255);
  chunks=new Vector();
  
  for (int i=0;i<300;i++) addChunk();
}

void draw() {
  background(0);
  
  Vec averagePos=new Vec(0,0,0);
  for (int i=0;i<chunks.size();i++) {
    chunk=(Chunk)(chunks.elementAt(i));
    chunk.update(chunks);
    averagePos.add(chunk.position);
  }
  averagePos.div(float(chunks.size()));
  
  translate(width/2.0,height/2.0,-500);
  rotateX(-PI/8.0);
  for (int i=0;i<chunks.size();i++) {
    chunk=(Chunk)(chunks.elementAt(i));
    chunk.position.sub(averagePos);
    chunk.toScreen();
  }
  
  //saveFrame("./../../temp/Orbit######.tga");
}

void addChunk() {
  chunk=new Chunk(width,height);
  chunk.noTrace();
  chunks.add(chunk);
}
