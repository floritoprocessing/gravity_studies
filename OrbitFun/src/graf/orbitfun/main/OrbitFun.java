package graf.orbitfun.main;

import java.util.Date;

import processing.core.*;

import graf.orbitfun.physics.Physics;



public class OrbitFun extends PApplet {

	public static void main(String[] args) {
		PApplet.main(new String[] {"graf.orbitfun.main.OrbitFun"});
	}
	
	static int PARTICLE_AMOUNT = 700;
	static int LINE_LENGTH = 30;
	static int LINE_DELAY = 3;
	
	float[][][] line;
	int camMode=0;
	boolean saveOnNextFrame = false;
	
	Physics engine;
	
	public void settings() {
		size(800,800,P3D);
	}
	
	public void setup() {
		background(0);
		smooth();
		strokeWeight(1);
		engine = new Physics();
		for (int i=0;i<5;i++)
			engine.add(Physics.createGravityCenter(random(-400,400),random(-400,400),random(-400,400),1000,150));
		for (int i=0;i<PARTICLE_AMOUNT;i++)
			engine.add(Physics.createParticle(random(-200,200),random(-200,200),random(-200,200),random(-.5f,.5f),random(-.5f,.5f),random(-.5f,.5f)));
		line = new float[PARTICLE_AMOUNT][LINE_LENGTH*LINE_DELAY][3];
		for (int i=0;i<line.length;i++)
			for (int j=0;j<line[0].length;j++)
				line[i][j] = null;
	}
	
	public void mousePressed() {
		if (line[0][LINE_LENGTH*LINE_DELAY-1]!=null) {
			background(0);
			camMode=1-camMode;
		}
	}
	
	public void keyPressed() {
		if (key=='s') saveOnNextFrame=true;
	}
	
	public void draw() {
		
				
		engine.tick();
		float[][] pos = engine.getSlavePositions3fa();
		for (int i=0;i<pos.length;i++) {
			//point(pos[i][0],pos[i][1],pos[i][2]);
			for (int j=line[i].length-1;j>0;j--)
				line[i][j] = line[i][j-1];
			line[i][0]=pos[i];
		}
		
		//background(0);
		if (camMode==0)
			camera(0,0,1000,0,0,0,0,1,0);
		else {
			float[] p1 = line[0][LINE_DELAY-1];
			float[] p2 = line[0][0];
			camera(p1[0],p1[1],p1[2],p2[0],p2[1],p2[2],0,1,0);
		}
		
		
		for (int i=0;i<line.length;i++) {
			for (int p=0;p<LINE_LENGTH-1;p++) {
				float po = (float)p/LINE_LENGTH;	// 0..0.3..0.6..1.0
				float pg = max(0,po*4);				// 0..1.0..1.0..1.0
				float pr = max(0,po);
				stroke(255-255*pr,255-255*pg,0,0.1f*(255 - (255*po)));
				float[] p1 = line[i][p*LINE_DELAY];
				float[] p2 = line[i][(p+1)*LINE_DELAY];
				if (p1!=null&&p2!=null)
					line(p1[0],p1[1],p1[2],p2[0],p2[1],p2[2]);
			}
		}
		
		if (saveOnNextFrame) {
			saveOnNextFrame = false;
			Date d = new Date();
			int year = 1900+d.getYear();
			int month = d.getMonth();
			int date = d.getDate();
			int hh = d.getHours();
			int mm = d.getMinutes();
			int ss = d.getSeconds();
			String name = "OrbitFun_";
			name += nf(year,4)+"-"+nf(month,2)+"-"+nf(date,2)+" ";
			name += nf(hh,2)+nf(mm,2)+nf(ss,2)+" ";
			name += nf(frameCount,5);
			name += ".tga";
			saveFrame("C:\\Documents and Settings\\mgraf\\My Documents\\Processing Marcus Output\\"+name);
			System.out.println("saved");
		}
		
		saveFrame("output/OrbitFun_###.jpg");
	}
	
}
