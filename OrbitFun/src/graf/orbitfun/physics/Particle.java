package graf.orbitfun.physics;

public class Particle {
	
	public static final int TYPE_SLAVE = 0;
	public static final int TYPE_MASTER = 1;
		
	public double x=0, y=0, z=0;
	public double xm=0, ym=0, zm=0;
	public double mass=1;
	public int type = TYPE_SLAVE;
	public double minimumDistance=0.001;
	
	Particle(double x, double y) {
		this.x = x;
		this.y = y;
	}
	
	Particle(double x, double y, double xm, double ym) {
		this(x,y);
		this.xm=xm;
		this.ym=ym;
	}
	
	public Particle(double x, double y, double z, double xm, double ym, double zm) {
		this.x=x;
		this.y=y;
		this.z=z;
		this.xm=xm;
		this.ym=ym;
		this.zm=zm;
	}

	public Particle(double x, double y, double z, double mass, double minimumDistance, int type) {
		this(x,y);
		this.z = z;
		this.mass=mass;
		this.minimumDistance = minimumDistance;
		this.type=type;
	}

	

	public float[] getPos2fa() {
		return new float[] { (float)x, (float)y };
	}
	
	public float[] getPos3fa() {
		return new float[] { (float)x, (float)y, (float)z };
	}
}
