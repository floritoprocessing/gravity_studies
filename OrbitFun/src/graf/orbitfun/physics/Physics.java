package graf.orbitfun.physics;

import java.util.Vector;

public class Physics {
	
	Vector slaves = new Vector();
	Vector masters = new Vector();
	
	public Physics() {
	}
	
	public void tick() {
		for (int i=0;i<slaves.size();i++) {
			Particle p = (Particle)slaves.elementAt(i);
			
			double ax=0, ay=0, az=0;
			
			for (int j=0;j<masters.size();j++) {
				Particle m = (Particle)masters.elementAt(j);
				double dx = m.x-p.x, dy = m.y-p.y, dz=m.z-p.z;
				double r2 = dx*dx+dy*dy+dz*dz;
				double r = Math.sqrt(r2);
				if (r<m.minimumDistance) r=m.minimumDistance;
				double a = m.mass/r2;
				ax += a * dx/r;
				ay += a * dy/r;
				az += a * dz/r;
			}
			p.xm += ax;
			p.ym += ay;
			p.zm += az;
			
			p.x += p.xm;
			p.y += p.ym;
			p.z += p.zm;
		}
	}

	
	public static Particle createParticle(double x, double y) {
		return new Particle(x,y);
	}
	
	public static Particle createParticle(double x, double y, double xm, double ym) {
		return new Particle(x,y,xm,ym);
	}
	
	public static Particle createParticle(double x, double y, double z, double xm, double ym, double zm) {
		return new Particle(x,y,z,xm,ym,zm);
	}
	
	public static Particle createGravityCenter(double x, double y, double z, double mass, double minimumDistance) {
		return new Particle(x,y,z,mass,minimumDistance,Particle.TYPE_MASTER);
	}

	public void add(Particle p) {
		if (p.type==Particle.TYPE_MASTER)
			masters.add(p);
		else
			slaves.add(p);
	}

	public float[][] getSlavePositions2fa() {
		float[][] pos = new float[slaves.size()][2];
		for (int i=0;i<slaves.size();i++) {
			pos[i] = ((Particle)slaves.elementAt(i)).getPos2fa();
		}
		return pos;
	}

	public float[][] getSlavePositions3fa() {
		float[][] pos = new float[slaves.size()][3];
		for (int i=0;i<slaves.size();i++) {
			pos[i] = ((Particle)slaves.elementAt(i)).getPos3fa();
		}
		return pos;
	}

	


	
}
