package graf.OrbitFun2008;

import processing.core.*;
import graf.gravity.*;
import graf.timing.Timer;
import graf.graphic.GraphicTool;

public class OrbitFun2008 extends PApplet {

	public static void main(String[] args) {
		PApplet.main(new String[] {"graf.OrbitFun2008.OrbitFun2008"});
	}
	
	private String output = "output/OrbitFun2008_150bpm_####.jpg";
	private boolean saveImage = false;
	
	private int BPM = 150;
	private double framesPerBeat;
	
	private int AMOUNT = 400000;
	double timeStep = 0.5;
	double timeStepVariation = 0;
	private double G = 1.0;
	private int stepsPerFrame = 5;
	private int colorParticle = 0xFF903B;
	private double opacityParticle = 0.2;
	private double opacityStep = 0.85;
	
	private double massSun = 1500;
	private double massJupiter = 15;
	
	private boolean rotatingJupiterFrame = true;
	
	private double zoom = 1.0;
	
	private Mass sun;
	private Mass jupiter;
	private Mass jupiter2;
	private Mass[] particle;
	private Timer timer = new Timer();
	
	
	
	
	public void settings() {
		size(768,576,P3D);
	}
	
	
	
	public void setup() {
		
		
		background(0);
		
		double bps = (BPM/60.0);
		double spb = 1.0/bps;
		framesPerBeat = spb*25;
		System.out.println(framesPerBeat);
				
		sun = new Mass(massSun,width/2,height/2);
		jupiter = new Mass(massJupiter,0,-height*.25);
		jupiter2 = new Mass(massJupiter,0,height*.251);
		particle = new Mass[AMOUNT];
		
		for (int i=0;i<particle.length;i++) {
			double x = 1 + width/2 * Math.sqrt(2) * Math.pow(Math.random(),2/3.0);
			double y = 0;
			double xm = 0;
			double ym = GravityTool.orbitalSpeedCircular(G*sun.getMass(), x);
			double rd = 2 * Math.PI * Math.random();
			double SIN=Math.sin(rd);
		    double COS=Math.cos(rd); 
		    double xn=x*COS-y*SIN; 
		    double yn=y*COS+x*SIN;
		    x=xn + width/2;
		    y=yn + height/2;
		    xn=xm*COS-ym*SIN; 
		    yn=ym*COS+xm*SIN;
		    particle[i] = new Mass(0.001,x,y);
		    particle[i].setMovementX(xn * (0.999+0.002*Math.random()));
		    particle[i].setMovementY(yn * (0.999+0.002*Math.random()));
		}
		
		double xm = GravityTool.orbitalSpeedCircular(G*sun.getMass(), Math.abs(jupiter.getY()));
		jupiter.setX(jupiter.getX()+width/2);
		jupiter.setY(jupiter.getY()+height/2);
		jupiter.setMovementX(xm);
		jupiter2.setX(jupiter2.getX()+width/2);
		jupiter2.setY(jupiter2.getY()+height/2);
		jupiter2.setMovementX(-xm);
		
		//frameRate(25);
	}
	
	
	public void draw() {

		//zoom = 1.0 + Math.pow(0.5 - 0.5*Math.cos(2*Math.PI*frameCount/25.0),2);
		//System.out.println(zoom);
		//System.out.println(frameRate);
		
		for (int ff=0;ff<stepsPerFrame;ff++) {
			
			double time = frameCount + (double)ff/stepsPerFrame;
			double pulsing = Math.pow(0.5 - 0.5*Math.cos(2*Math.PI*time/framesPerBeat),2);
			double halfPulse = 0.5 - 0.5*Math.cos(Math.PI*time/framesPerBeat);
			
			zoom = 1.0 + 0.2*pulsing + 0.2*halfPulse;
			
			double dt = timeStep - (Math.random()-0.5) * timeStepVariation;
			
			double[] accJup = GravityTool.createAcceleration(jupiter, sun, G);
			jupiter.accelerate(accJup[0], accJup[1], dt);
			jupiter.move(dt);
			
			accJup = GravityTool.createAcceleration(jupiter2, sun, G);
			jupiter2.accelerate(accJup[0], accJup[1], dt);
			jupiter2.move(dt);
			

			for (int i=0;i<particle.length;i++) {
				double[] acc1 = GravityTool.createAcceleration(particle[i],sun,G);
				double[] acc2 = GravityTool.createAcceleration(particle[i],jupiter,G);
				double[] acc3 = GravityTool.createAcceleration(particle[i],jupiter2,G);
				particle[i].accelerate(acc1[0]+acc2[0]+acc3[0], acc1[1]+acc2[1]+acc3[1], dt);
				particle[i].move(dt);
			}
			
			
			double jupRot = Math.atan2(jupiter.getY()-height/2,jupiter.getX()-width/2);
			
			
			double pp = 1 - opacityStep;
			for (int x=0;x<width;x++) for (int y=0;y<height;y++) set(x,y,GraphicTool.blend(0x000000,get(x, y),pp,GraphicTool.BLEND_MODE_NORMAL));
			
			double SIN=Math.sin(-jupRot);
		    double COS=Math.cos(-jupRot);
			for (int i=0;i<particle.length;i++) {
				double xd = (int)particle[i].getX();
				double yd = (int)particle[i].getY();
				if (rotatingJupiterFrame ) {
					xd -= width/2.0;
					yd -= height/2.0;
					double xn=xd*COS-yd*SIN; 
					double yn=yd*COS+xd*SIN;
					xd = xn + width/2.0;
					yd = yn + height/2.0;
				}
				xd -= width/2.0;
				yd -= height/2.0;
				xd *= zoom;
				yd *= zoom;
				xd += width/2.0;
				yd += height/2.0;
				//set(x,y,GraphicTool.blend(colorParticle,get(x,y),opacity,GraphicTool.BLEND_MODE_ADD));
				GraphicTool.set(this,xd,yd,colorParticle,opacityParticle * (zoom-0.4) ,GraphicTool.BLEND_MODE_ADD);
			}
			
			
		}
		
		
		
		
		
		
		
		
		
		
		
		/*if (frameCount==1 || peak) {
			//System.out.println(frameCount);
			saveFrame("orbitFun2008-#####.tga");
		}*/
		
		/*pg.ellipseMode(CENTER);
		pg.noStroke();
		pg.fill(255,255,0);
		pg.ellipse((float)sun.getX(),(float)sun.getY(),20,20);
		
		pg.fill(128,255,128);
		pg.ellipse((float)jupiter.getX(),(float)jupiter.getY(),10,10);
		pg.endDraw();*/
		
		if (saveImage) saveFrame(output);
		
	}
}
