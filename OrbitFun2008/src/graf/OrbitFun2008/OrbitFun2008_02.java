package graf.OrbitFun2008;

import processing.core.*;
import graf.gravity.*;
import graf.timing.Timer;
import graf.graphic.GraphicTool;

public class OrbitFun2008_02 extends PApplet {

	public static void main(String[] args) {
		PApplet.main(new String[] {"graf.OrbitFun2008.OrbitFun2008_02"});
	}
	
	private String output = "C:\\Documents and Settings\\mgraf\\My Documents\\Processing Marcus Output\\OrbitFun2008_02\\OrbitFun2008_02_150bpm_#####.tga";
	private boolean saveImage = false;
	
	private int BPM = 150;
	private double framesPerBeat;
	
	private int AMOUNT = 120000; //400000
	double timeStep = 0.1;
	double timeStepVariation = 0;
	private double G = 10.0;
	private int stepsPerFrame = 8;
	private int colorParticle = 0xFF903B;
	private double opacityParticle = 0.1; // 0.2
	private double opacityStep = 0.85;    // 0.85
	
	private double massSun = 50;
	private double massJupiter = 15;
	
	private boolean rotatingJupiterFrame = false;
	
	private double zoom = 1.0;
	
	private Mass sun;
	private Mass jupiter;
	private Mass jupiter2;
	private Mass[] particle;
	private Timer timer = new Timer();
	
	private boolean up = true;
	
	
	
	public void settings() {
		size(768/2,576/2,P3D);
	}
	
	public void setup() {
		
		
		background(0);
		
		double bps = (BPM/60.0);
		double spb = 1.0/bps;
		framesPerBeat = spb*25;
		System.out.println(framesPerBeat);
				
		sun = new Mass(massSun,width/2,height/2);
		jupiter = new Mass(massJupiter,0,-height*.4);
		jupiter2 = new Mass(massJupiter,0,height*.4001);
		particle = new Mass[AMOUNT];
		
		for (int i=0;i<particle.length;i++) {
			resetParticle(i);
		}
		
		double xm = GravityTool.orbitalSpeedCircular(G*sun.getMass(), Math.abs(jupiter.getY()));
		jupiter.setX(jupiter.getX()+width/2);
		jupiter.setY(jupiter.getY()+height/2);
		jupiter.setMovementX(xm);
		jupiter2.setX(jupiter2.getX()+width/2);
		jupiter2.setY(jupiter2.getY()+height/2);
		jupiter2.setMovementX(-xm);
		
		//frameRate(25);
	}
	
	
	public void resetParticle(int i) {
		double x = 1 + width/2 * Math.sqrt(2) * Math.pow(Math.random(),2/3.0);
		double y = 0;
		double xm = 0;
		double ym = GravityTool.orbitalSpeedCircular(G*sun.getMass(), x);
		double rd = 2 * Math.PI * Math.random();
		double SIN=Math.sin(rd);
	    double COS=Math.cos(rd); 
	    double xn=x*COS-y*SIN; 
	    double yn=y*COS+x*SIN;
	    x=xn + width/2;
	    y=yn + height/2;
	    xn=xm*COS-ym*SIN; 
	    yn=ym*COS+xm*SIN;
	    particle[i] = new Mass(0.001,x,y);
	    particle[i].setMovementX(xn * (0.999+0.002*Math.random()));
	    particle[i].setMovementY(yn * (0.999+0.002*Math.random()));
	}
	
	
	
	
	
	
	public void draw() {

		//zoom = 1.0 + Math.pow(0.5 - 0.5*Math.cos(2*Math.PI*frameCount/25.0),2);
		//System.out.println(zoom);
		//System.out.println(frameRate);
		
		for (int ff=0;ff<stepsPerFrame;ff++) {
			
			/*double time = frameCount + (double)ff/stepsPerFrame;
			double pulsing = Math.pow(0.5 - 0.5*Math.cos(2*Math.PI*time/framesPerBeat),2);
			pulsing = pulsing<0.5?0.0:pulsing/8;
			double halfPulse = 0.5 - 0.5*Math.cos(Math.PI*time/framesPerBeat);
			halfPulse = halfPulse<0.8?0.0:halfPulse/8;
			
			zoom = 1.5;// + 0.2*pulsing + 0.2*halfPulse;
*/			
			double dt = timeStep - (Math.random()-0.5) * timeStepVariation;
			
			double[] accJup = GravityTool.createAcceleration(jupiter, sun, G);
			jupiter.accelerate(accJup[0], accJup[1], dt);
			jupiter.move(dt);
			
			accJup = GravityTool.createAcceleration(jupiter2, sun, G);
			jupiter2.accelerate(accJup[0], accJup[1], dt);
			jupiter2.move(dt);
			

			for (int i=0;i<particle.length;i++) {
				double[] acc1 = GravityTool.createAcceleration(particle[i],sun,G);
				double[] acc2 = GravityTool.createAcceleration(particle[i],jupiter,G);
				double[] acc3 = GravityTool.createAcceleration(particle[i],jupiter2,G);
				double dx = sun.getX() - particle[i].getX();
				double dy = sun.getY() - particle[i].getY();
				double d = Math.sqrt(dx*dx+dy*dy);
				//double[] acc4 = { -dx/d*(pulsing+halfPulse), -dy/d*(pulsing+halfPulse) };
				particle[i].accelerate(acc1[0]+acc2[0]+acc3[0], acc1[1]+acc2[1]+acc3[1], dt);
				particle[i].move(dt);
				
				if (particle[i].getX()<-0.5*width||particle[i].getX()>1.5*width||
						particle[i].getY()<-0.5*height||particle[i].getY()>1.5*height) {
					resetParticle(i);
				}
			}
			
			
			
			
			double jupRot = Math.atan2(jupiter.getY()-height/2,jupiter.getX()-width/2);
			
			
			double pp = 1 - opacityStep;
			for (int x=0;x<width;x++) for (int y=0;y<height;y++) set(x,y,GraphicTool.blend(0x000000,get(x, y),pp,GraphicTool.BLEND_MODE_NORMAL));
			
			double SIN=Math.sin(-jupRot);
		    double COS=Math.cos(-jupRot);
			for (int i=0;i<particle.length;i++) {
				double xd = particle[i].getX();
				double yd = particle[i].getY();
				if (rotatingJupiterFrame ) {
					xd -= width/2.0;
					yd -= height/2.0;
					double xn=xd*COS-yd*SIN; 
					double yn=yd*COS+xd*SIN;
					xd = xn + width/2.0;
					yd = yn + height/2.0;
				}
				xd -= width/2.0;
				yd -= height/2.0;
				xd *= zoom;
				yd *= zoom;
				xd += width/2.0;
				yd += height/2.0;
				//set(x,y,GraphicTool.blend(colorParticle,get(x,y),opacity,GraphicTool.BLEND_MODE_ADD));
				GraphicTool.set(this,xd,yd,colorParticle,opacityParticle * (zoom-0.4) ,GraphicTool.BLEND_MODE_ADD);
			}
			
			
			if (up) {
				sun.setMass(sun.getMass()*1.0018);
				jupiter.setMass(jupiter.getMass()*1.0002);
				jupiter2.setMass(jupiter2.getMass()*1.0002);
				if (sun.getMass()>100) {
					//sun.setMass(100);
					up = false;
				}
			} else {
				sun.setMass(sun.getMass()/1.0018);
				jupiter.setMass(jupiter.getMass()/1.0002);
				jupiter2.setMass(jupiter2.getMass()/1.0002);
				if (sun.getMass()<50) {
					//sun.setMass(50);
					up = true;
				}
			}
			
			
			
		}
		
		
		
		
		
		
		
		
		
		
		
		/*if (frameCount==1 || peak) {
			//System.out.println(frameCount);
			saveFrame("orbitFun2008-#####.tga");
		}*/
		
		
		//endDraw();
		
		if (saveImage) saveFrame(output);
		System.out.println(frameCount+": "+sun.getMass());
		
		//ellipseMode(CENTER);
		//noStroke();
		/*fill(255,255,0);
		ellipse((float)sun.getX(),(float)sun.getY(),20,20);*/
		
		/*fill(128,255,128);
		ellipse((float)jupiter.getX(),(float)jupiter.getY(),10,10);
		ellipse((float)jupiter2.getX(),(float)jupiter2.getY(),10,10);*/
	}
}
