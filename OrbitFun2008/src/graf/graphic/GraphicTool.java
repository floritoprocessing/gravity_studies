package graf.graphic;

import processing.core.*;
import graf.OrbitFun2008.OrbitFun2008;

public class GraphicTool {

	public static final int BLEND_MODE_NORMAL = 0;
	public static final int BLEND_MODE_ADD = 1;
	
	public static int red(int color) {
		return color>>16&0xFF;
	}
	
	public static int green(int color) {
		return color>>8&0xFF;
	}
	
	public static int blue(int color) {
		return color&0xFF;
	}
	
	public static int max(double br) {
		return (int)(br>255?255:br);
	}
	
	public static int color(int r, int g, int b) {
		return 0xFF<<24|r<<16|g<<8|b;
	}
	
	public static int mix(int color1, int color2, double percentage1, double percentage2) {
		int r = max(percentage1*red(color1)+percentage2*red(color2));
		int g = max(percentage1*green(color1)+percentage2*green(color2));
		int b = max(percentage1*blue(color1)+percentage2*blue(color2));
		return color(r,g,b);
	}
	
	public static int blend(int color1, int color2, double percentage1, int blendMode) {
		switch (blendMode) {
			case BLEND_MODE_NORMAL: return mix(color1,color2,percentage1,1.0-percentage1);
			case BLEND_MODE_ADD: return mix(color1,color2,percentage1,1.0);
			default: throw new RuntimeException("Blend mode not recognized!");
		}
	}

	public static int get(PApplet pa, double x, double y) {
		return (pa.get((int)x,(int)y));
	}

	
	public static void set(PApplet pa, double x, double y, int color, double percentage, int blendMode) {
		if (x<0||y<0) return;
		int x0 = (int)x, x1 = x0+1;
		int y0 = (int)y, y1 = y0+1;
		double px1 = x-x0, px0 = 1-px1;
		double py1 = y-y0, py0 = 1-py1;
		double p00 = percentage*px0*py0;
		double p01 = percentage*px0*py1;
		double p10 = percentage*px1*py0;
		double p11 = percentage*px1*py1;
		pa.set(x0,y0,blend(color,pa.get(x0, y0),p00,blendMode));
		pa.set(x0,y1,blend(color,pa.get(x0, y1),p01,blendMode));
		pa.set(x1,y0,blend(color,pa.get(x1, y0),p10,blendMode));
		pa.set(x1,y1,blend(color,pa.get(x1, y1),p11,blendMode));
		//pa.set((int)x, (int)y, c);
	}
	
}
