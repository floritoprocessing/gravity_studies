package graf.gravity;

public class GravityTool {
	
	public static final int DIMENSIONS_2 = 2;
	public static final int DIMENSIONS_3 = 3;

	/**
	 * Gravitational constant in m^3 kg^-1 s^-2. <br>
	 * Accuracy is 0.00067 * 10^-11
	 */
	public static final double G = 6.67428 * Math.pow(10, -11);
	
	/**
	 * Returns the orbital speed
	 * @param GM standard gravitational parameter
	 * @param r distance between orbiting body and central body
	 * @return
	 */
	public static double orbitalSpeedCircular(double GM, double r) {
		return Math.sqrt(GM/r);
	}
	
	/**
	 * Returns the orbital speed
	 * @param GM standard gravitational parameter
	 * @param r distance between orbiting body and central body
	 * @param a semi-major axis
	 * @return
	 */
	public static double orbitalSpeedElliptic(double GM, double r, double a) {
		return Math.sqrt(GM * (2/r-1/a));
	}

	public static double[] createAcceleration(Mass m, Mass center, double gravitationalConstant) {
		
		if (m.dimension!=center.dimension) throw new RuntimeException("Two objects are not in the same dimension");
		
		double dx = center.x - m.x;
		double dy = center.y - m.y;
		double dz = 0;
		if (m.in3D()) dz = center.z - m.z;
		
		double r = Math.sqrt(dx*dx+dy*dy+dz*dz);
		if (r==0) r=Double.MIN_VALUE;
		double invR3 = 1/(r*r*r);
		double mu = gravitationalConstant * center.mass * invR3;
		
		double accX = dx * mu;
		double accY = dy * mu;
		
		if (m.in2D()) {
			return new double[] {accX, accY};
		} else {
			double accZ = dz * mu;
			return new double[] {accY, accY, accZ};
		}
		//return null;
	}
}
