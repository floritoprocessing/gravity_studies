package graf.gravity;

import graf.gravity.GravityTool;

public class Mass {

	protected double x=0, y=0, z=0;
	protected double movX=0, movY=0, movZ=0;
	protected double mass;
	
	protected int dimension = GravityTool.DIMENSIONS_2;
	
	/**
	 * Creates a new mass in a 2-dimensional environment at position 0/0 with the specified value.
	 * @param mass
	 */
	public Mass(double mass) {
		this.mass = mass;
	}
	
	/**
	 * Creates a new mass in a 2-dimensional environment at position x/y with the specified value.
	 * @param mass
	 * @param x
	 * @param y
	 */
	public Mass(double mass, double x, double y) {
		this(mass);
		this.x = x;
		this.y = y;
	}
	
	/**
	 * Creates a new mass in a 3-dimensional environment at position x/y/z with the specified value
	 * @param mass
	 * @param x
	 * @param y
	 * @param z
	 */
	public Mass(double mass, double x, double y, double z) {
		this(mass,x,y);
		this.z = z;
		dimension = GravityTool.DIMENSIONS_3;
	}
	
	
	
	
	
	
	/*public int getDimension() {
		return dimension;
	}*/
	
	public double getMass() {
		return mass;
	}
	
	public double getMovementX() {
		return movX;
	}
	
	public double getMovementY() {
		return movY;
	}
	
	public double getMovementZ() {
		if (in2D()) throw new RuntimeException("Cannot get z-movement in a 2D environment");
		return movZ;
	}
	
	public double getX() {
		return x;
	}
	
	public double getY() {
		return y;
	}
	
	public double getZ() {
		if (in2D()) throw new RuntimeException("Cannot get z-position in a 2D environment");
		return z;
	}
	
	public boolean in2D() {
		return (dimension==GravityTool.DIMENSIONS_2);
	}
	
	public boolean in3D() {
		return (dimension==GravityTool.DIMENSIONS_3);
	}
	
	public void setMass(double mass) {
		this.mass = mass;
	}
	
	public void setMovementX(double movX) {
		this.movX = movX;
	}
	
	public void setMovementY(double movY) {
		this.movY = movY;
	}
	
	public void setMovementZ(double movZ) {
		if (in2D()) throw new RuntimeException("Cannot set z-movement in a 2D environment");
		this.movZ = movZ;
	}
	
	public void setX(double x) {
		this.x = x;
	}
	
	public void setY(double y) {
		this.y = y;
	}
	
	public void setZ(double z) {
		if (in2D()) throw new RuntimeException("Cannot set z-position in a 2D environment");
		this.z = z;
	}
	
	/**
	 * Change the position vector based on the movement vector
	 * @param time
	 */
	public void move(double time) {
		x += time * movX;
		y += time * movY;
		if (in3D()) z += time * movZ;
	}
	
	/**
	 * Change the movement vector (2D)
	 * @param accX
	 * @param accY
	 * @param time
	 */
	public void accelerate(double accX, double accY, double time) {
		movX += time * accX;
		movY += time * accY;
	}
	
	/**
	 * Change the movement vector (3D)
	 * @param accX
	 * @param accY
	 * @param accZ
	 * @param time
	 */
	public void accelerate(double accX, double accY, double accZ, double time) {
		if (in2D()) throw new RuntimeException("Cannot accelerate with 3D vector in a 2D environment");
		movX += time * accX;
		movY += time * accY;
		movZ += time * accZ;
	}

	
	
	

}
