package graf.timing;

public class Timer {
	
	double nanoTime;
	
	public Timer() {
		start();
	}
	
	public void start() {
		nanoTime = System.nanoTime();
	}
	
	/**
	 * Returns time between start() and stop() in nanoSeconds (10^-6 milliseconds)
	 * @return
	 */
	public double stop() {
		return (System.nanoTime()-nanoTime);
	}
	
}
