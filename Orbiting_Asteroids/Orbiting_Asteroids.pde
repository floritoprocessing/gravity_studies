int maxAsteroids=120;
float GM=10000;
float GMPlan=0.04;
Asteroid[] ast = new Asteroid[maxAsteroids+1];
float genxRot=0.8*HALF_PI;
float tGenxRot=genxRot;
float transX=width/2;
float tTransX=width/2;


void setup() {
  size(600,300,P3D);
  background(255);
  colorMode(RGB,255);
  fill(0);stroke(0);
  ellipseMode(DIAMETER);
  lights();
  for (int i=1;i<=maxAsteroids;i++) {
    ast[i]=new Asteroid(i);
  }
}

void draw() {
  if (mouseY!=0) {
    tGenxRot=HALF_PI*(0.8+0.4*mouseY/(float)height);  //0.8..1.2
  } else {
    tGenxRot=0.8*HALF_PI;
  }
  genxRot=(7*genxRot+tGenxRot)/8;
  background(0);
  tTransX=width/2-(mouseX/2-width/4);
  transX=(7*transX+tTransX)/8;
  translate(transX,height/2,-150);
  rotateX(genxRot);
  noLights();
  noSmooth();
  createSun();
  lights();
  smooth();
  for (int i=1;i<=maxAsteroids;i++) {
    ast[i].update();
  }
  
  
}

class Asteroid {
  float x,y,z,px,py,pz;
  float xmov=0.0,ymov=0.0,zmov=0.0;
  float distance, a, deg;
  float[] lx=new float[11];
  float[] ly=new float[11];
  float[] lz=new float[11];
  int nr;
  Asteroid (int n) {
    nr=n;
    float r=20+pow(random(0,1),0.5)*220;//random(40,250);
    float deg=random(0,TWO_PI);
    x=r*cos(deg); y=r*sin(deg); z=r*random(-0.01,0.01);
    for (int i=1;i<=10;i++) {
      lx[i]=x; ly[i]=y; lz[i]=z;
    }
    float spd=sqrt(GM/r);
    spd*=(1+random(-0.05,0.05));
    xmov=spd*sin(deg);
    ymov=-spd*cos(deg);
    zmov=0;
  }
  
  void update() {
    // calculate vector to sun;
    distance = dist(0,0,0,x,y,z);
    a=GM/(distance*distance);
    deg=atan2(y,x);
    xmov-=a*cos(deg);
    ymov-=a*sin(deg);
    deg=atan2(z,x);
    zmov-=a*sin(deg);
    // calculate vector to other planets;
    for (int i=1;i<=maxAsteroids;i++) {
      if (i!=nr) {
        px=ast[i].x; py=ast[i].y; pz=ast[i].z;
        distance = dist(px,py,x,y);
        a=GMPlan/(distance*distance);
        deg=atan2(y-py,x-px);
        xmov-=a*cos(deg);
        ymov-=a*sin(deg);
        deg=atan2(z-pz,x-px);
        zmov-=a*sin(deg);
      }
    }
    x+=xmov; y+=ymov; z+=zmov;
    for (int i=10;i>=2;i--) {
      lx[i]=lx[i-1];
      ly[i]=ly[i-1];
      lz[i]=lz[i-1];
    }
    lx[1]=x; ly[1]=y; lz[1]=z;
    
    drawAsteroid(x,y,z,lx,ly,lz);
  }
}

void createSun() {
  pushMatrix();
  fill(255,200,0);
  sphere(5);
  popMatrix();
}

void drawAsteroid(float x,float y, float z, float[] lx, float[] ly, float[] lz) {
  color white=color(255,255,255,128);
  color black=color(0,0,0);
  color Red=color(255,0,0);
  color lightRed=color(255,64,64);
  
  stroke(white);
  for (int i=1;i<=9;i++) {
    line(lx[i],ly[i],lz[i],lx[i+1],ly[i+1],lz[i+1]);
  }
  
  noStroke();
  fill(Red);
  pushMatrix();
  translate(x,y,z);
  rotateX(-genxRot);
  ellipse(0,0,3,3);
  popMatrix();
}

void mousePressed() {
  for (int i=1;i<=maxAsteroids;i++) {
    ast[i]=new Asteroid(i);
  }
}
