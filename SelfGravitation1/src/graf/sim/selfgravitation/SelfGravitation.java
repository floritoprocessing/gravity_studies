package graf.sim.selfgravitation;

import graf.math.vector.Vec;
import graf.particle.Particle;
import processing.core.PApplet;

public class SelfGravitation extends PApplet {

	public static void main(String[] args) {
		PApplet.main(new String[] {"graf.sim.selfgravitation.SelfGravitation"});
	}

	private static final int METHOD_NORMAL = 0;
	private static final int METHOD_FASTER = 1;
	private static final String[] METHOD_NAME = { "NORMAL", "FASTER" }; 
	private int method = METHOD_NORMAL;

	private static final double G = 1;
	private static final double standardMass = 0.1;
	private static final double minumumDistance = 0.5;
	
	Particle[] particle = new Particle[3000];
	Vec[] acceleration = new Vec[particle.length];

	private boolean nextFrameSwitchMethod = false;
	
	public void settings() {
		size(600,600,P3D);
	}
	
	
	public void setup() {
		
		double[] radius = new double[particle.length];
		double[] radians = new double[particle.length];
		for (int i=0;i<particle.length;i++) {
			radians[i] = 2*Math.PI*Math.random();
			radius[i] = width*0.24*Math.sqrt(0.05+0.95*Math.random());
			Vec pos = new Vec(radius[i],0);
			pos.rotZ(radians[i]);
			pos.add(new Vec(width/2,height/2));
			particle[i] = new Particle(standardMass,pos);
			acceleration[i] = new Vec();
		}
		for (int i=0;i<particle.length;i++) {
			acceleration[i] = getAcceleration(5, i, particle);
			double orbitalVelocity = Math.sqrt(acceleration[i].length()*radius[i]);
			Vec orbitVector = new Vec(0,1);
			orbitVector.rotZ(radians[i]);
			orbitVector.setLength(orbitalVelocity);
			particle[i].setMovement(orbitVector);
		}
	}

	private double[] getAcceleration(double minDistance, int sourceIndex, int targetIndex, Particle[] p) {
		/*Vec sourceToTarget = Vec.sub(p[target].getPosition(),p[sourceIndex].getPosition());
		double r = sourceToTarget.length();
		double r2 = r*r;
		double F = G*p[sourceIndex].getMass()*p[target].getMass()/r2;
		double a = F/p[sourceIndex].getMass();
		Vec acc = new Vec(sourceToTarget);
		acc.setLength(a);*/
		double stx = p[targetIndex].getPosition().x - p[sourceIndex].getPosition().x;
		double sty = p[targetIndex].getPosition().y - p[sourceIndex].getPosition().y;
		//double stz = p[target].getPosition().z - p[sourceIndex].getPosition().z;
		double r2 = stx*stx+sty*sty;//+stz*stz;
		
		double r = Math.sqrt(r2);
		if (r>minDistance) {
		double a = G*p[targetIndex].getMass()/r2;
		double sfac = a/r;
		double accx = stx*sfac, accy = sty*sfac;//, accz = stz*sfac;	
		return new double[] {accx,accy};
		} else {
			return new double[] {0,0};
		}
	}

	private Vec getAcceleration(double minDistance, int sourceIndex, Particle[] p) {
		Vec totalAcc = new Vec();
		for (int targetIndex=0;targetIndex<p.length;targetIndex++) {
			if (targetIndex!=sourceIndex) {
				totalAcc.add(getAcceleration(minDistance, sourceIndex, targetIndex, p));
			}
		}
		return totalAcc;
	}

	//double[][] soTaX = new double[particle.length][particle.length];
	//double[][] soTaY = new double[particle.length][particle.length];
	//double[][] rInv = new double[particle.length][particle.length];
	//double[][] r2Inv = new double[particle.length][particle.length];
	//double[][] rxR2Inv = new double[particle.length][particle.length];
	float[][] nAccX = new float[particle.length][particle.length];
	float[][] nAccY = new float[particle.length][particle.length];


	public void keyPressed() {
		nextFrameSwitchMethod = true;
	}
	

	public void draw() {
		if (nextFrameSwitchMethod) {
			nextFrameSwitchMethod=false;
			if (method==METHOD_FASTER) method=METHOD_NORMAL;
			else method=METHOD_FASTER;
			System.out.println("Method: "+METHOD_NAME[method]);
		}

		//long ti = System.currentTimeMillis();
		
		if (method==METHOD_FASTER) {
			double stx, sty, r2, rxR2Inv, accx, accy;
			for (int sourceIndex=0;sourceIndex<particle.length-1;sourceIndex++) {
				for (int targetIndex=sourceIndex+1;targetIndex<particle.length;targetIndex++) {
					stx = particle[targetIndex].getPosition().x - particle[sourceIndex].getPosition().x;
					sty = particle[targetIndex].getPosition().y - particle[sourceIndex].getPosition().y;
					r2 = stx*stx+sty*sty;
					rxR2Inv = 1.0/(r2*Math.sqrt(r2));
					nAccX[sourceIndex][targetIndex] = (float)(stx * rxR2Inv);
					nAccY[sourceIndex][targetIndex] = (float)(sty * rxR2Inv);
					nAccX[targetIndex][sourceIndex] = -nAccX[sourceIndex][targetIndex];
					nAccY[targetIndex][sourceIndex] = -nAccY[sourceIndex][targetIndex];
				}
			}
			for (int sourceIndex=0;sourceIndex<particle.length;sourceIndex++) {
				//acceleration[sourceIndex].set(0,0);
				accx=0;
				accy=0;
				for (int targetIndex=0;targetIndex<particle.length;targetIndex++) {
					if (targetIndex!=sourceIndex) {
						accx += nAccX[sourceIndex][targetIndex]*particle[targetIndex].getMass();
						accy += nAccY[sourceIndex][targetIndex]*particle[targetIndex].getMass();
						//acceleration[sourceIndex].add(accx,accy);
					}
				}
				//acceleration[sourceIndex].mul(G);
				acceleration[sourceIndex].set(accx*G,accy*G);
			}
		}

		else if (method==METHOD_NORMAL) {
			for (int source=0;source<particle.length;source++) {
				acceleration[source] = getAcceleration(minumumDistance,source,particle);
			}
		}
		
		//long tit = System.currentTimeMillis() - ti;
		//System.out.println("Time to calc ("+METHOD_NAME[method]+"): "+tit);

		for (int i=0;i<particle.length;i++) {
			particle[i].integrate(1,acceleration[i]);
		}

		background(0);
		stroke(255,255,255);//,50);
		for (int i=0;i<particle.length;i++) {
			float x = (float)particle[i].getPosition().x;
			float y = (float)particle[i].getPosition().y;
			point(x,y);
			//set((int)x,(int)y,color(255,255,255,50));
		}
	}

}
