PGraphics canvas;
boolean showCanvas = true;

void initCanvas(int w, int h) {
  canvas = createGraphics(w,h,P3D);
  canvas.beginDraw();
  canvas.background(0);
  canvas.strokeWeight(2);
  canvas.endDraw();
}

void clearCanvas() {
  canvas.beginDraw();
  canvas.background(0);
  canvas.endDraw();
}

void drawPlanets(Planet[] planets) {
  canvas.beginDraw();
  canvas.blendMode(ADD);
  canvas.translate(width/2,height/2);
  if (rotating) {
    float rd = atan2(planet.pos.y,planet.pos.x);
    canvas.rotate(-rd);
  }
  canvas.scale(mToPixel);
  canvas.stroke(255,30,15,16);
  
  canvas.beginShape(POINTS);
  for (Planet p:planets) {
    canvas.vertex(p.pos.x,p.pos.y);
  }
  canvas.endShape();
  canvas.endDraw();
}