/** G in m^3 kg^-1 s^-2 */
final float G = 6.67408 * pow(10,-11);
// 149.6 million km in meters
final float AU = 149600000.*1000;

//1.989 × 10^30 kg
final float SUN_MASS = 1.989*pow(10,30);
final Planet SUN = new Planet(SUN_MASS, new PVector());




final float TIME_HOUR = 3600;
final float TIME_DAY = 24*TIME_HOUR;


//float orbitalVelocityAroundSun(PVector posInM) {
//    float rInM = posInM.mag();
//    float v = sqrt(G*SUN_MASS/rInM); // metersPerSecond
//    return v;
//}

PVector orbitalVelocityAroundSun(PVector p) {
 // PVector p = new PVector(posInM.x, posInM.y, posInM.z);
  float rInM = p.mag();
  float v = sqrt(G*SUN_MASS/rInM); // metersPerSecond
  PVector vel = new PVector(0,-v,0);
  float rot = atan2(p.y,p.x);
  vel.rotate(rot);
  return vel;
}

PVector accelerationTo(PVector bodyPos, Planet... planets ) {
  
  PVector totalAcceleration = new PVector();
  
  for (Planet p:planets) {
    
    PVector toPlanet = PVector.sub(p.pos,bodyPos);
    float radius = toPlanet.mag();
    float radiusSq = radius*radius;
    PVector toPlanetNorm = toPlanet.normalize();
    
    //float F = G * p.mass * asteroid.mass / radiusSq; --> because F = M*A
    // m s^-2
    float accelerationMagnitude = G * p.mass / radiusSq;
    //println("acceleration "+accelerationMagnitude);
    PVector toPlanetAcceleration = PVector.mult(toPlanetNorm,accelerationMagnitude);
    totalAcceleration = totalAcceleration.add(toPlanetAcceleration);
  }
  
  return totalAcceleration;
  
}