//5.972 × 10^24 kg
final float EARTH_MASS = 5.972*pow(10,24);
final PVector EARTH_POS = new PVector(AU,0);
final Planet EARTH = new Planet(EARTH_MASS, EARTH_POS, orbitalVelocityAroundSun(EARTH_POS));//new PVector(0,-orbitalVelocityAroundSun(EARTH_POS)));

final float JUPITER_MASS = 317.83*EARTH_MASS;
final PVector JUPITER_POS = new PVector(5.203*AU,0);
final Planet JUPITER = new Planet(JUPITER_MASS, JUPITER_POS, orbitalVelocityAroundSun(JUPITER_POS));//new PVector(0,-orbitalVelocityAroundSun(JUPITER_POS)));

class Planet {

  float mass = 1;
  PVector pos;
  PVector vel;

  Planet(float mass, PVector pos) {
    this(mass, pos, new PVector(0, 0, 0));
  }

  Planet(float mass, PVector pos, PVector vel) {
    this.mass = mass;
    this.pos = new PVector(pos.x, pos.y, pos.z);
    this.vel = new PVector(vel.x, vel.y, vel.z);
  }

  void move(float tick) {
    move(tick, null);
  }

  void move(float tick, PVector acc) {

    if (acc==null) {
      acc = new PVector();
    }

    vel = PVector.add(vel, PVector.mult(acc, tick));
    pos = PVector.add(pos, PVector.mult(vel, tick));

    //PVector newPos = PVector.add( pos, PVector.mult(vel,tick), PVector.mult(acc,0.5*tick*tick));
    //PVector newVel = PVector.mult( PVector.sub(newPos, pos), 1/tick);
    //println(frameCount+" vel "+vel+" -> "+newVel+"\t"+vel.mag()+"/"+newVel.mag());
    //vel.set(newVel);
    //pos.set(newPos);
    
    //println(pos);
  }
  
  Planet createClone() {
    return new Planet(mass, pos, vel);
  }
}