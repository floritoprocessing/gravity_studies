Planet sun;
Planet planet;
//Planet planet2;
Planet[] asteroids;

float mToPixel;

float TICK;
int PLANET_AMOUNT;

boolean rotating = true;
boolean showAsteroids = true;


  
void setup() {
  
  size(800,800,P3D);
  frameRate(120);
  
  initCanvas(width,height);
  
  planet = JUPITER.createClone();
  
  //planet2 = JUPITER.createClone();
  //planet2.pos.rotate(radians(180));
  //planet2.vel.rotate(radians(180));
  
  TICK = TIME_HOUR*5;
  PLANET_AMOUNT = 500;
  mToPixel = width/(planet.pos.mag())/3;
  
  
  asteroids = new Planet[PLANET_AMOUNT];
  
  float rBase = planet.pos.mag();
  float r0 = rBase*0.95;
  float r1 = rBase*1.05;
  
  for (int i=0;i<asteroids.length;i++) {
    
    // equal area distribution on circle
    PVector pos;
    do {
      pos = new PVector(random(-r1,r1),random(-r1,r1));
    } while (pos.mag()<r0 || pos.mag()>r1);
    PVector mov = orbitalVelocityAroundSun(pos);//new PVector(0,-orbitalVelocityAroundSun(pos),0);
    
    float rot = atan2(pos.y,pos.x);
    pos.rotate(-rot);
    mov.rotate(-rot);
    rot = random(1)<0.5 ? -1 : 1;
    rot *= random(radians(55),radians(65));
    pos.rotate(rot);
    mov.rotate(rot);
    
    asteroids[i] = new Planet(1, pos, mov);
  }
  
}



void draw() {
 
  float targetFrameTime = 1000.0/60.0;
  float tiEnd = millis()+targetFrameTime;
  info_loops=0;
  
  do {
    planet.move(TICK, accelerationTo(planet.pos, SUN));
   // planet2.move(TICK, accelerationTo(planet2.pos, SUN, planet));
    for (Planet p:asteroids) {
      p.move(TICK, accelerationTo(p.pos, SUN, planet));
    }
    info_loops++;
  } while (millis()<tiEnd);
  
  onScreen();
}




void onScreen() {
  
  if (!showCanvas) {
    background(0);
  } else {
    canvas.loadPixels();
    background(canvas);
  }
  fill(255);
  text(nf(frameRate,1,1)+" fps",10,20);
  text("loops="+info_loops,10,35);
  text("[r]otating="+rotating,10,50);
  text("show [a]steroids="+showAsteroids,10,65);
  text("show [c]anvas="+showCanvas,10,80);
  
  
  translate(width/2,height/2);
  noStroke();
  
  
  // draw sun
  fill(255,255,0);
  ellipse(SUN.pos.x*mToPixel, SUN.pos.y*mToPixel, 30, 30);
  
  if (rotating) {
    float rd = atan2(planet.pos.y,planet.pos.x);
    rotate(-rd);
  }
  
  // draw earth
  fill(50,50,255);
  ellipse(planet.pos.x*mToPixel, planet.pos.y*mToPixel, 10, 10);
  //ellipse(planet2.pos.x*mToPixel, planet2.pos.y*mToPixel, 10, 10);
  
  //noFill();
  //strokeWeight(1);
  //stroke(50,50,255,128);
  //float r = 2*planet.pos.mag()*mToPixel;
  //ellipse(0,0, r,r);
  //r = 2*planet2.pos.mag()*mToPixel;
  //ellipse(0,0, r,r);
  
  // draw planets on screen
  if (showAsteroids) {
    stroke(0,128,0);
    strokeWeight(2);
    pushMatrix();
    scale(mToPixel);
    beginShape(POINTS);
    for (Planet p:asteroids) {
      vertex(p.pos.x, p.pos.y);
    }
    endShape();
    popMatrix();
  }
  
  if (showCanvas) {
    // draw planets on canvas
    drawPlanets(asteroids);
  }
  
  
}





void keyPressed() {
  if (key=='r') {
    rotating = !rotating;
    clearCanvas();
  }
  if (key=='c') {
    showCanvas = !showCanvas;
    if (showCanvas) {
      clearCanvas();
    }
  }
  if (key=='C') clearCanvas();
  if (key=='a') showAsteroids =! showAsteroids;
}

void mouseMoved() {
  
  float distToCenterInPix = dist(width/2,height/2,mouseX,mouseY);
  float distToCenterInMeters = distToCenterInPix/mToPixel;
  float distToCenterInPlanetDistance = distToCenterInMeters / planet.pos.mag();
  println(distToCenterInPlanetDistance);
  
  
}