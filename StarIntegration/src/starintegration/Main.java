package starintegration;

import processing.core.*;

public class Main extends PApplet {

	public static void main(String[] args) {
		PApplet.main(new String[] {"starintegration.Main"});
	}
	
	private static final String outputPath = "output/StarIntegration_";
	private static final boolean saveImage = true;
	

	Stars stars;
	int frame=0;

	public void settings() {
		size(800,600,P3D);
	}
	public void setup() {
		
		background(0);
		noStroke();
		fill(255);

		stars = new Stars(this);
	}

	public void draw() {
		background(0);
		translate(width/2,height/2,-200);

		stars.drawAll();
		if (saveImage) saveFrame(outputPath+nf(frame,4)+".jpg");
		frame++;

		/*int ti=millis();
		stars.calculateDistances();
		System.out.println(millis()-ti);*/
		
		int ti=millis();
		stars.moveAndCollideAll();
		//System.out.println(millis()-ti);
		
		
		stars.updateAllToNewPosition();
		stars.removeStarsOutOfBounds();

	}
}