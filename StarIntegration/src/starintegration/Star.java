package starintegration;

import processing.core.PApplet;

public class Star implements StarConstants {
	
	int id;
	Vec acc = new Vec();
	Vec pos;
	Vec posNew;
	Vec mov;
	double M = 0.5; // is volume
	double side;
	
	public Star(int id, Vec pos, Vec mov) {
		this.id = id;
		this.pos = new Vec(pos);
		posNew = new Vec(pos);
		this.mov = new Vec(mov);
		updateSideFromMass();
	}
	
	private void updateSideFromMass() {
		 side = MIN_DISTANCE_BETWEEN_STARS * Math.pow(M,1/3.0);
	}
	
	public void accelerateAndCollide(Stars stars) {
		acc.zero();
		Vec dVec = new Vec();
		Vec tVec = new Vec();
		
		for (int i=0;i<stars.size();i++) {
			Star s = stars.starAt(i);
			if (s!=this) {
				//dVec.set(s.pos.getX()-pos.getX(),s.pos.getY()-pos.getY(),s.pos.getZ()-pos.getZ());//
				dVec = Vec.sub(s.pos,pos);
				double d = dVec.len();
				//double d = stars.getDistance(s.id,id);
				if (d<MIN_DISTANCE_BETWEEN_STARS) {
					// collide!
					//d=MIN_DISTANCE_BETWEEN_STARS;
					
					double totalM = M+s.M;
					double thisFac = M/totalM;
					double otherFac = s.M/totalM;
					
					Vec scaledMovThis = Vec.mul(mov,thisFac);
					Vec scaledMovOther = Vec.mul(s.mov,otherFac);
					mov.set(Vec.add(scaledMovThis,scaledMovOther));
					M = totalM;
					updateSideFromMass();
					
					stars.removeStarAt(i);
					i--;
				} else {
					double a = G*s.M/(d*d);
					tVec = new Vec(dVec);
					tVec.mul(a/d);
					acc.add(tVec);
				}
			}
		}
		
		mov.add(acc);
		
	}

	public void move() {
		posNew = Vec.add(mov,pos);
	}
	
	public void updateToNewPosition() {
		pos.set(posNew);
	}

	public void draw(PApplet pa) {
		//if (side>1) System.out.println(side);
		float s = (float)side;
		if (s<1) {
			pa.stroke(255);
			pa.point(pos.getXf(), pos.getYf(), pos.getZf());
		}
		else {
			pa.noStroke();
			pa.pushMatrix();
			pa.translate(pos.getXf(),pos.getYf(),pos.getZf());
			pa.box(s,s,s);
			pa.popMatrix();
		}
		//
	}

}
