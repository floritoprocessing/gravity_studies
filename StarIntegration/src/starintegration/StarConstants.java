package starintegration;

public interface StarConstants {
	public static final double MAX_DISTANCE_FROM_0 = 800;
	public static final double MIN_DISTANCE_BETWEEN_STARS = 0.5;
	
	public static final double G = 0.25;
}
