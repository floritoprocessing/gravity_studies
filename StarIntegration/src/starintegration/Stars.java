package starintegration;

import java.util.Vector;

import processing.core.PApplet;

public class Stars {

	private static final int AMOUNT = 5000;
	
	private PApplet pa;
	private Vector stars;
	
	//private boolean allMoved = true;
	//private int moveIndex = 0;
	//private double[][] distance;
	
	public Stars(PApplet pa) {
		this.pa = pa;
		stars = new Vector();
		for (int i=0;i<AMOUNT;i++)
			stars.add(new Star(i,Vec.createRandomEvenDistribution(50),Vec.createRandom(0)));
		//distance = new double[AMOUNT][AMOUNT];
	}
	
	protected int size() {
		return stars.size();
	}
	
	protected Star starAt(int i) {
		return (Star)stars.elementAt(i);
	}
	
	protected void removeStarAt(int i) {
		stars.removeElementAt(i);
	}
	
	/*public void calculateDistances() {
		double d, dx, dy, dz;
		for (int i=0;i<stars.size()-1;i++) {
			int iId = starAt(i).id;
			Star si = starAt(i);
			for (int j=i+1;j<stars.size();j++) {
				int jId = starAt(j).id;
				Star sj = starAt(j);
				dx = si.pos.getX()-sj.pos.getX();
				dy = si.pos.getY()-sj.pos.getY();
				dz = si.pos.getZ()-sj.pos.getZ();
				d = Math.sqrt(dx*dx+dy*dy+dz*dz);
				distance[iId][jId] = d;//Vec.sub(starAt(i).pos,starAt(j).pos).len();
				distance[jId][iId] = distance[iId][jId];
			}
		}
	}
	
	public double getDistance(int a, int b) {
		return distance[a][b];
	}*/

	/*public void startMoving() {
		moveIndex=0;
	}
	
	public boolean allMoved() {
		return allMoved;
	}
	
	public void moveAsMuchAsPossible(int timeInMillis) {
		long endTime = System.currentTimeMillis()+timeInMillis;
		allMoved=true;
		while (System.currentTimeMillis()<endTime&&moveIndex<stars.size()) {
			allMoved=false;
			starAt(moveIndex).accelerateTo(this);
			starAt(moveIndex).move();
			moveIndex++;
		}
	}*/
	
	public void moveAndCollideAll() {
		for (int i=0;i<stars.size();i++) starAt(i).accelerateAndCollide(this);
		//System.out.println("acceleration amount of stars: "+stars.size());
		System.out.println(stars.size());
		for (int i=0;i<stars.size();i++) starAt(i).move();
	}
	
	public void drawAll() {
		for (int i=0;i<stars.size();i++) starAt(i).draw(pa);
	}

	public void updateAllToNewPosition() {
		for (int i=0;i<stars.size();i++) starAt(i).updateToNewPosition();
	}
	
	public void removeStarsOutOfBounds() {
		for (int i=0;i<stars.size();i++) {
			if (starAt(i).pos.len()>Star.MAX_DISTANCE_FROM_0) {
				stars.removeElementAt(i);
				i--;
			}
		}
		//System.out.println(stars.size());
	}

	

}
