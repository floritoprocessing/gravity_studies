package starintegration;

public class Vec {

	private double x,y,z;

	public Vec() {
		x=0;
		y=0;
		z=0;
	}

	public Vec(double x) {
		this(x,0,0);
	}
	
	public Vec(double x, double y) {
		this(x,y,0);
	}

	public Vec(double x, double y, double z) {
		this.x=x;
		this.y=y;
		this.z=z;
	}

	public Vec(Vec v) {
		x=v.x;
		y=v.y;
		z=v.z;
	}

	
	public void zero() {
		x=0;
		y=0;
		z=0;
	}

	public void set(double x, double y, double z) {
		this.x=x;
		this.y=y;
		this.z=z;
	}
	
	public void set(Vec v) {
		x=v.x;
		y=v.y;
		z=v.z;
	}
	
	
	
	public double getX() {
		return x;
	}
	
	public double getY() {
		return y;
	}
	
	public double getZ() {
		return z;
	}
	
	public float getXf() {
		return (float)x;
	}
	
	public float getYf() {
		return (float)y;
	}
	
	public float getZf() {
		return (float)z;
	}

	public String toString() {
		return x+"\t"+y+"\t"+z;
	}
	
	
	
	public void add(Vec b) {
		x+=b.x;
		y+=b.y;
		z+=b.z;
	}

	public void mul(double n) {
		x*=n;
		y*=n;
		z*=n;
	}
	
	public void div(double d) {
		if (isNullVec())
			throw new RuntimeException("Cannot divide Vec by zero");
		else {
			x/=d;
			y/=d;
			z/=d;
		}
	}
	
	
	
	public void rotX(double rd) {
		double SIN=Math.sin(rd); 
		double COS=Math.cos(rd);
		double yn=y*COS-z*SIN;
		double zn=z*COS+y*SIN;
		y=yn;
		z=zn;
	}

	public void rotY(double rd) {
		double SIN=Math.sin(rd);
		double COS=Math.cos(rd); 
		double xn=x*COS-z*SIN; 
		double zn=z*COS+x*SIN;
		x=xn;
		z=zn;
	}

	public void rotZ(double rd) {
		double SIN=Math.sin(rd);
		double COS=Math.cos(rd); 
		double xn=x*COS-y*SIN; 
		double yn=y*COS+x*SIN;
		x=xn;
		y=yn;
	}
	
	
	public double lenSq() {
		return x*x+y*y+z*z;
	}
	
	public double len() {
		return Math.sqrt(lenSq());
	}
	
	public boolean isNullVec() {
		return (x==0&&y==0&&z==0);
	}
	
	
	public static final Vec createRandom(double lengthMin, double lengthMax) {
		double l = lengthMin + (lengthMax-lengthMin)*Math.random();
		Vec out = new Vec(l);
		out.rotZ(Math.random()*Math.PI);
		out.rotX(Math.random()*2*Math.PI);
		return out;
	}
	
	public static Vec createRandom(double length) {
		return createRandom(0,length);
	}

	
	public static Vec createRandomEvenDistribution(double length) {
		double p = Math.pow(Math.random(),1/2.0);
		Vec out = new Vec(p*length);
		
		double v = Math.random()*2*Math.PI;
		double w = Math.random();
		double u=Math.asin(2*w-1);
		out.rotZ(v);
		out.rotX(u);
		//double z = (Math.random()<0.5?-1:1)*Math.pow(Math.random(),3/2.0);
		//out.rotZ(Math.PI*0.5 + Math.PI*0.5*z);
		//out.rotX(Math.random()*2*Math.PI);
		return out;
	}

	public static Vec add(Vec a, Vec b) {
		return new Vec(a.x+b.x,a.y+b.y,a.z+b.z);
	}

	public static Vec sub(Vec a, Vec b) {
		return new Vec(a.x-b.x,a.y-b.y,a.z-b.z);
	}

	public static Vec mul(Vec a, double n) {
		return new Vec(a.x*n,a.y*n,a.z*n);
	}
}
