import java.util.Vector;

int PLANETS_AMOUNT_MIN = 50;
int PLANETS_AMOUNT_MAX = 400;
float PLANET_INITAL_SPEED_MAX = 1.05;
float PLANET_INITAL_POS_RANGE = 150;

boolean SAVE_FRAME = false;
int SAVE_FRAME_EVERY_NTH=1;

boolean SHOW_PLANET_AMOUNT = true;

boolean fillPlanets=true;
Vec lastAvCen = new Vec();
int planNr=0;
Vector planets;
Vector explosions;
PFont font;
int fCount=0;
int tickTime=millis();

void setup() {
  size(720,576,P3D);
  noStroke();
  planets = new Vector();
  explosions = new Vector();
  font = loadFont("SansSerif.bold-14.vlw");
  textFont(font,14);
  ellipseMode(CENTER);
}

void draw() {
  // add planets
  if (fillPlanets) {
    if (random(1.0)<0.5) {
      Vec pos = new Vec(random(-PLANET_INITAL_POS_RANGE/2.0,PLANET_INITAL_POS_RANGE/2.0),random(-PLANET_INITAL_POS_RANGE/2.0,PLANET_INITAL_POS_RANGE/2.0),random(-PLANET_INITAL_POS_RANGE/2.0,PLANET_INITAL_POS_RANGE/2.0));
      Vec mov = new Vec(pos);
      mov.setLen(PLANET_INITAL_SPEED_MAX);
      Planet p = new Planet(random(1,5),pos,mov);
      p.setG(0.005 * 100);
      planets.add(p);
      if (planets.size()>=PLANETS_AMOUNT_MAX) fillPlanets=false;
    }
  }

  // TICK PLANETS
  for (int i=0;i<planets.size();i++) ((Planet)(planets.elementAt(i))).tickAcc(planets,explosions);
  for (int i=0;i<planets.size();i++) ((Planet)(planets.elementAt(i))).tickMov(planets);
  for (int i=0;i<explosions.size();i++) ((Explosion)(explosions.elementAt(i))).tick(explosions);

  // MOVE ALL PLANETS AND EXPLOSIONS TOWARDS AVERAGE CENTER
  Vec avCen=new Vec();
  float totMass=0;
  float maxMass=0;
  for (int i=0;i<planets.size();i++) {
    float mass=((Planet)(planets.elementAt(i))).mass;
    maxMass=max(maxMass,mass);
    totMass+=sq(mass);
    avCen.add(vecMul(((Planet)(planets.elementAt(i))).pos,mass));
  }
  avCen.div(totMass);
  lastAvCen = vecAdd(vecMul(new Vec(),0.999),vecMul(avCen,0.001));
  for (int i=0;i<planets.size();i++) ((Planet)(planets.elementAt(i))).pos.sub(lastAvCen);
  for (int i=0;i<explosions.size();i++) ((Explosion)(explosions.elementAt(i))).pos.sub(lastAvCen);


  // DRAW EVERYTHING
  background(0);
  //lights();
  translate(width/2.0,height/2.0,0);
  for (int i=0;i<planets.size();i++) ((Planet)(planets.elementAt(i))).show();
  for (int i=0;i<explosions.size();i++) ((Explosion)(explosions.elementAt(i))).draw();


  // how many plantes:
  if (SHOW_PLANET_AMOUNT&&!SAVE_FRAME) {
    //println("nr of planets: "+planets.size());
    fill(255,255,255,128);
    text("Nr of planets: "+planets.size(),-width/2.0+20,-height/2.0+30);
    tickTime=millis()-tickTime;
    text("Frame time: "+tickTime+" milliseconds",-width/2.0+20,-height/2.0+50);
    text("Maximum mass: "+round(maxMass*100.0)/100.0,-width/2.0+20,-height/2.0+70);
    tickTime=millis();
  }


  // save frame is image file
  if (SAVE_FRAME) if (fCount%SAVE_FRAME_EVERY_NTH==0) saveFrame("C:/temp/Blobbing_#####.tga");
  fCount++;
}
