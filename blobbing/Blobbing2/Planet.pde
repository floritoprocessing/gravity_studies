class Planet {
  float mass, radius; // for simplicity.. mass = volume
  float G = 0.005 * 100;
  Vec pos, mov, acc=new Vec(0,0,0);
  //float rr=random(224,255),gg=random(162,200),bb=random(128,160);
  float rr=random(128,255),gg=random(128,255),bb=random(128,255);
  
  Planet(float _mass, Vec _pos, Vec _mov) {
    mass=_mass;
    radius = massToRadius(mass);
    pos=new Vec(_pos);
    mov=new Vec(_mov);
  }
  
  void setG(float _G) {
    G = _G;
  }
  
  float massToRadius(float m) {
    float r = pow((m*3.0/4.0)/PI,1/3.0);
    return r;
  }
  
  float radiusToMass(float r) {
    float m = (4.0/3.0) * PI * pow(r,3);
    return m;
  }
  
  void tickAcc(Vector ps, Vector expl) {
    //s = s + v*dt + 1/2*a*dt^2
    //pos.add(vecMul(mov,dt));
    acc.setVec(0,0,0);
    for (int i=0;i<ps.size();i++) {
      Planet p = (Planet)(ps.elementAt(i));
      if (p!=this) {
        float dx=p.pos.x-pos.x;
        float dy=p.pos.y-pos.y;
        float dz=p.pos.z-pos.z;
        float d2 = dx*dx + dy*dy + dz*dz;
        
        if (d2>radius*radius+p.radius*p.radius) {
          // GRAVITY:
          float F = G * mass * p.mass / d2;
          Vec dirVec = new Vec(dx,dy,dz);
          dirVec.setLen(F/mass);
          acc.add(dirVec);
        } else {
          // CLUTTER TOGETHER:
          float tMass=mass+p.mass;
          float mRat = mass/tMass;
          float pmRat = p.mass/tMass;
          pos = vecAdd( vecMul(pos,mRat) , vecMul(p.pos,pmRat) );
          mov = vecAdd( vecMul(mov,mRat) , vecMul(p.mov,pmRat) );
          rr = rr*mRat + p.rr*pmRat;
          gg = gg*mRat + p.gg*pmRat;
          bb = bb*mRat + p.bb*pmRat;
          float minMass = min(mass,p.mass);
          mass += p.mass;
          radius = massToRadius(mass);
          ps.remove(p);
          expl.add(new Explosion(pos,minMass*0.1,radius,rr,gg,bb));
        }
      }
    }
  }
  
  void tickMov(Vector ps) {
    mov.add(acc);
    pos.add(mov);
    if (pos.outOfBoundaries(2000,2000,2000)) ps.remove(this);
  }
  
  void show() {
    pushMatrix();
    fill(color(rr,gg,bb));
    vecTranslate(pos);
    //sphere(radius);
    ellipse(0,0,radius,radius);
    popMatrix();
  }
}
