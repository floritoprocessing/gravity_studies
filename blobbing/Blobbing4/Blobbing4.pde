import java.util.Vector;

//int PLANETS_AMOUNT_MIN = 50;
int PLANETS_AMOUNT_MAX = 1500;
//float PLANET_INITAL_SPEED_MAX = 0.08;
//float PLANET_INITAL_POS_RANGE = 70;

boolean SAVE_FRAME = false;
int SAVE_FRAME_EVERY_NTH=5;
String SAVE_FRAME_DIRECTORY = "C:\\Documents and Settings\\mgraf\\My Documents\\temp\\frames\\";
int saveFrameCount = 0;

boolean paused=false;

boolean SHOW_PLANET_AMOUNT = true;

boolean fillPlanets=true;
Vec lastAvCen = new Vec();
int planNr=0;
Vector planets;
Vector explosions;
PFont font;
int fCount=0;
int tickTime=millis();

void setup() {
  size(720,576,P3D);
  noStroke();
  planets = new Vector();
  explosions = new Vector();
  font = loadFont("SansSerif.bold-14.vlw");
  textFont(font,14);
  ellipseMode(CENTER);
  frameRate(1000);
  
  Planet p = new Planet(1000,new Vec(),new Vec());
  planets.add(p);
  
  for (int i=1;i<PLANETS_AMOUNT_MAX;i++) {
    Planet s  = (Planet)(planets.elementAt(0));
    float r = random(150,180);
    float mass = 0.1;
    if (i==1) mass = 50.0;
    Vec pos = new Vec(r,0,0);
    Vec mov = new Vec(0,sqrt(s.G*(s.mass+mass)/r),0);
    float rd0 = random(-0.01,0.01);
    pos.rotY(rd0);
    mov.rotY(rd0);
    
    float rd1 = random(TWO_PI);
    pos.rotZ(rd1);
    mov.rotZ(rd1);
    //float rd2 = random(TWO_PI);
    //pos.rotX(rd2);
    //mov.rotX(rd2);
    planets.add(new Planet(mass,pos,mov));
    // V^2 = GM / r
  }
  
}

void keyPressed() {
  if (key=='p'||key=='P') paused = !paused;
}

void draw() {
  if (!paused) {
    // TICK PLANETS
    for (int i=0;i<planets.size();i++) ((Planet)(planets.elementAt(i))).tickAcc(planets,explosions);
    for (int i=0;i<planets.size();i++) ((Planet)(planets.elementAt(i))).tickMov(planets);
    for (int i=0;i<explosions.size();i++) ((Explosion)(explosions.elementAt(i))).tick(explosions);

    // MOVE ALL PLANETS AND EXPLOSIONS TOWARDS AVERAGE CENTER
    Vec avCen=new Vec();
    float totMass=0;
    float maxMass=0;
    for (int i=0;i<planets.size();i++) {
      float mass=((Planet)(planets.elementAt(i))).mass;
      maxMass=max(maxMass,mass);
      totMass+=sq(mass);
      avCen.add(vecMul(((Planet)(planets.elementAt(i))).pos,sq(mass)));
    }
    avCen.div(totMass);
    lastAvCen = vecAdd(vecMul(new Vec(),0.999),vecMul(avCen,0.001));
    for (int i=0;i<planets.size();i++) ((Planet)(planets.elementAt(i))).pos.sub(lastAvCen);
    for (int i=0;i<explosions.size();i++) ((Explosion)(explosions.elementAt(i))).pos.sub(lastAvCen);


    // DRAW EVERYTHING
    background(0);
    //lights();
    translate(width/2.0,height/2.0,0);
    for (int i=0;i<planets.size();i++) ((Planet)(planets.elementAt(i))).show();
    for (int i=0;i<explosions.size();i++) ((Explosion)(explosions.elementAt(i))).draw();

    // save frame is image file
    if (SAVE_FRAME&&(fCount%SAVE_FRAME_EVERY_NTH==0)) {
      saveFrame(SAVE_FRAME_DIRECTORY+"Blobbing_"+nf(saveFrameCount,6)+".tga");
      saveFrameCount++;
    }
    fCount++;

    // how many plantes:
    if (SHOW_PLANET_AMOUNT) {
      //println("nr of planets: "+planets.size());
      fill(255,255,255,128);
      text("Nr of planets: "+planets.size(),-width/2.0+20,-height/2.0+30);
      tickTime=millis()-tickTime;
      text("Frame time: "+tickTime+" milliseconds",-width/2.0+20,-height/2.0+50);
      text("Maximum mass: "+round(maxMass*100.0)/100.0,-width/2.0+20,-height/2.0+70);
      tickTime=millis();
    }
  }
}
