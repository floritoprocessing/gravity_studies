class Explosion {
  Vec pos;
  float strength;
  float radius;
  float rr,gg,bb;
  float trans = 64;
  
  Explosion(Vec _pos, float _stren, float _radius, float _rr, float _gg, float _bb) {
    pos=new Vec(_pos);
    strength = _stren;
    radius = _radius;
    rr=_rr;
    gg=_gg;
    bb=_bb;
  }
  
  void tick(Vector all) {
    radius += strength*0.2;
    trans -= 1.0*0.2;
    if (trans<=0) all.remove(this);
  }
  
  void draw() {
    pushMatrix();
    fill(color(rr,gg,bb,trans));
    vecTranslate(pos);
    //sphere(radius);
    ellipse(0,0,radius,radius);
    popMatrix();
  }
  
}
