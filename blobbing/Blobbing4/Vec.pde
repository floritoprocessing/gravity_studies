/*****************************
 *  Vec class and functions  *
 *****************************/

class Vec {
  float x=0,y=0,z=0;
  
  Vec() {
  }

  Vec(float _x, float _y, float _z) {
    x=_x;
    y=_y;
    z=_z;
  }
  
  Vec(Vec _v) {
    x=_v.x; 
    y=_v.y; 
    z=_v.z;
  }
  
  void setVec(Vec _v) {
    x=_v.x; 
    y=_v.y; 
    z=_v.z;
  }
  
  void setVec(float _x, float _y, float _z) {
    x=_x;
    y=_y;
    z=_z;
  }
  
  float len() {
    return sqrt(x*x+y*y+z*z);
  }
  
  float lenSQ() {
    return (x*x+y*y+z*z);
  }
  
  void gravitateTo(Vec _to, float _mgd, float _fFac) {
    float minGravDist2=_mgd*_mgd;
    float dx=_to.x-x;
    float dy=_to.y-y;
    float dz=_to.z-z;
    float d2=dx*dx+dy*dy+dz*dz;
    if (d2<minGravDist2) {d2=minGravDist2;}
    //float d=sqrt(d2);
    float F=_fFac/d2;
    add(new Vec(dx*F,dy*F,dz*F));
  }

  void add(Vec _v) {
    x+=_v.x;
    y+=_v.y;
    z+=_v.z;
  }
  
  void add(float _x, float _y, float _z) {
    x+=_x;
    y+=_y;
    z+=_z;
  }

  void sub(Vec _v) {
    x-=_v.x;
    y-=_v.y;
    z-=_v.z;
  }

  void mul(float p) {
    x*=p;
    y*=p;
    z*=p;
  }
  
  void div(float p) {
    x/=p;
    y/=p;
    z/=p;
  }

  void negX() {
    x=-x;
  }

  void negY() {
    y=-y;
  }

  void negZ() {
    z=-z;
  }

  void neg() {
    negX();
    negY();
    negZ();
  }
  
  void normalize() {
    float l=len();
    if (l!=0) { // if not nullvector
      div(l);
    }
  }
  
  Vec getNormalized() {
    float l=len();
    if (l!=0) {
      Vec out=new Vec(this);
      out.div(l);
      return out;
    } else {
      return new Vec();
    }
  }
  
  void setLen(float ml) {
    Vec out=new Vec(this);
    float fac=ml/len();
    x*=fac;
    y*=fac;
    z*=fac;
  }
  
  void toAvLen(float ml) {
    Vec out=new Vec(this);
    float fac=ml/((ml+len())/2.0);
    x*=fac;
    y*=fac;
    z*=fac;
  }

  void rotX(float rd) {
    float SIN=sin(rd); 
    float COS=cos(rd);
    float yn=y*COS-z*SIN;
    float zn=z*COS+y*SIN;
    y=yn;
    z=zn;
  }

  void rotY(float rd) {
    float SIN=sin(rd);
    float COS=cos(rd); 
    float xn=x*COS-z*SIN; 
    float zn=z*COS+x*SIN;
    x=xn;
    z=zn;
  }

  void rotZ(float rd) {
    float SIN=sin(rd);
    float COS=cos(rd); 
    float xn=x*COS-y*SIN; 
    float yn=y*COS+x*SIN;
    x=xn;
    y=yn;
  }

  void rot(float xrd, float yrd, float zrd) {
    rotX(xrd);
    rotY(yrd);
    rotZ(zrd);
  }
  
  boolean isNullVec() {
    if (x==0&&y==0&&z==0) {return true;} else {return false;}
  }
  
  boolean outOfBoundaries(double bx, double by, double bz) {
    if (x<-bx||x>bx) return true;
    if (y<-by||y>by) return true;
    if (z<-bz||z>bz) return true;
    return false;
  }
}




Vec vecAdd(Vec a, Vec b) {
  Vec out=new Vec(a);
  out.add(b);
  return out;
}

Vec vecSub(Vec a, Vec b) {
  Vec out=new Vec(a);
  out.sub(b);
  return out;
}

Vec vecMul(Vec a, float b) {
  Vec out=new Vec(a);
  out.mul(b);
  return out;
}

Vec vecDiv(Vec a, float b) {
  Vec out=new Vec(a);
  out.div(b);
  return out;
}

float vecLen(Vec a) {
  return a.len();
}

Vec vecRotY(Vec a, float rd) {
  Vec out=new Vec(a);
  out.rotY(rd);
  return out;
}

Vec rndDirVec(float r) {
  Vec out=new Vec(0,0,r);
  out.rotX(random(-PI/2.0,PI));
  out.rotZ(random(0,2*PI));
  return out;
}

Vec rndPlusMinVec(float s) {
  Vec out=new Vec(-s+2*s*random(1.0),-s+2*s*random(1.0),-s+2*s*random(1.0));
  return out;
}

void vecSwap(Vec v1, Vec v2) {
  Vec t=new Vec(v1);
  v1.setVec(v2);
  v2.setVec(t);
}

/*****************************
 * adapted 3d functions to Vec
 *****************************/

void vecTranslate(Vec v) {
  translate((float)v.x,(float)v.y,(float)v.z);
}

void vecVertex(Vec v) {
  vertex((float)v.x,(float)v.y,(float)v.z);
}

void vecLine(Vec a, Vec b) {
  line((float)a.x,(float)a.y,(float)a.z,(float)b.x,(float)b.y,(float)b.z);
}

void vecRect(Vec a, Vec b) {
  rect((float)a.x,(float)a.y,(float)b.x,(float)b.y);
}

void vecCamera(Vec eye, Vec cen, Vec upaxis) {
  camera((float)eye.x,(float)eye.y,(float)eye.z,(float)cen.x,(float)cen.y,(float)cen.z,(float)upaxis.x,(float)upaxis.y,(float)upaxis.z);
}
