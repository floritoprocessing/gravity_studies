package graf.gravitationalLensing;

import java.util.Vector;

import processing.core.PApplet;
import processing.core.PGraphics;
import processing.core.PImage;

public class GravitationalLensing extends PApplet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -273997643840753965L;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//graf.gravitationalLensing.GravitationalLensing
		PApplet.main("graf.gravitationalLensing.GravitationalLensing");
	}

	private final String path = "";
	private final String sourceName = "Perla.jpg";

	PImage source = new PImage(1500,1200);
	PImage target = new PImage(1400,1400);

	final float sourcePos = -4000;
	final float targetPos = 0;
	final Vec sunPos = new Vec(-200,100,-2000);

	private static final float sunWeightStart = 500;
	private static final float sunWeightEnd = 500;
	private static final float sunWeightStep = 1;
	
	float sunWeight = sunWeightStart;
	int targetX = 0, targetY = 0;
	float lightSpeed = -10;
	boolean raytrace = true;
	int maxIterations = (int)(2*(sourcePos-targetPos)/lightSpeed); // more than lightSpeed / (sourcePos-targetPos)

	private boolean display3d = true;

	private long imageTime;
	
	public void settings() {
		size(600,600,P3D);
	}
	public void setup() {

		
		background(0);

//		PGraphics tmpg = createGraphics(source.width, source.width, P3D);
//		tmpg.image(loadImage(path+sourceName),0,0,tmpg.width,tmpg.height);
//		tmpg.updatePixels();
		PImage temp = loadImage(path+sourceName);
		temp.loadPixels();
		source.copy(temp, 0, 0, temp.width,temp.height, 0, 0, source.width,source.height);
		source.updatePixels();
		imageTime = System.currentTimeMillis();
	}

	public void keyPressed() {
		if (key=='d') display3d =!display3d;
	}

	public void draw() {

		/*
		 * Raytracing!
		 * Now shoot Particles from target straight towards source and let them be influenced by a 'sun' in the middle
		 * When the Particle crosses the source plane, read the color and put in on the target
		 */
		
		Vector<Vec> ray = new Vector<Vec>();

		long ti = System.currentTimeMillis();
		int traceColor = color(255,255,255);

		if (raytrace)
			while (System.currentTimeMillis()<ti+40) {

				Raytrace trace = new Raytrace(
						targetX-target.width/2,targetY-target.height/2,targetPos,
						0,0,lightSpeed,
						sunPos,
						sunWeight,
						sourcePos,
						1000);
				ray = trace.getTrace();
				if (ray!=null) {

				}

				if (raytrace) {
					if (ray==null)
						target.set(targetX, targetY, color(0,0,0,0));
					else {
						double sourceX = (ray.elementAt(ray.size()-1).getX() + source.width/2.0);
						double sourceY = (ray.elementAt(ray.size()-1).getY() + source.height/2.0);
						traceColor = AntiAlias.get(source,sourceX,sourceY);
						target.set(targetX, targetY, traceColor);
					}
					target.updatePixels();
				}

				targetX++;
				if (targetX==target.width) {
					targetX=0;
					targetY++;
					if (targetY==target.height) {
						
						target.save(path+"result_sunWeight_"+nf((int)sunWeight,4)+".tga");
						System.out.println("Rendering time: "+(int)((System.currentTimeMillis()-imageTime)/1000)+" seconds");
						System.out.println("Saving result for sunWeight "+sunWeight);
						raytrace=false;
					}
				}

			}


		if (!raytrace) {
			sunWeight += sunWeightStep;
			if (sunWeight<=sunWeightEnd) {
				targetX=0;
				targetY=0;
				target.copy(new PImage(1,1),0,0,1,1,0,0,target.width,target.height);
				target.updatePixels();
				raytrace=true;
				imageTime = System.currentTimeMillis();
			} else {
				System.out.println("END");
				noLoop();
			}
		}
		
		/*
		 * DISPLAY images, 3d model, raytrace
		 */

		background(128);
		float sScale = ((float)width/2.8f)/source.width;
		float sW = source.width*sScale, sH = source.height*sScale;
		float tScale = ((float)width/2.8f)/target.width;
		float tW = target.width*tScale, tH = target.height*tScale;

		pushMatrix();
		image(source, width/4 - sW/2, height/4 - sH/2, sW, sH);
		image(target, width/4*3 - tW/2, height/4 - tH/2, tW, tH);
		popMatrix();

		if (display3d) {
			pushMatrix();

			translate(width/2,height/2+1000);
			translate(0,0,-2000);
			translate((float)sunPos.getX(),(float)sunPos.getY(),(float)sunPos.getZ());
			rotateY(PI/6+frameCount/1000.0f);
			translate(-(float)sunPos.getX(),-(float)sunPos.getY(),-(float)sunPos.getZ());

			pushMatrix();
			if (ray!=null) {
				stroke(traceColor);
				for (int i=0;i<ray.size()-1;i++) {
					Vec p1 = ray.elementAt(i);
					Vec p2 = ray.elementAt(i+1);
					line((float)p1.getX(),(float)p1.getY(),(float)p1.getZ(),
							(float)p2.getX(),(float)p2.getY(),(float)p2.getZ());
				}
			}
			popMatrix();

			pushMatrix();
			//translate(0,0,sunPos);
			translate((float)sunPos.getX(),(float)sunPos.getY(),(float)sunPos.getZ());
			noStroke();
			fill(255,255,0);
			sphereDetail(8);
			sphere(sunWeight);
			popMatrix();

			pushMatrix();
			translate(0,0,sourcePos);
			image(source,-source.width/2,-source.height/2);
			popMatrix();

			pushMatrix();
			translate(0,0,targetPos);
			image(target,-target.width/2,-target.height/2);
			popMatrix();

			popMatrix();

		}
	}

}
