package graf.gravitationalLensing;

public class Particle {
	
	private double x, y, z;
	
	public Particle(double x, double y, double z) {
		this.x=x;
		this.y=y;
		this.z=z;
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public double getZ() {
		return z;
	}

}
