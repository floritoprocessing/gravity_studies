package graf.gravitationalLensing;

import java.util.Vector;

public class Raytrace {

	private Vec pos;
	private Vec mov;
	private Vec sunPos;
	
	private Vector<Vec> tracePositions;
	
	public Raytrace(double startX, double startY, double startZ, 
			double movX, double movY, double movZ,
			Vec sunPos, 
			double gravMass,
			double targetZ, 
			int maxIterations) {
		
		pos = new Vec(startX,startY,startZ);
		mov = new Vec(movX,movY,movZ);
		//sunPos = new Vec(gravX,gravY,gravZ);
		
		boolean endIsLarger;
		if (startZ<targetZ) endIsLarger=true;
		else endIsLarger=false;
		
		boolean crossed = false;
		int iteration = 0;
		
		tracePositions = new Vector<Vec>();
		
		while (!crossed && iteration<maxIterations) {
			
			double dist = Vec.distance(pos,sunPos);
			double grav = gravMass/(dist*dist);
			Vec acc = sunPos.clone();
			acc.sub(pos);
			acc.normalize();
			acc.mul(grav);
			mov.add(acc);
			pos.add(mov);
			tracePositions.add(pos.clone());
			if (endIsLarger && pos.getZ()>=targetZ) crossed=true;
			else if (!endIsLarger && pos.getZ()<=targetZ) crossed=true;
			iteration++;
		}
		
		if (!crossed) tracePositions=null;
		
	}
	
	public Vector<Vec> getTrace() {
		return tracePositions;
	}
	
}
