package graf.gravitationalLensing;

public class Vec {

	private double x=0, y=0, z=0;
	
	public Vec() {
	}
	
	public Vec(double x, double y) {
		this.x = x;
		this.y = y;
	}

	public Vec(double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public Vec clone() {
		return new Vec(this.x,this.y,this.z);
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public double getZ() {
		return z;
	}

	public void setZ(double z) {
		this.z = z;
	}

	public void add(Vec vec) {
		x+=vec.x;
		y+=vec.y;
		z+=vec.z;
	}
	
	public void sub(Vec vec) {
		x-=vec.x;
		y-=vec.y;
		z-=vec.z;
	}
	
	public String toString() {
		return x+" "+y+" "+z;
	}

	public static double distance(Vec a, Vec b) {
		double dx = a.x-b.x;
		double dy = a.y-b.y;
		double dz = a.z-b.z;
		return Math.sqrt(dx*dx+dy*dy+dz*dz);
	}
	
	public double getLength() {
		return Math.sqrt(x*x+y*y+z*z);
	}

	public void normalize() {
		double len = getLength();
		if (len!=0) div(len);
	}

	public void div(double f) {
		x/=f;
		y/=f;
		z/=f;
	}
	
	public void mul(double f) {
		x*=f;
		y*=f;
		z*=f;
	}

}
