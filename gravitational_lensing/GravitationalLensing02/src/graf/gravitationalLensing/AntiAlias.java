package graf.gravitationalLensing;

import processing.core.PImage;

public class AntiAlias {

	public static int mix(int c0, double p0, int c1, double p1) {
		int r0 = (int)Math.max(0,Math.min(255,((c0>>16&0xFF)*p0)));
		int g0 = (int)Math.max(0,Math.min(255,((c0>>8&0xFF)*p0)));
		int b0 = (int)Math.max(0,Math.min(255,((c0&0xFF)*p0)));
		int r1 = (int)Math.max(0,Math.min(255,((c1>>16&0xFF)*p1)));
		int g1 = (int)Math.max(0,Math.min(255,((c1>>8&0xFF)*p1)));
		int b1 = (int)Math.max(0,Math.min(255,((c1&0xFF)*p1)));
		return (r0+r1)<<16|(g0+g1)<<8|(b0+b1);
	}

	public static int get(PImage img, double x, double y) {
		int x0 = (int)x, x1=x0+1;
		int y0 = (int)y, y1=y0+1;
	
		//if (x0<0 || y0<0 || x1>=img.width || y1>=img.height) return 0xff000000;
	
		double px1 = x-x0, px0 = 1-px1;
		double py1 = y-y0, py0 = 1-py1;
		double p00 = px0*py0;
		double p01 = px0*py1;
		double p10 = px1*py0;
		double p11 = px1*py1;
	
		int c00 = img.get(x0,y0);
		int c01 = img.get(x0,y1);
		int c10 = img.get(x1,y0);
		int c11 = img.get(x1,y1);
	
		int c = 0xff000000 | mix( mix(c00,p00,c01,p01) , 1, mix(c10,p10,c11,p11), 1 );
	
		return c;
	}

}
