package graf.gravitationalLensing;

import java.util.Vector;

import processing.core.PApplet;
import processing.core.PGraphics;
import processing.core.PImage;
import processing.opengl.*;

public class GravitationalLensing extends PApplet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -273997643840753965L;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//graf.gravitationalLensing.GravitationalLensing
		PApplet.main("graf.gravitationalLensing.GravitationalLensing");
	}

	private final String path = "";
	private final String sourceName = "Perla.jpg";

	PImage source = new PImage(374*5,322*5);
	PImage target = new PImage(374*5,322*5);

	final float sourcePos = -4000;
	final float targetPos = 0;

	/*final Vec[] sunPos = {
			new Vec(-200,100,-2000),
			new Vec(-200,100,-2000)
	};*/

	private static final Vec centerOfRotation = new Vec(0,0,-2000);
	
	private Sun[] sun;/* = { 
			new Sun(-200,100,-2000,500),
			new Sun(200,-300,-2500,200),
			new Sun(10,10,-1000,200),
			new Sun(-350,-350,-200,500),
			new Sun(800,600,-3500,800)
			};*/

	private int targetX = 0, targetY = 0;
	private static final float lightSpeed = -10;
	private boolean raytrace = true;
	private final int maxIterations = (int)(2*(sourcePos-targetPos)/lightSpeed); // more than lightSpeed / (sourcePos-targetPos)

	private boolean display3d = true;

	private long imageTime;

	private static final int renderPassBase = 10;
	private int renderPasses = 2;
	private int renderPass = 0;
	private int renderBoxSize = (int)Math.pow(renderPassBase,(renderPasses-renderPass-1));
	
	private float rotX = 0;
	private float rotXadd = 0;
	private float rotY = PI/3;
	private float rotYadd = 0;
	private float mouseDownX = 0;
	private float mouseDownY = 0;
	
	
	public void settings() {
		size(600,600,P3D);
	}
	public void setup() {

		
		background(0);

		sun = new Sun[6];
		for (int i=0;i<sun.length;i++) {
			sun[i] = new Sun(random(-source.width/2,source.width/2),
					random(-source.height/2,source.height/2),
					random(sourcePos,targetPos),
					random(100,1000)
					);
		}
		
		PImage img = loadImage(path+sourceName);
		img.loadPixels();
		source.copy(img,0,0,img.width,img.height,0,0,source.width,source.height);
		source.updatePixels();
		
		//PGraphics tmpg = createGraphics(source.width, source.width, OPENGL);
		//tmpg.image(loadImage(path+sourceName),0,0,tmpg.width,tmpg.height);
		//source.copy(tmpg, 0, 0, tmpg.width,tmpg.height, 0, 0, source.width,source.height);

		imageTime = System.currentTimeMillis();
	}

	public void keyPressed() {
		if (key=='d') display3d =!display3d;
	}
	
	public void mousePressed() {
		mouseDownX = mouseX;
		mouseDownY = mouseY;
	}
	
	public void mouseReleased() {
		rotX += rotXadd;
		rotXadd = 0;
		rotY += rotYadd;
		rotYadd = 0;
	}

	public void draw() {
		
		if (mousePressed) {
			rotXadd = (mouseY-mouseDownY)/300;
			rotYadd = (mouseX-mouseDownX)/300;
		}

		/*
		 * Raytracing!
		 * Now shoot Particles from target straight towards source and let them be influenced by a 'sun' in the middle
		 * When the Particle crosses the source plane, read the color and put in on the target
		 */

		Vector<Vec> ray = new Vector<Vec>();

		long ti = System.currentTimeMillis();
		int traceColor = color(255,255,255);

		if (raytrace)
			while (System.currentTimeMillis()<ti+40) {

				Raytrace trace = new Raytrace(
						targetX-target.width/2,targetY-target.height/2,targetPos,
						0,0,lightSpeed,
						sun,
						sourcePos,
						maxIterations);
				ray = trace.getTrace();

				if (raytrace) {
					if (ray==null)
						target.set(targetX, targetY, color(0,0,0,0));
					else {
						double sourceX = (ray.elementAt(ray.size()-1).getX() + source.width/2.0);
						double sourceY = (ray.elementAt(ray.size()-1).getY() + source.height/2.0);
						traceColor = AntiAlias.get(source,sourceX,sourceY);
						for (int xo=0;xo<renderBoxSize;xo++) for (int yo=0;yo<renderBoxSize;yo++)
							target.set(targetX+xo, targetY+yo, traceColor);
					}
					target.updatePixels();
				}
				
				//if (frameCount%1000==0) target.save(path+"intermediateResult.tga");

				targetX+=renderBoxSize;
				if (targetX>=target.width) {
					targetX=0;
					targetY+=renderBoxSize;
					if (targetY>=target.height) {

						target.save(path+"result_1.tga");
						System.out.println("Rendering time: "+(int)((System.currentTimeMillis()-imageTime)/1000)+" seconds");
						raytrace=false;
					}
				}

			}


		if (!raytrace) {
			renderPass++;
			if (renderPass<renderPasses) {
				System.out.println("render pass: "+renderPass);
				targetX=0;
				targetY=0;
				renderBoxSize = (int)Math.pow(renderPassBase,(renderPasses-renderPass-1));
				raytrace=true;
			} else {
				System.out.println("END");
				noLoop();
			}
		}

		/*
		 * DISPLAY images, 3d model, raytrace
		 */

		background(128);
		float sScale = ((float)width/2.8f)/source.width;
		float sW = source.width*sScale, sH = source.height*sScale;
		float tScale = ((float)width/2.8f)/target.width;
		float tW = target.width*tScale, tH = target.height*tScale;

		if (display3d) {
			pushMatrix();
			image(source, width/4 - sW/2, height/4 - sH/2, sW, sH);
			image(target, width/4*3 - tW/2, height/4 - tH/2, tW, tH);
			popMatrix();

			pushMatrix();

			translate(width/2,height/2+1000);
			translate(0,0,-2000);
			translate((float)centerOfRotation.getX(),(float)centerOfRotation.getY(),(float)centerOfRotation.getZ());
			
			rotateY(rotY+rotYadd);
			rotateZ(rotX+rotXadd);
			
			translate(-(float)centerOfRotation.getX(),-(float)centerOfRotation.getY(),-(float)centerOfRotation.getZ());

			pushMatrix();
			if (ray!=null) {
				stroke(traceColor);
				for (int i=0;i<ray.size()-1;i++) {
					Vec p1 = ray.elementAt(i);
					Vec p2 = ray.elementAt(i+1);
					line((float)p1.getX(),(float)p1.getY(),(float)p1.getZ(),
							(float)p2.getX(),(float)p2.getY(),(float)p2.getZ());
				}
			}
			popMatrix();

			for (int i=0;i<sun.length;i++) {
				pushMatrix();
				translate((float)sun[i].getPos().getX(),(float)sun[i].getPos().getY(),(float)sun[i].getPos().getZ());
				noStroke();
				fill(255,255,0);
				sphereDetail(8);
				sphere((float)sun[i].getMass()/4);
				popMatrix();
			}

			pushMatrix();
			translate(0,0,sourcePos);
			image(source,-source.width/2,-source.height/2);
			popMatrix();

			pushMatrix();
			translate(0,0,targetPos);
			image(target,-target.width/2,-target.height/2);
			popMatrix();

			popMatrix();

		} else {
			float wStretch = (float)width/target.width;
			float hStretch = (float)height/target.height;
			float stretch = Math.min(wStretch,hStretch);
			float left = (width-target.width*stretch)/2;
			float top = (height-target.height*stretch)/2;
			image(target,left,top,target.width*stretch,target.height*stretch);
		}
	}

}
