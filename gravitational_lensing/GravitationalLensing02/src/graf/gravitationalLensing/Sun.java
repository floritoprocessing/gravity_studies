package graf.gravitationalLensing;

public class Sun extends Vec {

	private double mass = 100;
	
	public Sun(double x, double y, double z, double mass) {
		super(x,y,z);
		this.mass = mass;
	}

	public Vec getPos() {
		return (Vec)this;
	}

	public double getMass() {
		return mass;
	}

	public void setMass(double mass) {
		this.mass = mass;
	}
	
}
