import java.util.Vector;

Vector suns = new Vector();
Vector planets = new Vector();

Preset3 preset = new Preset3();

void setup() {
  size(800,800,P3D);
  background(0,0,0);
  
  for (int i=0;i<preset.SUN_AMOUNT;i++) {
    SpaceObject sun = new SpaceObject(preset.SUN_MASS,preset.SUN_RADIUS,preset.SUN_COLOR,width*random(0.0,1.0),height*random(0.0,1.0));
    sun.setMass(random(0.25,1.0)*preset.SUN_MASS);
    sun.setVelocity(random(-preset.SUN_SPEED,preset.SUN_SPEED),random(-preset.SUN_SPEED,preset.SUN_SPEED));
    suns.add(sun);
  }

  for (int i=0;i<preset.PLANET_AMOUNT;i++) {
    SpaceObject planet = new SpaceObject(preset.PLANET_MASS,preset.PLANET_RADIUS,preset.PLANET_COLOR,random(width),random(height));
    planet.setVelocity(random(-preset.PLANET_SPEED,preset.PLANET_SPEED),random(-preset.PLANET_SPEED,preset.PLANET_SPEED));
    planet.setDrawMode(SpaceObject.DRAWMODE_POINT);
    planets.add(planet);
  }
}

void draw() {
  for (int f=0;f<50;f++) {
  //background(0,0,0);
  for (int i=0;i<planets.size();i++) ((SpaceObject)(planets.elementAt(i))).accelerateTo(suns);
  
  for (int i=0;i<suns.size();i++) {
    Vector nextSun = new Vector();
    int j=i+1;
    if (j==suns.size()) j=0;
    nextSun.add(suns.elementAt(j));
    ((SpaceObject)(suns.elementAt(i))).accelerateTo(nextSun);
  }
  
  for (int i=0;i<planets.size();i++) ((SpaceObject)(planets.elementAt(i))).move();
  for (int i=0;i<suns.size();i++) ((SpaceObject)(suns.elementAt(i))).move();
  
  //for (int i=0;i<planets.size();i++) ((SpaceObject)(planets.elementAt(i))).looseMass(preset.PLANET_MASSLOSS);
  for (int i=0;i<suns.size();i++) ((SpaceObject)(suns.elementAt(i))).slowDown(preset.SUN_SLOWDOWN);
  

  if (preset.DRAW_SUN) for (int i=0;i<suns.size();i++) ((SpaceObject)(suns.elementAt(i))).draw();
  for (int i=0;i<planets.size();i++) ((SpaceObject)(planets.elementAt(i))).draw();
  }
}
