class Preset2 {
  
  public static final boolean DRAW_SUN = true;
  
  public static final int SUN_AMOUNT = 50;
  public static final float SUN_MASS = 50;
  public static final float SUN_RADIUS = 1;
  public static final float SUN_SPEED = 0.1;
  public static final float SUN_SLOWDOWN = 0.996;
  
  public static final int PLANET_AMOUNT = 1500;
  public static final float PLANET_MASS = 0.001;
  public static final float PLANET_RADIUS = 1;
  public static final float PLANET_SPEED = 1;
  public static final float PLANET_MASSLOSS = 0.999;
  
  public static final float MINIMUM_GRAVITY_DISTANCE = 15.0;
  
  public static final float G = 20.0;
  
  public static final int PLANET_COLOR = 0x080040FF;
  public static final int SUN_COLOR = 0x10FFFFFF;
  
  Preset2() {
  }
}
