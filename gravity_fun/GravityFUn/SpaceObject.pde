class SpaceObject {
  
  float px=0, py=0;
  float vx=0, vy=0;
  float ax=0, ay=0;
  
  float mass=1;
  float radius=5;
  
  int col = 0xFF000000;
  
  int drawMode = 1;
  
  public static final int DRAWMODE_POINT = 0;
  public static final int DRAWMODE_ELLIPSE = 1;
  
  SpaceObject(float _mass, float _radius, color _col, float _px, float _py) {
    mass = _mass;
    radius = _radius;
    col = _col;
    px = _px;
    py = _py;
  }
  
  void setVelocity(float _vx, float _vy) {
    vx = _vx;
    vy = _vy;
  }
  
  void setDrawMode(int dm) {
    drawMode = dm;
  }
  
  void accelerateTo(Vector objects) {
    ax=0;
    ay=0;
    
    for (int i=0;i<objects.size();i++) {
      SpaceObject s = (SpaceObject)(objects.elementAt(i));
      float dx = s.getPosX() - px;
      float dy = s.getPosY() - py;
      float r2 = dx*dx + dy*dy;
      float r = sqrt(r2);
      float a = preset.G*s.getMass()/r2;
      if (r>preset.MINIMUM_GRAVITY_DISTANCE) {
        float aDivR = a/r;
        ax += aDivR*dx;
        ay += aDivR*dy;
      }
    }
    
    vx+=ax;
    vy+=ay;
  }
  
  void move() {
    px+=vx;
    py+=vy;
  }
  
  void slowDown(float f) {
    vx*=f;
    vy*=f;
  }
  
  void looseMass(float f) {
    mass*=f;
  }
  
  float getPosX() {
    return px;
  }
  
  float getPosY() {
    return py;
  }
  
  void setMass(float _mass) {
    mass = _mass;
  }
  
  float getMass() {
    return mass;
  }
  
  void draw() {
    if (drawMode == DRAWMODE_POINT) {
      stroke(col);
      point(px,py);
    } else {
      noStroke();
      fill(col);
      ellipseMode(CENTER);
      ellipse(px,py,radius,radius);
    }
  }
  
}
