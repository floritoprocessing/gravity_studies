import java.util.ArrayList;

import processing.core.PApplet;

/**
 * 
 */

/**
 * @author Marcus
 *
 */
public class DrawableLightLine {
	
	int color;
	float[] xyz;
	
	public DrawableLightLine(float[] xyz, int color) {
		this.xyz = new float[xyz.length];
		System.arraycopy(xyz, 0, this.xyz, 0, xyz.length);
		this.color = color;
	}
	
	public void draw(PApplet pa) {
		pa.noFill();
		pa.stroke(color);
		pa.beginShape(PApplet.POLYGON);
		for (int i=0;i<xyz.length;i+=3) {
			pa.vertex(xyz[i],xyz[i+1],xyz[i+2]);
		}
		pa.endShape(PApplet.OPEN);
	}
}
