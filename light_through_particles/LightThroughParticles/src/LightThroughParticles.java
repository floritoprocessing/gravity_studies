
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Calendar;

import lightparticlesystem.Light;
import lightparticlesystem.LightParticleSystem;
import lightparticlesystem.LightType;
import lightparticlesystem.Particle;
import lightparticlesystem.PhotoPlateXY;
import lightparticlesystem.Vec;
import processing.core.PApplet;
import processing.core.PFont;

/**
 * 
 */

/**
 * @author Marcus
 * 
 */
public class LightThroughParticles extends PApplet {

	
	float SYSTEM_WIDTH = 600;
	float SYSTEM_HEIGHT = 800;
	float SYSTEM_DEPTH = 1200;
	float SYSTEM_GRAVITY_CONSTANT = 1000;
	float SYSTEM_LIGHTSPEED = 5;
	
	float LIGHTS_SHOOT_PERCENTAGE_STEP = 0.002f; // 0.0005
	float LIGHTS_SHOOT_JITTER_PERCENTAGE = 0.000f;//0.0015f; // 0.02
	
	Light[] LIGHTS = createEightLights();
	
	Particle[] PARTICLES = createThreeRings();
	
	float PHOTOPLATE_X = 0;
	float PHOTOPLATE_Y = 0;
	float PHOTOPLATE_Z = -200;
	
	int PHOTOPLATE_PIXEL_WIDTH = 1600;
	int PHOTOPLATE_PIXEL_HEIGHT = 1000;
	float PHOTOPLATE_WIDTH = 400;
	float PHOTOPLATE_HEIGHT = 250;
	
	
	/*
	 * Draw settings
	 */
	
	float displaySettings_lightLinesOpacity 	= 64;
	float displaySettings_lightsourceSpacing 	= 19.9f;
	float displaySettings_particleSphereSize 	= 4.0f;
	float displaySettings_camLon 	= 100;
	float displaySettings_camLat 	= -10;
	float displaySettings_camZ 	= 500;
	
	private static final String imageExtension = ".jpg";
	private static String imageOutputFolder = "output/LightThroughParticles/";
	
	/**
	 * Array containing light lines to draw (only calculated on first frame)
	 */
	ArrayList<DrawableLightLine> draw_lightLines = null;
	boolean drawTheLightLines = true;
	
	boolean savePhotoPlateForEachLight = true;
	
	/**
	 * Amount of milliseconds to spend for calculating rays per frame
	 */
	int calcSettings_rayCalculationTimePerFrame = 40;
	int calcSettings_updateImageEveryNFrames = 250;
	float stats_raysPerFrame = 0;
	long stats_totalRaysCalculated = 0;

	/*
	 * Variables used for iterating through light source positions
	 */
	int lightIndex = 0;
	float lightPercentageX = 0;
	float lightPercentageY = 0;
	
	LightParticleSystem lps;;
	
	

	enum ViewMode {
		CAMERA,
		PHOTO_PLATE
	}
	
	PlateImage photoPlateImage;
	
	boolean imageUpdated = false;
	boolean photoPlateSaved = false;
	
	ViewMode viewMode = ViewMode.CAMERA;
	
	PFont font;

	public void settings() {
		size(800, 600, P3D);
	}
	
	public void setup() {
		
		ArrayList<String> asStrings = uppercapFieldsAsString(this);
		for (String s:asStrings) {
			System.out.println(s);
		}
		String settingsName = imageOutputFolder+"LightThroughParticles "+dateToString()+" settings.txt";
		saveStrings(settingsName, asStrings.toArray(new String[0]));
		
		
		font = createFont("Verdana", 14);
		textFont(font);
		
		lps = new LightParticleSystem(SYSTEM_WIDTH, SYSTEM_HEIGHT, SYSTEM_DEPTH);

		lps.setLightSpeed(SYSTEM_LIGHTSPEED);
		
		for (Particle p:PARTICLES) {
			lps.addParticle(p);
		}
		
		for (Light l:LIGHTS) {
			lps.addLight(l);
		}
		
		photoPlateImage = new PlateImage(this, PHOTOPLATE_PIXEL_WIDTH, PHOTOPLATE_PIXEL_HEIGHT);		
		lps.setPhotoPlateXY(PHOTOPLATE_X, PHOTOPLATE_Y, PHOTOPLATE_Z, PHOTOPLATE_WIDTH, PHOTOPLATE_HEIGHT);
	}

	/**
	 * @return
	 */
	private static ArrayList<String> uppercapFieldsAsString(Object mainObject) {
		String NL = System.getProperty("line.separator");
		Field[] fields = mainObject.getClass().getDeclaredFields();
		ArrayList<String> asStrings = new ArrayList<String>();
		for (Field f:fields) {
			String name = f.getName();
			if (name.equals(name.toUpperCase())) {
				Object value = null;
				try {
					value = f.get(mainObject);
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
				if (value!=null) {
					if (!value.getClass().isArray()) {
						String type = f.getType().getSimpleName();
						asStrings.add(type+" "+name+" = "+value+";");
					} else {
						//	TODO: array
						String type = f.getType().getComponentType().getSimpleName();
						
						String one = "";
						one = type+"[] "+name+" = {"+NL;
						int amount = Array.getLength(value);
						for (int i=0;i<amount;i++) {
							if (i<amount-1)
								one += "\t"+Array.get(value, i).toString()+","+NL;
							else
								one += "\t"+Array.get(value, i).toString()+NL;
						}
						one += "};";
						asStrings.add(one);
					}
				}
			}
		}
		return asStrings;
	}

	

	/*
	 * public void mouseDragged() { camLon += (mouseX-pmouseX)*1; camLat -=
	 * (mouseY-pmouseY)*1; println(camLon+", "+camLat); }
	 */

	private boolean saveScreenshot;

	public void mouseDragged() {
		if (mousePressed) {
			displaySettings_camLon += (mouseX-pmouseX)*0.5f;
			displaySettings_camLat -= (mouseY-pmouseY)*0.5f;
		}
	}
	
	private float[] calculateRays() {

		//get lights and photo plates
		Light[] lights = lps.getLights();
		//PhotoPlateXY[] photoPlates = lps.getPhotoPlatesXY();
		PhotoPlateXY photoPlate = lps.getPhotoPlateXY();

		/*
		 * 
		 *  SHOOT RAYS ON TO PHOTO PLATE
		 *  
		 */
		
		Vec rayPos = null;
		int rayCount = 0;
		
		float[][] pathAndHitpoint;
		float[] hitPoints;
		float xGlobal, yGlobal;
		float xOnPlatePercentage, yOnPlatePercentage;
		float x, y;
		
		long endCalcTime = System.currentTimeMillis() + calcSettings_rayCalculationTimePerFrame;
		boolean endTimeReached = false;
		while (
			!endTimeReached
			//System.currentTimeMillis()<endCalcTime
			//rayCount < raysPerFrame 
			&& lightIndex < lights.length) {

			if (rayCount%10==9) {
				endTimeReached = (System.currentTimeMillis()>=endCalcTime);
			}
			rayCount++;
			
			Light currentLight = lights[lightIndex];
			int currentRed = currentLight.getR();
			int currentGreen = currentLight.getG();
			int currentBlue = currentLight.getB();
			
			// shoot ray!
			if (LIGHTS_SHOOT_JITTER_PERCENTAGE!=0) {
				rayPos = currentLight.getPointAtPercentage(
						lightPercentageX+random(-LIGHTS_SHOOT_JITTER_PERCENTAGE, LIGHTS_SHOOT_JITTER_PERCENTAGE), 
						lightPercentageY+random(-LIGHTS_SHOOT_JITTER_PERCENTAGE, LIGHTS_SHOOT_JITTER_PERCENTAGE));
			} else {
				rayPos = currentLight.getPointAtPercentage(lightPercentageX, lightPercentageY);
			}

			// ray direction
			Vec dir = new Vec(0, 0, -lps.getLightSpeed());
			LightParticleSystem.rotate(dir, currentLight.getLongitude(),
					currentLight.getLatitude());

			pathAndHitpoint = lps.getPathAndHitPoint(rayPos.x,
					rayPos.y, rayPos.z, dir.x, dir.y, dir.z, false, 2000);
			// float[] xyz = pathAndHitpoint[0];

			hitPoints = pathAndHitpoint[1];
			if (hitPoints.length > 0) {
				for (int h = 0; h < hitPoints.length; h += 3) {
					xGlobal = hitPoints[h];
					yGlobal = hitPoints[h+1];
					//float zGlobal = hitPoints[h + 2];
					xOnPlatePercentage = photoPlate.getXPercentageOnPlate(xGlobal);
					yOnPlatePercentage = photoPlate.getYPercentageOnPlate(yGlobal);
					x = xOnPlatePercentage * PHOTOPLATE_PIXEL_WIDTH;
					y = yOnPlatePercentage * PHOTOPLATE_PIXEL_HEIGHT;
					photoPlateImage.setAdd(x,y,currentRed,currentGreen,currentBlue);
				}
			}
			
			if (savePhotoPlateForEachLight && lightIndex<lights.length &&
					lightPercentageX==0 && lightPercentageY==0) {
				photoPlateImage.updatePImage();
				imageUpdated = true;
				savePhotoPlate();
			}

			// increase light percentage
			lightPercentageX += LIGHTS_SHOOT_PERCENTAGE_STEP;
			if (lightPercentageX > 1) {
				lightPercentageX = 0;
				lightPercentageY += LIGHTS_SHOOT_PERCENTAGE_STEP;
				if (lightPercentageY > 1) {
					lightPercentageY = 0;
					lightIndex++;
				}
			}
		}
		
		return new float[] {rayCount};
		
		/*if (rayPos==null) {
			return new float[] {rayCount};
		} else {
			return new float[] {rayCount};//, rayPos.x, rayPos.y, rayPos.z };
		}*/
		
		//return rayCount;
	}
	
	public void draw() {

		/*
		 * Calculate and project the rays
		 */
		float[] returnArgs = calculateRays();
		int rayCount = (int)returnArgs[0];
		
		
		Light[] lights = lps.getLights();
		
		
		// update photoPlate
		if (frameCount%calcSettings_updateImageEveryNFrames==0) {
			photoPlateImage.updatePImage();
			imageUpdated = true;
		}

		// save result!
		if (lightIndex == lights.length && !photoPlateSaved) {
			photoPlateSaved = true;
			photoPlateImage.updatePImage();
			imageUpdated = true;
			savePhotoPlate();
		}
		
		
		
		
		/*
		 * 
		 * Start drawing on screen
		 * 
		 */
		
		
		// clear background
		background(48);
		
		beginCamera();
		camera();
		fill(255);
		if (stats_raysPerFrame==0) {
			stats_raysPerFrame = rayCount;
		} else {
			stats_raysPerFrame = 0.95f*stats_raysPerFrame + 0.05f*rayCount;
		}
		stats_totalRaysCalculated += rayCount;
		text("Frame "+frameCount+": Rays calculated = "+rayCount+"" +
				". Rotal rays = "+ stats_totalRaysCalculated +
				". Average of "+nf(stats_raysPerFrame,1,1)+" rays per frame."
				,10,20);
		if (imageUpdated) {
			text("Image updated",10,40);
			imageUpdated = false;
		}
		text("Light "+(lightIndex+1)+"/"+lights.length+": yPerc="+nf(lightPercentageY*100,1,1)+"%",10,height-30);
		text("FrameRate = "+frameRate,10,height-10);
		endCamera();
		
		
		if (viewMode==ViewMode.PHOTO_PLATE) {
			
			float stretchX = (float)width/(float)PHOTOPLATE_PIXEL_WIDTH;
			float stretchY = (float)height/(float)PHOTOPLATE_PIXEL_HEIGHT;
			float stretch = Math.min(stretchX, stretchY);
			camera();
			translate(width/2,height/2);
			scale(stretch);
			translate(-PHOTOPLATE_PIXEL_WIDTH/2, -PHOTOPLATE_PIXEL_HEIGHT/2);
			image(photoPlateImage.getPImage(), 0, 0);
			
		} else if (viewMode==ViewMode.CAMERA) {
			// now set camera position
			Vec camPos = new Vec(0, 0, displaySettings_camZ);
			LightParticleSystem.rotate(camPos, displaySettings_camLon, displaySettings_camLat);
			camera(camPos.x, camPos.y, camPos.z, 
					0, 0, 0,
					0, 1, 0);
			
			// draw source
			if (lightIndex < lights.length) { //rayPos != null && 
				Vec rgb = new Vec(lights[lightIndex].getR(),
						lights[lightIndex].getG(), lights[lightIndex].getB());
				rgb.normalize();
				rgb.mul(255);
				fill(rgb.x, rgb.y, rgb.z);
				noStroke();
	
				pushMatrix();
				Vec rayPos = lights[lightIndex].getPointAtPercentage(lightPercentageX,
						lightPercentageY);
				translate(rayPos.x, rayPos.y, rayPos.z);
				box(10);
				popMatrix();
	
			}
	
			// draw cube
			drawSystemCube();
	
			// draw particles
			drawParticles();
	
			// draw lights
			// Light[] lights = lps.getLights();
			boolean calculateDrawLightPaths = true;
			if (draw_lightLines == null) {
				draw_lightLines = new ArrayList<DrawableLightLine>();
				calculateDrawLightPaths = true;
			} else {
				calculateDrawLightPaths = false;
			}
	
			noFill();
			for (Light l : lights) {
				switch (l.getType()) {
				case DIRECTIONAL:
	
					// DIRECTIONAL light: draw square
					Vec rgb = new Vec(l.getR(), l.getG(), l.getB());
					rgb.normalize();
					rgb.mul(255);
					stroke(rgb.x, rgb.y, rgb.z, 128);
					Vec[] corners = LightParticleSystem.createCornersOfQuad(l);
					beginShape(QUADS);
					for (Vec c : corners) {
						vertex((float) c.x, (float) c.y, (float) c.z);
					}
					endShape();
	
					// DIRECTIONAL light: draw light lines
					stroke(rgb.x, rgb.y, rgb.z, 64);
					// float movStep = -lps.getLightSpeed();
					float w2 = l.getWidth() / 2.0f;
					float h2 = l.getHeight() / 2.0f;
	
					Vec movement = new Vec(0, 0, -lps.getLightSpeed());
					LightParticleSystem.rotate(movement, l.getLongitude(),
							l.getLatitude());
					float xm = (float) movement.x;
					float ym = (float) movement.y;
					float zm = (float) movement.z;
	
					// beginShape(POINTS);
					if (calculateDrawLightPaths) {
						float x, y;
						float[][] pathAndHitpoint;
						float[] hitPoints;
						for (y = -h2; y <= h2; y += displaySettings_lightsourceSpacing) {
							for (x = -w2; x <= w2; x += displaySettings_lightsourceSpacing) {
	
								float z = 0;
								Vec start = new Vec(x, y, z);
								LightParticleSystem.rotate(start, l.getLongitude(),
										l.getLatitude());
								start.add(l.getX(), l.getY(), l.getZ());
								float xx = (float) start.x;
								float yy = (float) start.y;
								float zz = (float) start.z;
	
								// light path:
								pathAndHitpoint = lps.getPathAndHitPoint(
										xx, yy, zz, xm, ym, zm, true, 2000);
								float[] drawLightLinesXyz = pathAndHitpoint[0];
								hitPoints = pathAndHitpoint[1];
								drawLightLinesXyz = reduceXYZ(drawLightLinesXyz,
										0.5f);
								
								draw_lightLines.add(new DrawableLightLine(drawLightLinesXyz, 
										color(rgb.x,rgb.y,rgb.z,displaySettings_lightLinesOpacity)));
								
							}
						}
					}
	
					break;
	
				default:
					throw new RuntimeException("Unimplemented type " + l.getType());
				}
				
				//println(drawLightLinesXYZ.size());
				
			} // end Lights
	
			if (drawTheLightLines) {
				for (DrawableLightLine ll:draw_lightLines) {
					ll.draw(this);
				}
			}
			
					
			// draw Photo plates
			//PhotoPlateXY[] plates = lps.getPhotoPlatesXY();
			
			//for (PhotoPlateXY pp : plates) {
			
	
				// get the corners
				PhotoPlateXY pp = lps.getPhotoPlateXY();
				float[] cf = pp.getCornersXYZ();
				Vec[] corners = new Vec[] { new Vec(cf[0], cf[1], cf[2]),
						new Vec(cf[3], cf[4], cf[5]), new Vec(cf[6], cf[7], cf[8]),
						new Vec(cf[9], cf[10], cf[11]) };// LightParticleSystem.createCornersOfQuad(pp);
				Vec[] textureCorners = new Vec[] { new Vec(0, 0), new Vec(1, 0),
						new Vec(1, 1), new Vec(0, 1) };
				noStroke();//stroke(255);
				fill(255);
				beginShape(QUADS);
				textureMode(NORMAL);
				
				texture(photoPlateImage.getPImage());
				int cc = 0;
				for (Vec c : corners) {
					vertex((float) c.x, (float) c.y, (float) c.z,
							(float) textureCorners[cc].x,
							(float) textureCorners[cc].y);
					cc++;
				}
				endShape();
	
				Vec side1 = Vec.sub(corners[1], corners[0]);
				Vec side2 = Vec.sub(corners[1], corners[2]);
	
				// draw normal of plane
				Vec normal = Vec.cross(side1, side2);
				normal.normalize();
				normal.mul(100);
				Vec n0 = new Vec(pp.getX(), pp.getY(), pp.getZ());
				Vec n1 = Vec.add(n0, normal);
				beginShape(LINES);
				vertex((float) n0.x, (float) n0.y, (float) n0.z);
				vertex((float) n1.x, (float) n1.y, (float) n1.z);
				endShape();
	
				
			//}
			
			//endCamera();
	
		}
		
		//camera();
		

		
		if (saveScreenshot) {
			saveScreenshot = false;
			String name = "LightThroughParticles " + dateToString() + " screenshot" + imageExtension;
			save(imageOutputFolder+name);
		}
		
	} // end draw

	
	
	/**
	 * 
	 */
	private void savePhotoPlate() {
		String date = dateToString();
		String name = "LightThroughParticles " + date + " photoplate" + imageExtension;
		photoPlateImage.getPImage().save(imageOutputFolder + name);
		println("Saved " + name + " to " + imageOutputFolder);
	}

	/**
	 * @return
	 */
	private static String dateToString() {
		Calendar c = Calendar.getInstance();
		String yyyy = nf(c.get(Calendar.YEAR), 4);
		String mm = nf(c.get(Calendar.MONTH) + 1, 2);
		String dd = nf(c.get(Calendar.DATE), 2);
		String HH = nf(c.get(Calendar.HOUR_OF_DAY), 2);
		String MM = nf(c.get(Calendar.MINUTE), 2);
		String SS = nf(c.get(Calendar.SECOND), 2);
		String date = yyyy + mm + dd + " " + HH + MM + SS;
		return date;
	}

	/**
	 * Draws the particles of the system as spheres
	 */
	private void drawParticles() {
		Particle[] particles = lps.getParticles();
		noStroke();
		fill(255, 0, 0);
		for (Particle p : particles) {
			pushMatrix();
			translate(p.getX(), p.getY(), p.getZ());
			sphere(displaySettings_particleSphereSize);
			popMatrix();
		}
	}

	/**
	 * Draws the LightParticleSystem cube
	 */
	private void drawSystemCube() {
		stroke(255, 0, 0, 64);
		noFill();
		box(lps.getWidth(), lps.getHeight(), lps.getDepth());
	}

	public void keyPressed() {
		if (key == 'p') {
			savePhotoPlate();
		} else if (key=='l') {
			drawTheLightLines = !drawTheLightLines;
		} else if (key=='s') {
			saveScreenshot = true;
		} else if (key=='v') {
			photoPlateImage.updatePixels();
			imageUpdated = true;
			switch (viewMode) {
			case CAMERA:
				viewMode = ViewMode.PHOTO_PLATE;
				break;
			case PHOTO_PLATE:
				viewMode = ViewMode.CAMERA;
				break;
			default:
				break;
			}
		}
	}

	/**
	 * @param xyz
	 * @param i
	 * @return
	 */
	public static float[] reduceXYZ(float[] xyz, float factor) {
		int pointCount = xyz.length / 3;
		int newPointCount = (int) (pointCount * factor);
		float[] out = new float[newPointCount * 3];
		for (int destPointIndex = 0; destPointIndex < newPointCount; destPointIndex++) {
			int sourcePointIndex = (int) (destPointIndex / factor);
			out[destPointIndex * 3] = xyz[sourcePointIndex * 3];
			out[destPointIndex * 3 + 1] = xyz[sourcePointIndex * 3 + 1];
			out[destPointIndex * 3 + 2] = xyz[sourcePointIndex * 3 + 2];
		}
		// System.arraycopy(xyz, 0, out, 0, out.length);
		return out;
	}

	/**
	 * 
	 */
	private static Particle[] createThreeRings() {
		float z = - 50;
		float rad = 40;
		float mass = 0.1f;
		ArrayList<Particle> particles = new ArrayList<Particle>();
		for (int xOff=-2;xOff<=2;xOff++) {
			for (int deg=0;deg<360;deg+=60) {
				if (xOff==0 || (xOff<0&&deg!=0) || (xOff>0&&deg!=180)) {
					float x = (float)(rad* Math.cos(deg*PI/180.0)) + xOff*2*rad;
					float y = (float)(rad* Math.sin(deg*PI/180.0));
					particles.add(new Particle(x, y, z, mass));
				}
			}
		}
		return particles.toArray(new Particle[0]);
	}
	
	private static Light[] createEightLights() {
		float r0 = 2, g0 = 40, b0 = 10;
		float r1 = 80, g1 = 20, b1 = 7;
		float z0 = 100, z1 = 90;
		float lat0 = -1f, lat1 = 3f;
		Light[] lights = new Light[16];
		for (int i=0;i<lights.length;i++) {
			float p = (float)i/(float)(lights.length-1);
			float x=0, y=0;
			float z = map(p, 0, 1, z0, z1);
			LightType type = LightType.DIRECTIONAL;
			int r = (int)map(p, 0, 1, r0, r1);
			int g = (int)map(p, 0, 1, g0, g1);
			int b = (int)map(p, 0, 1, b0, b1);
			float width = 400, height=200;
			float longitude = 0;
			float latitude = map(p, 0, 1, lat0, lat1);
			if (i>lights.length/2) {
				y += 100;
				latitude += 15;
			}
			
			float[] args = new float[] { width, height, longitude, latitude };
			Light l = new Light(x, y, z, type, r, g, b, args);
			lights[i] = l;
		}
		
		// inter leave lights
		/*Light[] l2 = new Light[lights.length];
		for (int i=0;i<lights.length;i++) {
			int si = (i%2)*(lights.length/2); // 0..8..0..8
			si += (i/2); //
			l2[si]=lights[i];
		}*/
		
		
		return lights;
	}
	
	public static void main(String[] args) {
		PApplet.main(new String[] { "LightThroughParticles" });
	}
}
