import processing.core.PApplet;
import processing.core.PImage;

/**
 * 
 */

/**
 * @author Marcus
 *
 */
public class PlateImage {
	
	private int width, height;
	private int count;
	private int[] r, g, b;
	private PImage pImage;
	
	public PlateImage(PApplet pa, int w, int h) {
		pImage = pa.createImage(w, h, PApplet.RGB);
		count = w*h;
		r = new int[count];
		g = new int[count];
		b = new int[count];
		this.width = w;
		this.height = h;
	}
	
	public void updatePImage() {
		for (int i=0;i<count;i++) {
			pImage.pixels[i] = 0xff000000 | r[i]<<16 | g[i]<<8 | b[i]; 
		}
		pImage.updatePixels();
	}
	
	public PImage getPImage() {
		return pImage;
	}
	
	public void updatePixels() {
		pImage.updatePixels();
	}

	/**
	 * @param x
	 * @param y
	 * @param r
	 * @param g
	 * @param b
	 */
	public void setAdd(float x, float y, int r, int g, int b) {
		int x0 = (int)x, x1 = x0+1;
		int y0 = (int)y, y1 = y0+1;
		if (x0>=0&&x1<width&&y0>=0&&y1<height) {
			float xp1 = x-x0, xp0 = 1-xp1;
			float yp1 = y-y0, yp0 = 1-yp1;
			float p00 = xp0*yp0;
			float p01 = xp0*yp1;
			float p10 = xp1*yp0;
			float p11 = xp1*yp1;
			int i00 = x0 + y0*width;
			int i01 = i00 + width;
			int i10 = i00 + 1;
			int i11 = i10 + width;
			this.r[i00] = (int)(Math.min(255, this.r[i00] + p00*r));
			this.r[i01] = (int)(Math.min(255, this.r[i01] + p01*r));
			this.r[i10] = (int)(Math.min(255, this.r[i10] + p10*r));
			this.r[i11] = (int)(Math.min(255, this.r[i11] + p11*r));
			this.g[i00] = (int)(Math.min(255, this.g[i00] + p00*g));
			this.g[i01] = (int)(Math.min(255, this.g[i01] + p01*g));
			this.g[i10] = (int)(Math.min(255, this.g[i10] + p10*g));
			this.g[i11] = (int)(Math.min(255, this.g[i11] + p11*g));
			this.b[i00] = (int)(Math.min(255, this.b[i00] + p00*b));
			this.b[i01] = (int)(Math.min(255, this.b[i01] + p01*b));
			this.b[i10] = (int)(Math.min(255, this.b[i10] + p10*b));
			this.b[i11] = (int)(Math.min(255, this.b[i11] + p11*b));
		}
	}
	
}
