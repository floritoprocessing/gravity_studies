/**
 * 
 */
package lightparticlesystem;


/**
 * @author Marcus
 *
 */
public class Light {

	float width, height, longitude, latitude;
	
	float x, y, z;
	int r=255, g=255, b=255;
	LightType type;
		
	public Light(
			float x, float y, float z, 
			LightType type, int r, int g, int b, float... lightArguments) {
		this.x=x;
		this.y=y;
		this.z=z;
		this.type = type;
		this.r=r;
		this.g=g;
		this.b=b;
		
		switch (type) {
		case DIRECTIONAL:
			if (lightArguments!=null && lightArguments.length==4) {
				width = Math.abs(lightArguments[0]);
				height = Math.abs(lightArguments[1]);
				longitude = lightArguments[2];
				latitude = lightArguments[3];
			} else {
				throw new RuntimeException("Directional light wants 4 floats!");
			}
			
			break;

		default:
			throw new RuntimeException("Unimplemented type "+type);
		}
	}
	
	public Vec getPointAtPercentage(float xp, float yp) {
		switch (type) {
		case DIRECTIONAL:
			Vec[] corners = LightParticleSystem.createCornersOfQuad(this);
			Vec topLeft = corners[0];
			Vec topRight = corners[1];
			Vec botRight = corners[2];
			Vec botLeft = corners[3];
			
			Vec topXPoint = Vec.add(topLeft, Vec.mul(Vec.sub(topRight,topLeft), xp));
			Vec botXPoint = Vec.add(botLeft, Vec.mul(Vec.sub(botRight,botLeft), xp));
			Vec point = Vec.add(topXPoint, Vec.mul(Vec.sub(botXPoint, topXPoint), yp));
			return point;
		default:
			throw new RuntimeException("Unimplemented type "+type);
		}
	}
	
	public String toString() {
		switch (type) {
		case DIRECTIONAL:
			String out = "";
			out = "new Light(";
			out += x+"f, ";
			out += y+"f, ";
			out += z+"f, ";
			out += type.getDeclaringClass().getSimpleName()+"."+type.name()+", ";
			out += r+"f, ";
			out += g+"f, ";
			out += b+"f, ";
			out += width+"f, ";//"float... lightArguments)";
			out += height+"f, ";
			out += longitude+"f, ";
			out += latitude+"f)";
			return out;
		default:
			throw new RuntimeException("Unimplemented type "+type);
		}
	}
	
	
	/**
	 * @return the type
	 */
	public LightType getType() {
		return type;
	}
	
	/**
	 * @return the r
	 */
	public int getR() {
		return r;
	}
	
	/**
	 * @return the g
	 */
	public int getG() {
		return g;
	}
	
	/**
	 * @return the b
	 */
	public int getB() {
		return b;
	}
	
	/**
	 * @return the x
	 */
	public float getX() {
		return x;
	}
	
	/**
	 * @return the y
	 */
	public float getY() {
		return y;
	}
	
	/**
	 * @return the z
	 */
	public float getZ() {
		return z;
	}
	
	/**
	 * @return the width
	 */
	public float getWidth() {
		return width;
	}
	
	/**
	 * @return the height
	 */
	public float getHeight() {
		return height;
	}
	
	/**
	 * @return the longitude
	 */
	public float getLongitude() {
		return longitude;
	}
	
	/**
	 * @return the latitude
	 */
	public float getLatitude() {
		return latitude;
	}
	
}
