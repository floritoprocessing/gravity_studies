package lightparticlesystem;


import java.util.ArrayList;

import processing.core.PMatrix3D;

/**
 * @author Marcus
 *
 */
public class LightParticleSystem {
	
	float lightSpeed = 0.5f;
	float width, height, depth;
	float G = 1000f;
	
	ArrayList<Particle> particles = new ArrayList<Particle>();
	ArrayList<Light> lights = new ArrayList<Light>();
	PhotoPlateXY photoPlate = null;
	
	public LightParticleSystem(float width, float height, float depth) {
		this.width = width;
		this.height = height;
		this.depth = depth;
	}
	
	public void setG(float g) {
		G = g;
	}
	
	/**
	 * @param lightSpeed the lightSpeed to set
	 */
	public void setLightSpeed(float lightSpeed) {
		this.lightSpeed = lightSpeed;
	}
	
	/**
	 * @return the lightSpeed
	 */
	public float getLightSpeed() {
		return lightSpeed;
	}

	public void addParticle(Particle p) {
		particles.add(p);
	}
	
	/**
	 * @param x
	 * @param y
	 * @param z
	 * @param mass
	 */
	public void addParticle(float x, float y, float z, float mass) {
		Particle p = new Particle(x, y, z, mass);
		particles.add(p);
	}

	/**
	 * @return the particles as array
	 */
	public Particle[] getParticles() {
		return particles.toArray(new Particle[0]);
	}

	public void addLight(Light l) {
		lights.add(l);
	}
	
	/**
	 * @param x
	 * @param y
	 * @param z
	 * @param type
	 * @param lightArguments
	 */
	public void addLight(float x, float y, float z, LightType type, int r, int g, int b, float... lightArguments) {
		Light l = new Light(x, y, z, type, r, g, b, lightArguments);
		lights.add(l);
	}
	
	
	/**
	 * 
	 * @param x
	 * @param y
	 * @param z
	 * @param width
	 * @param height
	 * @param longitude
	 * @param latitude
	 */
	public void setPhotoPlateXY(float x, float y, float z, float width, float height) {
		PhotoPlateXY p = new PhotoPlateXY(x, y, z, width, height);
		photoPlate = p;
	}
	
	public PhotoPlateXY getPhotoPlateXY() {
		return photoPlate;
	}
	
	/**
	 * Returns a float[][].
	 * float[0] is an array of coordinate values (x/y/z)
	 * float[1] is an array of coordinate values (x,y,z) if the photoPlate was hit.
	 * @param x
	 * @param y
	 * @param z
	 * @param xm
	 * @param ym
	 * @param zm
	 * @param returnPath TODO
	 * @param j
	 * @return
	 */
	public float[][] getPathAndHitPoint(
			float x, float y, float z, 
			float xm, float ym, float zm, 
			boolean returnPath, int maxIterations) {
		
		float lastX, lastY, lastZ;
		
		if (photoPlate==null) {
			throw new RuntimeException("Please define a photoPlate first!");
		}
		
		
		float[] pathXYZ = null;
		if (returnPath) {
			pathXYZ =  new float[maxIterations*3];
		}
		
		int pointsMade = 0;
		
		// create Vecs for testing which side of plane
		// & check initial position in reference to photo plate
		//int[] sideOfPlate = new int[photoPlates.size()];;
		//for (int i=0;i<sideOfPlate.length;i++) {
		//	PhotoPlateXY pp = photoPlates.get(i);
		//	sideOfPlate[i] = pp.sideOfPlane(z);
		//}
		int sideOfPlate = photoPlate.sideOfPlane(z);
		
		ArrayList<Float> plateCrossingParams = new ArrayList<Float>();
		for (int i=0;i<maxIterations;i++) {
			if (returnPath) {
				pathXYZ[i*3] = x;
				pathXYZ[i*3+1] = y;
				pathXYZ[i*3+2] = z;
				pointsMade++;
			}
			
			// calc acceleration
			float xa=0;
			float ya=0;
			float za=0;
			float dx, dy, dz, dSq, fac;
			for (Particle p:particles) {
				
				dx = p.x - x;
				dy = p.y - y;
				dz = p.z - z;
				dSq = dx*dx+dy*dy+dz*dz; // distance square-root
				
				/*
				 * a = G * p.mass / (r*r) = G * p.mass / dSq
				 * 
				 * normalize dx/dy/dz vector and multiply it with a
				 * factor = a/d = a / Math.sqrt(dSq)
				 * 
				 * put it all togher:
				 * xa = xa + dx * a / Math.sqrt(dSq)
				 * xa = xa + dx * G * p.mass / dSq / Math.sqrt(dSq)
				 * xa = xa + dx * (G * p.mass / (Math.sqrt(dSq)*dSq) )
				 * -> fac = G * p.mass / (Math.sqrt(dSq)*dSq) 
				 */
				
				fac = (float)(G * p.mass / (Math.sqrt(dSq) * dSq));
				xa += dx * fac;
				ya += dy * fac;
				za += dz * fac;
			}
			// newPos = pos + t*mov + 0.5*t*t*acc
			// -> mov = (newPos=pos)/t;
			// BUT t = 1, so:
			float newX = x + xm + 0.5f*xa;
			float newY = y + ym + 0.5f*ya;
			float newZ = z + zm + 0.5f*za;
			// derive new speed:
			xm = (newX-x);
			ym = (newY-y);
			zm = (newZ-z);
			// set new position
			lastX = x;
			lastY = y;
			lastZ = z;
			x = newX;
			y = newY;
			z = newZ;
			
			if (outsideSystem(x,y,z)) {
				break;
			}
			
			// now check if new position is on other side of any of the plates
			boolean plateCrossed = false;
			int side;
			//for (int pi=0;pi<photoPlates.size();pi++) {
			
				//PhotoPlateXY pp = photoPlates.get(pi);
				//Vec p = new Vec(x,y,z);
				//Vec.cross(Vec.sub(new Vec(0,0,0), new Vec(-1,0,0)), Vec.sub(new Vec(0,0,0), new Vec(0,1,0)));
				//Vec plateNormal = Vec.cross(plateSide1[pi], plateSide2[pi]); // OK!
				//int side = getSideOfPlane(p, plateOrigin[pi], plateSide1[pi], plateSide2[pi]);
				side = photoPlate.sideOfPlane(z);
				if (side!=sideOfPlate && photoPlate.contains(x,y)) {
					
					// CROSSED THE PLATE
					//plateCrossingParams.add(new Float(0));//new Float(pi)); // index of plate
					plateCrossed = true;
					
					// Line-plane intersection for exact point:
					//	The equation of a plane (points P are on the plane with normal N and point P3 on the plane) can be written as
					//		N dot (P - P3) = 0
					//	The equation of the line (points P on the line passing through points P1 and P2) can be written as
					//		P = P1 + u (P2 - P1)
					//	The intersection of these two occurs when
					//		N dot (P1 + u (P2 - P1)) = N dot P3
					//	Solving for u gives
					//			N dot (P3-P1)
					//		u = -------------
					//			N dot (P2-P1)
					
					/*Vec P1 = new Vec(x,y,z);
					Vec P2 = new Vec(lastX,lastY,lastZ);
					Vec P3 = new Vec(pp.x, pp.y, pp.z);
					Vec plateNormal = new Vec(0,0,-1);
					Vec N = plateNormal;
					float aa = Vec.dot(N, Vec.sub(P3, P1));
					float bb = Vec.dot(N, Vec.sub(P2, P1));
					float u = aa/bb;
					
					Vec P = Vec.add(P1, Vec.mul(Vec.sub(P2, P1), u));
					plateCrossingParams.add(P.x);
					plateCrossingParams.add(P.y);
					plateCrossingParams.add(P.z);*/
					
					// other way to calculate line - plate crossing point:
					// since plate is on XY-plane:
					float linePercentage = (photoPlate.z-lastZ) / (z-lastZ);
					float crossX = lastX + linePercentage * (x-lastX);
					float crossY = lastY + linePercentage * (y-lastY);
					plateCrossingParams.add(crossX);
					plateCrossingParams.add(crossY);
					plateCrossingParams.add(photoPlate.z);
					
				}
				sideOfPlate = side;
			//} // photo plate index
			if (plateCrossed) {
				break;
			}
		}
		
		//System.out.println(pointsMade);
		
		float[] out = null;
		if (returnPath) {
			out = new float[pointsMade*3];
			System.arraycopy(pathXYZ, 0, out, 0, out.length);
		}

		float[] plateCrossings = new float[plateCrossingParams.size()];
		for (int i=0;i<plateCrossings.length;i++) {
			plateCrossings[i] = plateCrossingParams.get(i);
		}
		
		return new float[][] {
				out,
				plateCrossings
		};
	}
	
	
	public boolean outsideSystem(float x, float y, float z) {
		float w2 = width/2;
		float h2 = height/2;
		float d2 = depth/2;
		if (x<-w2||x>w2) return true;
		if (y<-h2||y>h2) return true;
		if (z<-d2||z>d2) return true;
		return false;
	}

	/**
	 * @return
	 */
	public Light[] getLights() {
		return lights.toArray(new Light[0]);
	}
	
	/**
	 * @return the width
	 */
	public float getWidth() {
		return width;
	}
	
	/**
	 * @return the height
	 */
	public float getHeight() {
		return height;
	}
	
	/**
	 * @return the depth
	 */
	public float getDepth() {
		return depth;
	}

	
	
	public static void rotate(Vec v, float longitude, float latitude) {
		v.rotX((float)(-latitude*Math.PI/180.0));
		v.rotY((float)(longitude*Math.PI/180.0));
	}
	
	/**
	 * @param x
	 * @param y
	 * @param z
	 * @param width
	 * @param height
	 * @param longitude
	 * @param latitude
	 */
	public static Vec[] createCornersOfQuad(
			float x, float y, float z, 
			float width, float height, 
			float longitude, float latitude) {
		
		float w2 = width/2.0f;
		float h2 = height/2.0f;
		Vec[] corners = new Vec[] {
				new Vec(-w2,-h2,0),
				new Vec(w2,-h2,0),
				new Vec(w2,h2,0),
				new Vec(-w2,h2,0),
		};
		for (Vec c:corners) {
			//c.rotX(-latitude*Math.PI/180.0);
			//c.rotY(longitude*Math.PI/180.0);
			rotate(c, longitude, latitude);
			c.add(x,y,z);
		}
		
		return corners;
	}

	/**
	 * @param l
	 * @return
	 */
	public static Vec[] createCornersOfQuad(Light l) {
		if (l.getType()==LightType.DIRECTIONAL) {
			return createCornersOfQuad(l.x, l.y, l.z, l.width, l.height, l.longitude, l.latitude);
		} else {
			throw new RuntimeException("Can not create quad from light type "+l.getType());
		}
	}
	
	public static int getSideOfPlane(Vec p, Vec planeOrigin, Vec side1, Vec side2) {
		
		Vec normal = Vec.cross(side1, side2); // OK!
		Vec pUntranslated = Vec.sub(p, planeOrigin);
		
		double dotProduct = Vec.dot(pUntranslated,normal);
		
		if (dotProduct==0) {
			return 0;
		} else if (dotProduct<0) {
			return -1; 
		} else {
			return 1;
		}
		
		
		//(point to test) dot (plane normal) = 0 => Point is on the plane
		//(point to test) dot (plane normal) > 0 => Point is on the same side as the normal vector
		//(point to test) dot (plane normal) < 0 => Point is on the opposite side of the normal vector
	}

	
}

