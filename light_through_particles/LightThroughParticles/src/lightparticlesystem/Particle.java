package lightparticlesystem;

/**
 * @author Marcus
 *
 */
public class Particle {
	
	float x, y, z;
	float mass;
	
	public Particle(float x, float y, float z, float mass) {
		this.x=x;
		this.y=y;
		this.z=z;
		this.mass=mass;
	}
	
	public String toString() {
		String s="";
		s = "new Particle(";
		s += x+"f, ";
		s += y+"f, ";
		s += z+"f, ";
		s += mass;
		s += ")";
		return s;
	}
	
	public void move(float xm, float ym, float zm) {
		x += xm;
		y += ym;
		z += zm;
	}
	
	/**
	 * @return the mass
	 */
	public float getMass() {
		return mass;
	}
	
	/**
	 * @return the x
	 */
	public float getX() {
		return x;
	}
	
	/**
	 * @return the y
	 */
	public float getY() {
		return y;
	}
	
	/**
	 * @return the z
	 */
	public float getZ() {
		return z;
	}
}
