/**
 * 
 */
package lightparticlesystem;


/**
 * @author Marcus
 *
 */
public class PhotoPlateXY {
	
	float x, y, z;
	float width, height;
	
	float[] cornersXYZ;
	float x0, x1, y0, y1;
	
	
	public PhotoPlateXY(float x, float y, float z, float width, float height) {
		this.x=x;
		this.y=y;
		this.z = z;
		this.width = Math.abs(width);
		this.height = Math.abs(height);
		x0 = x-this.width/2;
		x1 = x+this.width/2;
		y0 = y-this.height/2;
		y1 = y+this.height/2;
		
		cornersXYZ = new float[] {
				x0,	y0,	z,
				x1,	y0,	z,
				x1,	y1,	z,
				x0,	y1,	z
		};
	}

	public int sideOfPlane(float posZ) {
		if (posZ<z) return -1;
		else if (posZ>z) return 1;
		else return 0;
	}
	
	public float getXPercentageOnPlate(float globalX) {
		return (globalX-x0)/(x1-x0);
	}
	public float getYPercentageOnPlate(float globalY) {
		return (globalY-y0)/(y1-y0);
	}
	
	/**
	 * @return the cornersXYZ
	 */
	public float[] getCornersXYZ() {
		float[] out = new float[12];
		System.arraycopy(cornersXYZ, 0, out, 0, 12);
		return out;
	}
		
	/**
	 * @return the x
	 */
	public float getX() {
		return x;
	}
	
	/**
	 * @return the y
	 */
	public float getY() {
		return y;
	}
	
	/**
	 * @return the z
	 */
	public float getZ() {
		return z;
	}
	
	/**
	 * @return the width
	 */
	public float getWidth() {
		return width;
	}
	
	/**
	 * @return the height
	 */
	public float getHeight() {
		return height;
	}

	/**
	 * @param x2
	 * @param y2
	 * @return
	 */
	public boolean contains(float px, float py) {
		if (px>=x0&&px<=x1&&py>=y0&&py<=y1) return true;
		return false;
	}
	
	
	
}
