package lightparticlesystem;

public class Vec {

	public static Vec add(Vec u, Vec v) {
		return new Vec(u.x+v.x,u.y+v.y,u.z+v.z);
	}

	public static Vec add(Vec u, Vec v, Vec w) {
		return new Vec(u.x+v.x+w.x,u.y+v.y+w.y,u.z+v.z+w.z);
	}

	public static Vec div(Vec v, float n) {
		return new Vec(v.x/n,v.y/n,v.z/n);
	}

	public static Vec mul(Vec v, float n) {
		return new Vec(v.x*n,v.y*n,v.z*n);
	}

	public static Vec sub(Vec v, Vec w) {
		return new Vec(v.x-w.x,v.y-w.y,v.z-w.z);
	}

	public float x=0, y=0, z=0;

	public Vec() {
	}

	public Vec(float x, float y) {
		this.x = x;
		this.y = y;
	}

	public Vec(float x, float y, float z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public Vec(Vec v) {
		x=v.x;
		y=v.y;
		z=v.z;
	}

	public void add(float dx, float dy) {
		x+=dx;
		y+=dy;
	}
	
	public void add(float dx, float dy, float dz) {
		x+=dx;
		y+=dy;
		z+=dz;
	}
	public void add(float[] xyz) {
		int size = xyz.length;
		if (size==2) {
			x+=xyz[0];
			y+=xyz[1];
		} else if (size==3) {
			x+=xyz[0];
			y+=xyz[1];
			z+=xyz[2];
		}
	}
	public void add(Vec v) {
		x+=v.x;
		y+=v.y;
		z+=v.z;
	}

	public void div(float n) {
		x/=n;
		y/=n;
		z/=n;
	}

	public boolean isNullVec() {
		return (x==0&&y==0&&z==0);
	}

	public float length() {
		return (float)Math.sqrt(x*x+y*y+z*z);
	}

	public void mul(float n) {
		x*=n;
		y*=n;
		z*=n;
	}

	public void normalize() {
		if (!isNullVec()) {
			div(length());
		}
	}

	public void rotX(float rd) {
		float SIN=(float)Math.sin(rd); 
		float COS=(float)Math.cos(rd);
		float yn=y*COS-z*SIN;
		float zn=z*COS+y*SIN;
		y=yn;
		z=zn;
	}

	public void rotY(float rd) {
		float SIN=(float)Math.sin(rd);
		float COS=(float)Math.cos(rd); 
		float xn=x*COS-z*SIN; 
		float zn=z*COS+x*SIN;
		x=xn;
		z=zn;
	}

	public void rotZ(float rd) {
		float SIN=(float)Math.sin(rd);
		float COS=(float)Math.cos(rd); 
		float xn=x*COS-y*SIN; 
		float yn=y*COS+x*SIN;
		x=xn;
		y=yn;
	}

	public void set(float x, float y) {
		this.x=x;
		this.y=y;
	}

	public void set(float x, float y, float z) {
		this.x=x;
		this.y=y;
		this.z=z;
	}

	public void set(Vec v) {
		x=v.x;
		y=v.y;
		z=v.z;
	}
	
	public void setLength(float l) {
		if (!isNullVec()) {
			mul(l/length());
		}
	}

	public void sub(Vec v) {
		x-=v.x;
		y-=v.y;
		z-=v.z;
	}
	
	public String toString() {
		return x+", "+y+", "+z;
	}

	/**
	 * Returns the cross product of two vectors
	 * @param v1
	 * @param v2
	 * @return
	 */
	public static Vec cross(Vec v1, Vec v2) {
		
		return new Vec(
				v1.y*v2.z - v2.y*v1.z,
				v1.z*v2.x - v2.z*v1.x,
				v1.x*v2.y - v2.x*v1.y
		);
		
	}

	/**
	 * @param p
	 * @param normal
	 * @return
	 */
	public static float dot(Vec a, Vec b) {
		return a.x*b.x + a.y*b.y + a.z*b.z;
	}


}
