import graf.math.vector.Vec;
import lightparticlesystem.Light;
import lightparticlesystem.LightParticleSystem;
import lightparticlesystem.LightType;
import lightparticlesystem.Particle;
import lightparticlesystem.PhotoPlate;
import processing.core.PApplet;

/**
 * 
 */

/**
 * @author Marcus
 *
 */
public class LightThroughParticles extends PApplet {
	
	LightParticleSystem lps = new LightParticleSystem(600,500,500);
		
	float camLon,camLat;
	Vec camPos = new Vec(0,0,500);
	
	public void settings() {
		size(800,600,P3D);
	}
	
	public void setup() {
		
		
		lps.setG(10);
		lps.addParticle(0,0,0,1);
		lps.addParticle(200,-100,0,1);
		lps.addParticle(-150, -150, 0, 1);
		
		lps.addLight(0,-100,200,LightType.DIRECTIONAL,400,220,-5,-20);
		
		lps.addPhotoPlate(0,0,-200,400,180,5,-5);
	}
	
	public void mouseDragged() {
		camLon += (mouseX-pmouseX)*1;
		camLat -= (mouseY-pmouseY)*1;
	}
	
	public void draw() {
		background(0);
		
		camPos.set(0,0,500);
		LightParticleSystem.rotate(camPos, camLon, camLat);
		
		camera((float)camPos.x, (float)camPos.y, (float)camPos.z, 0, 0, 0, 0, 1, 0);
		
		// draw cube
		stroke(255,0,0,64);
		noFill();
		box(lps.getWidth(), lps.getHeight(), lps.getDepth());
	
		
		// draw particles
		Particle[] particles = lps.getParticles();
		noStroke();
		fill(255,0,0,128);
		for (Particle p:particles) {
			pushMatrix();
			translate(p.getX(), p.getY(), p.getZ());
			sphere(10);
			popMatrix();
		}
		
		// draw lights
		Light[] lights = lps.getLights();
		noFill();
		for (Light l:lights) {
			switch (l.getType()) {
			case DIRECTIONAL:
				
				// DIRECTIONAL light: draw square
				stroke(255,255,0,64);
				Vec[] corners = LightParticleSystem.createCornersOfQuad(l);
				beginShape(QUADS);
				for (Vec c:corners) {
					vertex((float)c.x,(float)c.y,(float)c.z);
				}
				endShape();
				
				
				// DIRECTIONAL light: draw light lines
				stroke(255,255,0,64);
				float spacing = 25;
				float movStep = -0.5f;
				float w2 = l.getWidth()/2.0f;
				float h2 = l.getHeight()/2.0f;
				
				Vec movement = new Vec(0,0,movStep);
				LightParticleSystem.rotate(movement, l.getLongitude(), l.getLatitude());
				float xm = (float)movement.x;
				float ym = (float)movement.y;
				float zm = (float)movement.z;
				
				//beginShape(POINTS);
				for (float y=-h2;y<=h2;y+=spacing) {
					for (float x=-w2;x<=w2;x+=spacing) {
						float z = 0;
						Vec start = new Vec(x,y,z);
						LightParticleSystem.rotate(start, l.getLongitude(), l.getLatitude());
						start.add(l.getX(),l.getY(),l.getZ());
						float xx = (float)start.x;
						float yy = (float)start.y;
						float zz = (float)start.z;
						
						// light path:
						float[][] pathAndHitpoint = lps.getPathAndHitPoint(xx,yy,zz,xm,ym,zm,2000);
						float[] xyz = pathAndHitpoint[0];
						float[] hitPoints = pathAndHitpoint[1];
						xyz = reduceXYZ(xyz,0.1f);
						
						strokeWeight(1);
						stroke(255,255,0,64);
						beginShape(POLYGON);
						for (int i=0;i<xyz.length;i+=3) {
							vertex(xyz[i],xyz[i+1],xyz[i+2]);
						}
						endShape(OPEN);
						
						stroke(255,255,255,64);
						strokeWeight(1);
						beginShape(POINTS);
						for (int i=0;i<hitPoints.length;i+=4) {
							vertex(hitPoints[i+1],hitPoints[i+2],hitPoints[i+3]);
						}
						endShape();
						strokeWeight(1);
						
					}
				}
				
				break;

			default:
				throw new RuntimeException("Unimplemented type "+l.getType());
			}
		} // end Lights
		
		
		
		
		// draw Photo plates
		PhotoPlate[] plates = lps.getPhotoPlates();
		stroke(255,255,255,64);
		for (PhotoPlate pp:plates) {
			
			Vec[] corners = LightParticleSystem.createCornersOfQuad(pp);
			beginShape(QUADS);
			for (Vec c:corners) {
				vertex((float)c.x,(float)c.y,(float)c.z);
			}
			endShape();
			
			Vec side1 = Vec.sub(corners[1], corners[0]);
			Vec side2 = Vec.sub(corners[1], corners[2]);
			
			// draw normal of plane
			Vec normal = Vec.cross(side1, side2);
			normal.normalize();
			normal.mul(100);
			Vec n0 = new Vec(pp.getX(), pp.getY(), pp.getZ());
			Vec n1 = Vec.add(n0,normal);
			beginShape(LINES);
			vertex((float)n0.x,(float)n0.y,(float)n0.z);
			vertex((float)n1.x,(float)n1.y,(float)n1.z);
			endShape();
			
			// check cam in relation to photo plate
			Vec planeOrigin = new Vec(pp.getX(), pp.getY(), pp.getZ());
			int side = LightParticleSystem.getSideOfPlane(camPos, planeOrigin, side1, side2);
			if (side<0) {
				println("Camera behind plate");
			} else if (side>0) {
				println("Camera in front of plate");
			} else {
				println("Camera on plate");
			}
		}
		//endShape();
		
		
		
		
		
		
		
	}
	
	public void keyPressed() {
		// move particles
		int i=0;
		Particle[] particles = lps.getParticles();
		for (Particle p:particles) {
			
			if (i%3==0) {
				p.move(0.5f,0.2f,0.1f);
			} else if (i%3==1) {
				p.move(-0.2f,-0.4f,-0.1f);
			} else if (i%3==2) {
				p.move(-0.1f,0.1f,0.5f);
			}
			
			i++;
		}
	}
	
	/**
	 * @param xyz
	 * @param i
	 * @return
	 */
	public static float[] reduceXYZ(float[] xyz, float factor) {
		int pointCount = xyz.length/3;
		int newPointCount = (int)(pointCount*factor);
		float[] out = new float[newPointCount*3];
		for (int destPointIndex=0;destPointIndex<newPointCount;destPointIndex++) {
			int sourcePointIndex = (int)(destPointIndex/factor);
			out[destPointIndex*3] = xyz[sourcePointIndex*3];
			out[destPointIndex*3+1] = xyz[sourcePointIndex*3+1];
			out[destPointIndex*3+2] = xyz[sourcePointIndex*3+2];
		}
		//System.arraycopy(xyz, 0, out, 0, out.length);
		return out;
	}

	public static void main(String[] args) {
		PApplet.main(new String[] {"LightThroughParticles"});
	}
}
