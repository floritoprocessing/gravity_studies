package graf.math.vector;

public class Vec {

	public static Vec add(Vec u, Vec v) {
		return new Vec(u.x+v.x,u.y+v.y,u.z+v.z);
	}

	public static Vec add(Vec u, Vec v, Vec w) {
		return new Vec(u.x+v.x+w.x,u.y+v.y+w.y,u.z+v.z+w.z);
	}

	public static Vec div(Vec v, double n) {
		return new Vec(v.x/n,v.y/n,v.z/n);
	}

	public static Vec mul(Vec v, double n) {
		return new Vec(v.x*n,v.y*n,v.z*n);
	}

	public static Vec sub(Vec v, Vec w) {
		return new Vec(v.x-w.x,v.y-w.y,v.z-w.z);
	}

	public double x=0, y=0, z=0;

	public Vec() {
	}

	public Vec(double x, double y) {
		this.x = x;
		this.y = y;
	}

	public Vec(double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public Vec(Vec v) {
		x=v.x;
		y=v.y;
		z=v.z;
	}

	public void add(double dx, double dy) {
		x+=dx;
		y+=dy;
	}
	
	public void add(double dx, double dy, double dz) {
		x+=dx;
		y+=dy;
		z+=dz;
	}
	public void add(double[] xyz) {
		int size = xyz.length;
		if (size==2) {
			x+=xyz[0];
			y+=xyz[1];
		} else if (size==3) {
			x+=xyz[0];
			y+=xyz[1];
			z+=xyz[2];
		}
	}
	public void add(Vec v) {
		x+=v.x;
		y+=v.y;
		z+=v.z;
	}

	public void div(double n) {
		x/=n;
		y/=n;
		z/=n;
	}

	public boolean isNullVec() {
		return (x==0&&y==0&&z==0);
	}

	public double length() {
		return Math.sqrt(x*x+y*y+z*z);
	}

	public void mul(double n) {
		x*=n;
		y*=n;
		z*=n;
	}

	public void normalize() {
		if (!isNullVec()) {
			div(length());
		}
	}

	public void rotX(double rd) {
		double SIN=Math.sin(rd); 
		double COS=Math.cos(rd);
		double yn=y*COS-z*SIN;
		double zn=z*COS+y*SIN;
		y=yn;
		z=zn;
	}

	public void rotY(double rd) {
		double SIN=Math.sin(rd);
		double COS=Math.cos(rd); 
		double xn=x*COS-z*SIN; 
		double zn=z*COS+x*SIN;
		x=xn;
		z=zn;
	}

	public void rotZ(double rd) {
		double SIN=Math.sin(rd);
		double COS=Math.cos(rd); 
		double xn=x*COS-y*SIN; 
		double yn=y*COS+x*SIN;
		x=xn;
		y=yn;
	}

	public void set(double x, double y) {
		this.x=x;
		this.y=y;
	}

	public void set(double x, double y, double z) {
		this.x=x;
		this.y=y;
		this.z=z;
	}

	public void set(Vec v) {
		x=v.x;
		y=v.y;
		z=v.z;
	}
	
	public void setLength(double l) {
		if (!isNullVec()) {
			mul(l/length());
		}
	}

	public void sub(Vec v) {
		x-=v.x;
		y-=v.y;
		z-=v.z;
	}

	/**
	 * Returns the cross product of two vectors
	 * @param v1
	 * @param v2
	 * @return
	 */
	public static Vec cross(Vec v1, Vec v2) {
		
		return new Vec(
				v1.y*v2.z - v2.y*v1.z,
				v1.z*v2.x - v2.z*v1.x,
				v1.x*v2.y - v2.x*v1.y
		);
		
	}

	/**
	 * @param p
	 * @param normal
	 * @return
	 */
	public static double dot(Vec a, Vec b) {
		return a.x*b.x + a.y*b.y + a.z*b.z;
	}


}
