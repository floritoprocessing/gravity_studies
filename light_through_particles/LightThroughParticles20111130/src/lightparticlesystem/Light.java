/**
 * 
 */
package lightparticlesystem;

/**
 * @author Marcus
 *
 */
public class Light {

	float width, height, longitude, latitude;
	
	float x, y, z;
	LightType type;
		
	public Light(float x, float y, float z, LightType type, float... lightArguments) {
		this.x=x;
		this.y=y;
		this.z=z;
		this.type = type;
		
		switch (type) {
		case DIRECTIONAL:
			if (lightArguments!=null && lightArguments.length==4) {
				width = Math.abs(lightArguments[0]);
				height = Math.abs(lightArguments[1]);
				longitude = lightArguments[2];
				latitude = lightArguments[3];
			} else {
				throw new RuntimeException("Directional light wants 4 floats!");
			}
			
			break;

		default:
			throw new RuntimeException("Unimplemented type "+type);
		}
	}
	
	
	
	/**
	 * @return the type
	 */
	public LightType getType() {
		return type;
	}
	
	/**
	 * @return the x
	 */
	public float getX() {
		return x;
	}
	
	/**
	 * @return the y
	 */
	public float getY() {
		return y;
	}
	
	/**
	 * @return the z
	 */
	public float getZ() {
		return z;
	}
	
	/**
	 * @return the width
	 */
	public float getWidth() {
		return width;
	}
	
	/**
	 * @return the height
	 */
	public float getHeight() {
		return height;
	}
	
	/**
	 * @return the longitude
	 */
	public float getLongitude() {
		return longitude;
	}
	
	/**
	 * @return the latitude
	 */
	public float getLatitude() {
		return latitude;
	}
	
}
