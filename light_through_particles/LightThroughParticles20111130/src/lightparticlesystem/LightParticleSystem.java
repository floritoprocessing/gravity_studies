package lightparticlesystem;

import graf.math.vector.Vec;

import java.util.ArrayList;

import processing.core.PMatrix3D;

/**
 * @author Marcus
 *
 */
public class LightParticleSystem {
	
	float width, height, depth;
	float G = 1000f;
	
	ArrayList<Particle> particles = new ArrayList<Particle>();
	ArrayList<Light> lights = new ArrayList<Light>();
	ArrayList<PhotoPlate> photoPlates = new ArrayList<PhotoPlate>();
	
	public LightParticleSystem(float width, float height, float depth) {
		this.width = width;
		this.height = height;
		this.depth = depth;
	}
	
	public void setG(float g) {
		G = g;
	}

	/**
	 * @param x
	 * @param y
	 * @param z
	 * @param mass
	 */
	public Particle addParticle(float x, float y, float z, float mass) {
		Particle p = new Particle(x, y, z, mass);
		particles.add(p);
		return p;
	}

	/**
	 * @return the particles as array
	 */
	public Particle[] getParticles() {
		return particles.toArray(new Particle[0]);
	}

	/**
	 * @param x
	 * @param y
	 * @param z
	 * @param type
	 * @param lightArguments
	 */
	public Light addLight(float x, float y, float z, LightType type, float... lightArguments) {
		Light l = new Light(x, y, z, type, lightArguments);
		lights.add(l);
		return l;
	}
	
	
	/**
	 * 
	 * @param x
	 * @param y
	 * @param z
	 * @param width
	 * @param height
	 * @param longitude
	 * @param latitude
	 */
	public PhotoPlate addPhotoPlate(float x, float y, float z, float width, float height, float longitude, float latitude) {
		PhotoPlate p = new PhotoPlate(x, y, z, width, height, longitude, latitude);
		photoPlates.add(p);
		return p;
	}
	
	public PhotoPlate[] getPhotoPlates() {
		return photoPlates.toArray(new PhotoPlate[0]);
	}
	
	/**
	 * @param x
	 * @param y
	 * @param z
	 * @param xm
	 * @param ym
	 * @param zm
	 * @param j
	 * @return
	 */
	public float[][] getPathAndHitPoint(
			float x, float y, float z, 
			float xm, float ym, float zm, 
			int maxIterations) {
		
		float lastX, lastY, lastZ;
		
		float[] pathXYZ = new float[maxIterations*3];
		float t = 1;
		float t2 = t*t;
		int pointsMade = 0;
		
		// create Vecs for testing which side of plane
		// & check initial position in reference to photo plate
		int[] sideOfPlate = new int[photoPlates.size()];
		Vec[] plateOrigin = new Vec[photoPlates.size()];
		Vec[] plateSide1 = new Vec[photoPlates.size()];
		Vec[] plateSide2 = new Vec[photoPlates.size()];
		for (int i=0;i<sideOfPlate.length;i++) {
			Vec point = new Vec(x,y,z); // first point
			PhotoPlate pp = photoPlates.get(i);
			plateOrigin[i] = new Vec(pp.x,pp.y,pp.z);
			Vec[] plateCorners = createCornersOfQuad(pp);
			plateSide1[i] = Vec.sub(plateCorners[1], plateCorners[0]);
			plateSide2[i] = Vec.sub(plateCorners[1], plateCorners[2]);
			sideOfPlate[i] = getSideOfPlane(point, plateOrigin[i], plateSide1[i], plateSide2[i]);
		}
		
		ArrayList<Float> plateCrossingParams = new ArrayList<Float>();
		for (int i=0;i<maxIterations;i++) {
			pathXYZ[i*3] = x;
			pathXYZ[i*3+1] = y;
			pathXYZ[i*3+2] = z;
			pointsMade++;
			
			// calc acceleration
			float xa=0;
			float ya=0;
			float za=0;
			for (Particle p:particles) {
				//a = G * M / (r*r);
				float dx = p.getX() - x;
				float dy = p.getY() - y;
				float dz = p.getZ() - z;
				Vec gravity = new Vec(dx,dy,dz);
				gravity.normalize();
				float dSq = dx*dx+dy*dy+dz*dz;
				float a = G * p.getMass() / dSq;
				gravity.mul(a);
				xa += gravity.x;
				ya += gravity.y;
				za += gravity.z;
			}
			float newX = x + t*xm + 0.5f*t2*xa;
			float newY = y + t*ym + 0.5f*t2*ya;
			float newZ = z + t*zm + 0.5f*t2*za;
			// derive new speed:
			xm = (newX-x)/t;
			ym = (newY-y)/t;
			zm = (newZ-z)/t;
			// set new position
			lastX = x;
			lastY = y;
			lastZ = z;
			x = newX;
			y = newY;
			z = newZ;
			
			if (outsideSystem(x,y,z)) {
				break;
			}
			
			// now check if new position is on other side of any of the plates
			boolean plateCrossed = false;
			for (int pi=0;pi<photoPlates.size();pi++) {
				PhotoPlate pp = photoPlates.get(pi);
				Vec p = new Vec(x,y,z);
				Vec plateNormal = Vec.cross(plateSide1[pi], plateSide2[pi]); // OK!
				int side = getSideOfPlane(p, plateOrigin[pi], plateSide1[pi], plateSide2[pi]);
				if (side!=sideOfPlate[pi]) {
					
					// CROSSED THE PLATE!
					plateCrossingParams.add(new Float(pi)); // index of plate
					// this is the point AFTER crossing the plate
					/*plateCrossingParams.add(x);
					plateCrossingParams.add(y);
					plateCrossingParams.add(z);*/
					plateCrossed = true;
					
					// TODO: make a line-plane intersection for exact point
					//	The equation of a plane (points P are on the plane with normal N and point P3 on the plane) can be written as
					//		N dot (P - P3) = 0
					//	The equation of the line (points P on the line passing through points P1 and P2) can be written as
					//		P = P1 + u (P2 - P1)
					//	The intersection of these two occurs when
					//		N dot (P1 + u (P2 - P1)) = N dot P3
					//	Solving for u gives
					//			N dot (P3-P1)
					//		u = -------------
					//			N dot (P2-P1)
					
					Vec P1 = new Vec(x,y,z);
					Vec P2 = new Vec(lastX,lastY,lastZ);
					Vec P3 = new Vec(pp.x, pp.y, pp.z);
					Vec N = plateNormal;
					
					double a = Vec.dot(N, Vec.sub(P3, P1));
					double b = Vec.dot(N, Vec.sub(P2, P1));
					double u = (float)(a/b);
					
					Vec P = Vec.add(P1, Vec.mul(Vec.sub(P2, P1), u));
					plateCrossingParams.add((float)P.x);
					plateCrossingParams.add((float)P.y);
					plateCrossingParams.add((float)P.z);
					
				}
				sideOfPlate[pi] = side;
			}
			if (plateCrossed) {
				break;
			}
		}
		
		
		float[] out = new float[pointsMade*3];
		System.arraycopy(pathXYZ, 0, out, 0, out.length);

		float[] plateCrossings = new float[plateCrossingParams.size()];
		for (int i=0;i<plateCrossings.length;i++) {
			plateCrossings[i] = plateCrossingParams.get(i);
		}
		
		return new float[][] {
				out,
				plateCrossings
		};
	}
	
	
	public boolean outsideSystem(float x, float y, float z) {
		float w2 = width/2;
		float h2 = height/2;
		float d2 = depth/2;
		if (x<-w2||x>w2) return true;
		if (y<-h2||y>h2) return true;
		if (z<-d2||z>d2) return true;
		return false;
	}

	/**
	 * @return
	 */
	public Light[] getLights() {
		return lights.toArray(new Light[0]);
	}
	
	/**
	 * @return the width
	 */
	public float getWidth() {
		return width;
	}
	
	/**
	 * @return the height
	 */
	public float getHeight() {
		return height;
	}
	
	/**
	 * @return the depth
	 */
	public float getDepth() {
		return depth;
	}

	
	
	public static void rotate(Vec v, float longitude, float latitude) {
		v.rotX(-latitude*Math.PI/180.0);
		v.rotY(longitude*Math.PI/180.0);
	}
	
	/**
	 * @param x
	 * @param y
	 * @param z
	 * @param width
	 * @param height
	 * @param longitude
	 * @param latitude
	 */
	public static Vec[] createCornersOfQuad(
			float x, float y, float z, 
			float width, float height, 
			float longitude, float latitude) {
		
		float w2 = width/2.0f;
		float h2 = height/2.0f;
		Vec[] corners = new Vec[] {
				new Vec(-w2,-h2,0),
				new Vec(w2,-h2,0),
				new Vec(w2,h2,0),
				new Vec(-w2,h2,0),
		};
		for (Vec c:corners) {
			//c.rotX(-latitude*Math.PI/180.0);
			//c.rotY(longitude*Math.PI/180.0);
			rotate(c, longitude, latitude);
			c.add(x,y,z);
		}
		
		return corners;
	}

	/**
	 * @param pp
	 * @return
	 */
	public static Vec[] createCornersOfQuad(PhotoPlate pp) {
		return createCornersOfQuad(pp.x, pp.y, pp.z, pp.width, pp.height, pp.longitude, pp.latitude);
	}

	/**
	 * @param l
	 * @return
	 */
	public static Vec[] createCornersOfQuad(Light l) {
		if (l.getType()==LightType.DIRECTIONAL) {
			return createCornersOfQuad(l.x, l.y, l.z, l.width, l.height, l.longitude, l.latitude);
		} else {
			throw new RuntimeException("Can not create quad from light type "+l.getType());
		}
	}
	
	public static int getSideOfPlane(Vec p, Vec planeOrigin, Vec side1, Vec side2) {
		
		Vec normal = Vec.cross(side1, side2); // OK!
		Vec pUntranslated = Vec.sub(p, planeOrigin);
		
		double dotProduct = Vec.dot(pUntranslated,normal);
		
		if (dotProduct==0) {
			return 0;
		} else if (dotProduct<0) {
			return -1; 
		} else {
			return 1;
		}
		
		
		//(point to test) dot (plane normal) = 0 => Point is on the plane
		//(point to test) dot (plane normal) > 0 => Point is on the same side as the normal vector
		//(point to test) dot (plane normal) < 0 => Point is on the opposite side of the normal vector
	}

	
}
