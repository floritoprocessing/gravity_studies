/**
 * 
 */
package lightparticlesystem;

/**
 * @author Marcus
 *
 */
public enum LightType {
	
	/**
	 * Wants four parameters: width, height, longituded(degrees), latitude(degrees)
	 */
	DIRECTIONAL,
	SPOT;

}
