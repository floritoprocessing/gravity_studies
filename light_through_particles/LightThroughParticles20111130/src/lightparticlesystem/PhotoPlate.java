/**
 * 
 */
package lightparticlesystem;

import graf.math.vector.Vec;

/**
 * @author Marcus
 *
 */
public class PhotoPlate {
	
	float x, y, z;
	float width, height, longitude, latitude;
	
	
	
	public PhotoPlate(
			float x, float y, float z, 
			float width, float height,
			float longitude, float latitude) {
		
		this.x = x;
		this.y = y;
		this.z = z;
		this.width = Math.abs(width);
		this.height = Math.abs(height);
		this.longitude = longitude;
		this.latitude = latitude;
		
	}

	
	
	/**
	 * @return the x
	 */
	public float getX() {
		return x;
	}
	
	/**
	 * @return the y
	 */
	public float getY() {
		return y;
	}
	
	/**
	 * @return the z
	 */
	public float getZ() {
		return z;
	}
	
	/**
	 * @return the width
	 */
	public float getWidth() {
		return width;
	}
	
	/**
	 * @return the height
	 */
	public float getHeight() {
		return height;
	}
	
	/**
	 * @return the longitude
	 */
	public float getLongitude() {
		return longitude;
	}
	
	/**
	 * @return the latitude
	 */
	public float getLatitude() {
		return latitude;
	}
	
	
}
