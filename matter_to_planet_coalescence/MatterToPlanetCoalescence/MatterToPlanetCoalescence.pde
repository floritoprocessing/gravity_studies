import java.util.Vector;

int NR_OF_PARTICLES=5000;

Particle particle;
Vector particles;

float coa=0;

void setup() {
  size(768,576,P3D);
  particles=new Vector();
  for (int i=0;i<NR_OF_PARTICLES;i++) {
    particle=new Particle(400,100);
    particles.add(particle);
  }
}

void draw() {
  if (coa<1.0) {
    background(0);
    camera(0,-250,700, 0,0,0, 0,1,0);
    for (int i=0;i<particles.size();i++) {
      particle=(Particle)(particles.elementAt(i));
      particle.update(coa);
    }
    //coa+=0.002;
    coa+=0.01;
    //saveFrame("./frames2/coalescence1-#####.tif");
  }
}
