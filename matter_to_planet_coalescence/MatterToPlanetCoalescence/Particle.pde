class Particle {
  color COL;
  float COL_BR;
  
  float torusSize;
  float torusTubeRadius;
  float xOffsetCenTorusTube;
  float yOffsetCenTorusTube;
  float PosRadTorusTube;
  float yAxisOffsetAngle;
  float yAxisOffsetAngleMax;
  
  float rndTubePos;
  float posOnTubeCut;
  float radOnTubeCut;
  
  float posRadTorusTubeOffset;
  float orbitalVelocity;
  
  float x,y,z;
  
  float COALESCENCE=0;  // 0..1
  
  Particle(float _torusSize, float _torusTubeRadius) {
    COL_BR=random(116,164);
    
    torusSize=_torusSize;
    torusTubeRadius=_torusTubeRadius;
    
    rndTubePos=random(1);
    posOnTubeCut=torusTubeRadius*pow(rndTubePos,0.1+COALESCENCE*4);
    radOnTubeCut=random(TWO_PI);
    xOffsetCenTorusTube=posOnTubeCut*cos(radOnTubeCut);
    yOffsetCenTorusTube=posOnTubeCut*sin(radOnTubeCut);
        
    float r=sqrt(sq(torusSize+xOffsetCenTorusTube)+sq(yOffsetCenTorusTube));
    orbitalVelocity=sqrt(1/r);
    
    yAxisOffsetAngleMax=random(-0.2,0.2);
    PosRadTorusTube=random(TWO_PI);
    posRadTorusTubeOffset=random(TWO_PI);
  }
  
  void update(float _COALESCENCE) {
    COALESCENCE=_COALESCENCE;
    posOnTubeCut=torusTubeRadius*pow(rndTubePos,0.1+COALESCENCE*4);
    xOffsetCenTorusTube=posOnTubeCut*cos(radOnTubeCut);
    yOffsetCenTorusTube=posOnTubeCut*sin(radOnTubeCut);  
    yAxisOffsetAngle=yAxisOffsetAngleMax*(1-COALESCENCE);
    
    PosRadTorusTube+=(0.5*orbitalVelocity);
    pushMatrix();
    if (posRadTorusTubeOffset>PI) {
      rotateY(posRadTorusTubeOffset);
    } else {
      rotateY(posRadTorusTubeOffset);
    }
    
    rotateZ(yAxisOffsetAngle);
    pushMatrix();
    rotateY(0);//PosRadTorusTube);
    //r
    translate(torusSize,0,0);
    translate(xOffsetCenTorusTube,yOffsetCenTorusTube);

    float rd=posRadTorusTubeOffset+PosRadTorusTube;
    float rd1=abs(PI-rd)/TWO_PI;
    COL=color(255,255,255,COL_BR);
    stroke(COL);
    point(0,0,0);
    popMatrix();
    popMatrix();
  }
}
