
import java.util.Vector;

int NR_OF_PARTICLES=15000;
float MASS_SUN=20000;
float MASS_POINT=100;

Particle particle;
Vector particles;

Particle attractPoint;

float coa=0;

void setup() {
  size(768,576,P3D);
  particles=new Vector();
  for (int i=0;i<NR_OF_PARTICLES;i++) {
    particle=new Particle(400,100,true);
    particle.setOrbitalVelocity(MASS_SUN);
    particles.add(particle);
  }
  
  attractPoint=new Particle(400,100,false);
  attractPoint.setOrbitalVelocity(MASS_SUN);
}

void draw() {
  for (int f=0;f<10;f++) {
  if (coa<1.0) {
    if (f==0) background(0);
    if (f==0) camera(0,-500,800, 0,0,0, 0,1,0);
   // rotateY(rd);
    for (int i=0;i<particles.size();i++) {
      particle=(Particle)(particles.elementAt(i));
      //particle.update(coa);
      particle.gravitateTo(new Vec(0,0,0),MASS_SUN);
      particle.gravitateTo(attractPoint.pos,MASS_POINT);
      particle.move();
      if (f==0) particle.show();
      if (particle.remove) { 
        particles.remove(particle);
        MASS_POINT+=0.1;
        println(particles.size());
      }
    }
    
    attractPoint.gravitateTo(new Vec(0,0,0),MASS_SUN);
    attractPoint.move();
    if (f==0) attractPoint.showAsBall(MASS_POINT);
    
    //coa+=0.002;
    //coa+=0.01;
    //saveFrame("./frames2/coalescence1-#####.tif");
  }
  }
}
