import Graf.*;
color COL=color(255,40,30);

GrafImage3D gi;

int NR_OF_PARTICLES=5000;
float MASS_SUN=30000;
float MASS_POINT=10;

Particle particle;
Vector particles;

Particle attractPoint;

float coa=0;

void setup() {
  size(400,300,P3D);
  gi = new GrafImage3D(width,height,GrafUtil.BLEND_MODE_ADD);
  gi.background(0);
  //gi.DistanceToViewer=1000;
  particles=new Vector();
//  for (int i=0;i<NR_OF_PARTICLES;i++) {
//    particle=new Particle(400,80,true);
//    particle.setOrbitalVelocity(MASS_SUN);
//    particles.add(particle);
//  }

  attractPoint=new Particle(400,80,false);
  attractPoint.setOrbitalVelocity(MASS_SUN);
}

boolean massUp=true;

void draw() {
  gi.background(0,0.15);
  //for (int g=0;g<5;g++) {
  //gi.background(0,0.05);
  for (int f=3;f>=0;f--) {
    if (massUp) {
      MASS_POINT*=1.0005;
      if (MASS_POINT>1000) massUp=false;
    }
    else {
      MASS_POINT/=1.0005;
      if (MASS_POINT<10) massUp=true;
    }
    MASS_POINT = 10 + mouseX*2;
    if (coa<1.0) {
      double degTurn = Math.atan2(attractPoint.pos.z,attractPoint.pos.x);
      if (false) {
        for (int i=0;i<100;i++) {
          Vec pos = new Vec(attractPoint.pos);
          pos.add(new Vec(random(-5.0,5.0),random(-5.0,5.0),random(-5.0,5.0)));
          pos.rotY(-degTurn-PI/2.0);
          pos.rotX(0.55);
          pos.add(new Vec(0,0,2400));
          gi.set(pos.x,pos.y,pos.z,0x00FFFF,1.0);
        }
      }
      
      double rotY = -degTurn-PI/2.0;
      double SINrotY = Math.sin(rotY);
      double COSrotY = Math.cos(rotY);
      
      double rotX = 0.55 + (height/2.0-mouseY)/(height);
      double SINrotX = Math.sin(rotX);
      double COSrotX = Math.cos(rotX);
      
      for (int i=0;i<particles.size();i++) {
        particle=(Particle)(particles.elementAt(i));
        particle.gravitateTo(new Vec(0,0,0),MASS_SUN);
        particle.gravitateTo(attractPoint.pos,MASS_POINT);
        particle.move();
        //if (f==0) {
          Vec pos = new Vec(particle.pos);
          
          //pos.rotY(-degTurn-PI/2.0);
          double xn=pos.x*COSrotY-pos.z*SINrotY; 
          double zn=pos.z*COSrotY+pos.x*SINrotY;
          pos.x = xn;
          pos.z = zn;
          
          //pos.rotX(0.55);
          double yn=pos.y*COSrotX-pos.z*SINrotX;
          zn=pos.z*COSrotX+pos.y*SINrotX;
          pos.y=yn;
          pos.z=zn;
          
          pos.add(new Vec(0,0,2400));
          gi.set(pos.x,pos.y,pos.z,0xFF5020,0.2);
        //}
        if (particle.remove) particles.remove(particle);
        else if (particle.oob) {
          particles.remove(particle);
        }
      }
      attractPoint.gravitateTo(new Vec(0,0,0),MASS_SUN);
      attractPoint.move();
    }
  }
  
  gi.render();
  //}
  
  //if (frameCount%10==0) {
    //gi.blur(1,.5);
    image(gi,0,0);
    //save("MatterToPlanet3_"+nf((int)(frameCount/10),4)+".tga");
  //}
  
  
  int add = NR_OF_PARTICLES-particles.size();
  //println(add);
  if (add>0) {
    if (add>10) add=10;
    for (int i=0;i<add;i++) {
      particle=new Particle(300,50,true);
      particle.setOrbitalVelocity(MASS_SUN);
      particles.add(particle);
    }
  }
}
