class Particle {
  
  //position on torus tube cut:
  float rdOnTorusTubeCut;
  float rOnTorusTubeCut;
  float torusTubeRadius;
  float torusPosRd;
  boolean remove=false;
  boolean oob=false;
  
  Vec pos=new Vec(0,0,0);
  Vec mov=new Vec(0,0,0);
  Vec acc=new Vec(0,0,0);
  Vec cen=new Vec(0,0,0);
  
  Particle(float torusSize, float _torusTubeRadius, boolean onRandomSpot) {
    torusTubeRadius=_torusTubeRadius;
    
    // position on torus tube cut:
    rdOnTorusTubeCut=random(TWO_PI);
    rOnTorusTubeCut=random(1);
    float r=torusTubeRadius*pow(rOnTorusTubeCut,0.5);
    pos=new Vec(r*cos(rdOnTorusTubeCut),r*sin(rdOnTorusTubeCut),0);
    
    // position on torus:
    torusPosRd=random(TWO_PI);
    pos.add(torusSize,0,0);
    pos.rotY(torusPosRd);
    
    if (!onRandomSpot) {
      COL=color(255,0,0);
      torusPosRd=0;
      pos=new Vec(400,0,0);
    }
  }
    
  void setOrbitalVelocity(float massSun) {
    // orbital velocity:
    mov=new Vec(0,0,Math.sqrt(massSun/pos.len()));
    mov.rotY(torusPosRd);
  }
  
  /*
  void update(float _COALESCENCE) {
    //float r=torusTubeRadius*pow(rOnTorusTubeCut,0.5+5*_COALESCENCE);
    //pos=new Vec(r*cos(rdOnTorusTubeCut),r*sin(rdOnTorusTubeCut),0);
    
    // gravity to center:
    double minGravDist2=5;//_mgd*_mgd;
    double dx=-pos.x;
    double dy=-pos.y;
    double dz=-pos.z;
    double d2=dx*dx+dy*dy+dz*dz;
    if (d2<minGravDist2) {
      d2=minGravDist2;
    }
    double d=Math.sqrt(d2);
    double a=massSun/d2;
    mov.add(new Vec(dx/d*a,dy/d*a,dz/d*a));
    
    //mov.add(acc);
    pos.add(mov);
    stroke(COL);
    vecPoint(pos);
  }
  */
  
  void gravitateTo(Vec cen, float massSun) {
    double minGravDist2=4;//_mgd*_mgd;
    double dx=cen.x-pos.x;
    double dy=cen.y-pos.y;
    double dz=cen.z-pos.z;
    double d2=dx*dx+dy*dy+dz*dz;
    if (d2<minGravDist2) {
      d2=minGravDist2;
      remove=true;
    }
    double d=Math.sqrt(d2);
    double a=massSun/d2;
    mov.add(new Vec(dx/d*a,dy/d*a,dz/d*a));;
  }
  
  void move() {
    //mov.mul(0.9999);
    pos.add(mov);
    if (Math.abs(pos.x)>10000) { oob=true;  }
    if (Math.abs(pos.y)>10000) { oob=true;  }
    if (Math.abs(pos.z)>10000) { oob=true;  }
  }
  
  void show() {
    vecPoint(pos);
  }
  
  void showAsBall(float mass) {
    float r=1*pow(mass,1/3.0);
    noStroke();
    fill(COL);
    vecTranslate(pos);
    ellipse(0,0,r,r);
  }
}
