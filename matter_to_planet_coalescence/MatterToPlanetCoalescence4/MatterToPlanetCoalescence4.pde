import Graf.*;
color COL=color(255,40,30);

GrafImage3D gi;

int NR_OF_PARTICLES=10000;
float MASS_SUN=20000;
float MASS_POINT=10;

Particle particle;
Vector particles;

Particle attractPoint;

float coa=0;

void setup() {
  size(800,450,P3D);
  gi = new GrafImage3D(800,450,GrafUtil.BLEND_MODE_ADD);
  gi.background(0);
  //gi.DistanceToViewer=1000;
  particles=new Vector();
//  for (int i=0;i<NR_OF_PARTICLES;i++) {
//    particle=new Particle(400,80,true);
//    particle.setOrbitalVelocity(MASS_SUN);
//    particles.add(particle);
//  }

  attractPoint=new Particle(400,80,false);
  attractPoint.setOrbitalVelocity(MASS_SUN);
}

boolean massUp=true;
double massMul = 1.005; //1.0005
//double massMax = 1000;
//double massMin = 10;

double rotX = 0.55;
double SINrotX=Math.sin(rotX);
double COSrotX=Math.cos(rotX);

void draw() {
  if (frameCount%2==0) gi.background(0,0.05);
  //for (int g=0;g<5;g++) {
  //gi.background(0,0.05);
  for (int f=0;f>=0;f--) {
    if (massUp) {
      MASS_POINT*=massMul;
      if (MASS_POINT>1000) massUp=false;
    }
    else {
      MASS_POINT/=massMul;
      if (MASS_POINT<10) massUp=true;
    }
    if (coa<1.0) {
      double degTurn = Math.atan2(attractPoint.pos.z,attractPoint.pos.x);
      double SINDegTurn=Math.sin(-degTurn-PI/2.0); 
      double COSDegTurn=Math.cos(-degTurn-PI/2.0);
      for (int i=0;i<particles.size();i++) {
        particle=(Particle)(particles.elementAt(i));
        particle.gravitateTo(new Vec(0,0,0),MASS_SUN);
        particle.gravitateTo(attractPoint.pos,MASS_POINT);
        particle.move();
        if (f==0) {
          for (int zo=-800;zo<=1600;zo+=600) {
            for (int xo=-1200;xo<=1200;xo+=600) {
              Vec pos = new Vec(particle.pos);
              
              pos.div(2);
              
              //pos.rotY(-degTurn-PI/2.0);
              double xn=pos.x*COSDegTurn-pos.z*SINDegTurn; 
              double zn=pos.z*COSDegTurn+pos.x*SINDegTurn;
              pos.x = xn;
              pos.z = zn;
              
              pos.z += zo;
              pos.x += xo;
              
              //pos.rotX(0.55);
              double yn=pos.y*COSrotX-pos.z*SINrotX;
              zn=pos.z*COSrotX+pos.y*SINrotX;
              pos.y=yn;
              pos.z=zn;
              pos.add(new Vec(0,0,2400));
              gi.set(pos.x,pos.y,pos.z,0xFF5020,0.2 - 0.15*(zo+800)/2400.0);
            }
          }
        }
        if (particle.remove) particles.remove(particle);
        else if (particle.oob) {
          particles.remove(particle);
        }
      }
      attractPoint.gravitateTo(new Vec(0,0,0),MASS_SUN);
      attractPoint.move();
    }
  }
  
  gi.render();
  //}
  
  if (frameCount%20==0) {
    //gi.blur(1,.5);
    image(gi,0,0);
    //save("MatterToPlanet4_"+nf((int)(frameCount/20),4)+".tga");
  }
  
  
  int add = NR_OF_PARTICLES-particles.size();
  //println(add);
  if (add>0) {
    if (add>10) add=10;
    for (int i=0;i<add;i++) {
      particle=new Particle(300,50,true);
      particle.setOrbitalVelocity(MASS_SUN);
      particles.add(particle);
    }
  }
}
