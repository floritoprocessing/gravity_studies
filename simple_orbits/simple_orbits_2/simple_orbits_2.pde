/* simple orbits
 */
boolean alldead;//=false;

void setup() {
  size(800,600,P3D);
  alldead=false;
  background(0,0,0);
  planet=new Planet[10];
  for (int i=0;i<planet.length;i++) {
    planet[i]=new Planet(100+random(width-200),100+random(height-200));
  }
  for (int i=0;i<planet.length/3;i++) {
    planet[(int)random(planet.length)].gravDir*=-1;
  }

  p=new Particle[10];
  for (int i=0;i<p.length;i++) { 
    p[i]=new Particle(random(width),random(height)); 
  }
}

void mousePressed() {
  setup();
}

void draw() {
  if (!alldead) {
    for (int i=0;i<planet.length;i++) {
      planet[i].update();
      //planet[i].toScreen();
    }

    boolean tempalldead=true;
    for (int i=0;i<p.length;i++) { 
      p[i].update(); 
      if (!p[i].dead) {tempalldead=false;}
    }
    if (tempalldead&&!alldead) {
      alldead=true;
      println("DONE!");
    }
  }
}

Particle[] p;
Planet[] planet;

class Particle {
  double x=0, y=0;
  float ms=0.5;
  double xm=random(-ms,ms), ym=random(-ms,ms);
  double fric=0.999995;
  double speed=0, maxspeed=0;
  color c=color(179+random(-10,10),242+random(-10,10),144+random(-10,10));
  color bc=c;
  double age=0.0002; boolean dead=false;

  Particle() {
  }
  Particle(double _x, double _y) {
    x=_x;
    y=_y;
  }

  void update() {
    maxspeed=0;
    for (int times=0;times<500;times++) {
      if (!dead) {
        age+=0.0002;
        dead=(age>=100);
        double ax=0, ay=0;
        for (int i=0;i<planet.length;i++) {
          double dx=planet[i].x-x, dy=planet[i].y-y;
          double d2=dx*dx+dy*dy;
          if (d2<10) {
            d2=10;
          }
          //double d=Math.sqrt(d2);
          double F=2*planet[i].gravDir/d2;
          ax+=dx*F;
          ay+=dy*F;
        }
        xm+=ax; 
        ym+=ay;
        xm*=fric; 
        ym*=fric;
        x+=xm; 
        y+=ym;

        speed=Math.sqrt(xm*xm+ym*ym);  //  0..oo
        //if (speed>maxspeed) {maxspeed=speed;}
        colorMode(HSB,255);
        c=color(hue(bc)+(int)(speed*128),saturation(bc),brightness(bc));
        colorMode(RGB,255);

        softSet((int)x,(int)y,c, 0.06 * ((100-(float)age)/100.0) );
      }
    } // end of 500 times
  }
}

class Planet {
  double x=0, y=0;
  float ms=0.5;
  double xm=random(-ms,ms), ym=random(-ms,ms);
  double gravDir=1;
  Planet() {
  }
  Planet(double _x, double _y) {
    x=_x;
    y=_y;
  }
  void toScreen() {
    fill(128,0,0);
    ellipseMode(CENTER);
    ellipse((float)x,(float)y,5,5);
  }
  void update() {
    x+=xm; 
    y+=ym;
    if (x>width||x<0) {
      xm*=-1;
    }
    if (y>height||y<0) {
      ym*=-1;
    }
  }
}

void softSet(int x, int y, color c1, float p1) {
  float p2=1.0-p1;
  float r1=red(c1), g1=green(c1), b1=blue(c1);
  color c2=get((int)x,(int)y); 
  float r2=red(c2), g2=green(c2), b2=blue(c2);
  float rr=p1*r1+p2*r2, gg=p1*g1+p2*g2, bb=p1*b1+p2*b2;
  set((int)x,(int)y,color(rr,gg,bb));
}
